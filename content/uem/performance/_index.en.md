+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "performance", "sql" ]
description = "WS1 UEM Large-Scale Performance configs"
title = "Large-Scale Performance"
+++

## Large-Scale Performance Tweaks

For **75000+** devices (iOS, Android & Windows Desktops)

## Linked Articles

{{% children sort="weight" description="true" %}}

## Attached Documents (pdf)
[Test Report 1811 - 75k devices](./_index.en.files/Performance-Test_Report_1907_External.pdf)
[Test Report 1907 - 1.2mln devices](./_index.en.files/Performance-Test_Report_1907_RSCC.pdf)
[Test Report 1907 (500k devices)](./_index.en.files/Performance-Test-Report_1907.pdf)
[Test Strategy 1902](./_index.en.files/Performance-Test-Srategy_1902.pdf)
[UI Test 1902](./_index.en.files/Performance-UI-Test_1902.pdf)
[Sizing Topology - 1mln devices](./_index.en.files/MOE1Mil.pdf)

## SQL

See [SQL Database page](uem/SQL/_index.html) for base tweaks on MS SQL.

-   Add 1 tempdb file per core (20 tempdb files for 20 core)

Each tempdb data file with below parameters:
    - Initial Size: 16000 MB
    - Autogrowth: 512 MB

```sql
USE [master];
GO
ALTER DATABASE [tempdb] ADD FILE (NAME = N'Temp1', FILENAME = N'H:\TempDBs\Temp1.ndf', SIZE = 8GB , FILEGROWTH = 512);
ALTER DATABASE [tempdb] ADD FILE (NAME = N'Temp2', FILENAME = N'H:\TempDBs\Temp2.ndf', SIZE = 8GB , FILEGROWTH = 512);
ALTER DATABASE [tempdb] ADD FILE (NAME = N'Temp3', FILENAME = N'H:\TempDBs\Temp3.ndf', SIZE = 8GB , FILEGROWTH = 512);
GO
```

-   Add below Trace flags in Startup Parameters for "mssqlserver" service as per Microsoft recommendations:  
    **T174** - [https://support.microsoft.com/en-us/help/3026083/fix-sos-cachestorespinlock-contention-on-ad-hoc-sql-server-plan-cache](https://support.microsoft.com/en-us/help/3026083/fix-sos-cachestorespinlock-contention-on-ad-hoc-sql-server-plan-cache)   
    **T834** - [https://support.microsoft.com/en-us/help/920093/tuning-options-for-sqlserver-when-running-in-high-performance-workloads](https://support.microsoft.com/en-us/help/920093/tuning-options-for-sqlserver-when-running-in-high-performance-workloads)  
    **T3247** - [https://support.microsoft.com/en-us/help/3216543/workloads-that-utilizemany-frequent-short-transactions-in-sql-server](https://support.microsoft.com/en-us/help/3216543/workloads-that-utilizemany-frequent-short-transactions-in-sql-server)
-   Set “Lock Pages in Memory” privilege for the service account  
    Open Windows **Local Group Policy** Editor  
    Open **Computer Configuration > Windows Settings > Local Policies > User Rights Assignment**, find **Lock Pages in Memory**
-   Disable "Named Pipes" and Enable "TCP/IP" network protocol  
    Open **SQL Server Configuration Manager** tool  
    Open **SQL Server Network Configuration > Protocols for MSSQLSERVER**, set **Named Pipes** to **Disabled**, set **TCP/IP** to **Enabled**
-   Increase Maximum Worker threads in server properties to "7500"  
    Open **Server Properties** from **SSMS** and go to **Processors** tab, find **Maximum Worker threads** field
-   Set “Max Degree Of Parallelism” to 2 and “Cost threshold for Parallelism” to 50  
    Open **Server Properties** from **SSMS** and go to **Advanced** tab, **Parallelism** section in the right pane
-   Enable “Received Side scale” setting for Network Adapter on SQL server  
    Run **DEVMGMT.msc** from CMD  
    Expand **Network adapters**, right click on your adapter and select **Properties**  
    Select the **Advanced** tab and find **Receive Side Scaling**. Set this to **Enabled**
-   Set “Delayed Durability” under database properties = “Forced” to reduce WriteLog waits  
    Open **Database Properties** from **SSMS** and go to **Options**
-   Update “Minimum and Maximum Server Memory” allocation in Server Properties  
    Open **Memory** from **SSMS → Server Memory Options section**  
    + Minimum Server Memory (in MB): 256000  
    + Maximum Server Memory (in MB): 1500000

## Performance Tweaks
### Example Architecture
![](PerfArchitecture.jpg)

-   Update Workspace ONE UEM application server to latest version

In WS1 UEM Console go to **Settings > Installation > Performance Tuning**:
-   Set Certificate Profile Publish Frequency: **100**
-   Set Apple Profile Installation Batch Size: **300**
-   Set iOS Device Invites Per Second: **30**
-   Set FastLaneMessageRateMultiple: **1.5**
-   Run Real-Time Compliance is set to **Disabled**

Check Windows Sample Frequency (for managing **Windows Desktops**):
- Go to **Settings > Devices & Users > Windows > Windows Desktop > App Settings**
- Go to **Settings > Devices & Users > Windows > Windows Desktop > Windows Sample Schedule**
![](PerfWinApps.png)
![](PerfWinSamples.png)
-   Override the default value in **Systemcode** table for **FastLaneMessageRateMultiple** to **1.5** by updating **Systemcodeoverride** table in DB.
-   Add the below config to AW.Send.Messaging.Service.exe.config file in Services folder of all console servers under appSettings:
```xml
<add key="WnsMessageProcessingRate" value="10" />
<add key="WnsMessageThrottlingEnabled" value="true" />
```
![](PerfSettings.png)
