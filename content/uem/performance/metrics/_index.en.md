+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "performance", "service", "web" ]
description = "WS1 UEM Large-Scale Performance configs"
title = "Large-Scale Performance"
+++

## WS1 UEM Services/Applications

API Workflow  
Batch Processing  
Entity Change Queue Monitor  
EventLog Processor Service  
Integration Service  
MEG Service  
Messaging Service  
Outbound Queue Monitor Service  
Smart Group Service  
Agent Builder Service  
Background Processor Service  
Compliance Service  
Content Delivery  
Device Scheduler  
Directory Sync Service  
GEM Inventory Service  
Policy Engine  
SMS Service  
Google Play Service  
Data Platform Service  
Interrogator Queue Service  
Provisioning Package Service  
MSMQ

|PerfMon Object| Perfmon Counter| 
---|---
Process | "% Privileged Time", "% Processor Time", "% User Time", "Elapsed Time", "Handle Count", "IO Read Bytes/sec", "IO Read Operations/sec", "IO Write Bytes/sec", "IO Write Operations/sec", "Private Bytes", "Thread Count", "Virtual Bytes", "Working Set", "Working Set - Private"
.NET CLR Exceptions | "# of Exceps Thrown", "# of Exceps Thrown / Sec", "# of Filters / Sec", "# of Finallys / Sec", "Throw to Catch Depth / Sec"
.NET CLR LocksAndThreads | "# of current logical Threads","# of current physical Threads","# of current recognized threads", "# of total recognized threads","Contention Rate / Sec","Current Queue Length","Queue Length / sec", "Queue Length Peak","rate of recognized threads / sec","Total # of Contentions"
.NET CLR Memory | "% Time in GC","# Bytes in all Heaps","# Gen 0 Collections","# Gen 1 Collections","# Gen 2 Collections", "# Induced GC","Allocated Bytes/sec","Finalization Survivors","Gen 0 heap size","Gen 1 heap size", "Gen 2 heap size","Large Object Heap size","# of Pinned Objects","# GC Handles","# of Sink Blocks in use", "# Total committed Bytes","# Total reserved Bytes","Finalization Survivors","Gen 0 Promoted Bytes/Sec", "Gen 1 Promoted Bytes/Sec","Large Object Heap size","Process ID","Promoted Finalization-Memory from Gen 0",  "Promoted Memory from Gen 0","Promoted Memory from Gen 1"
.NET Data Provider for SqlServer | "NumberOfActiveConnections", "NumberOfFreeConnections", "NumberOfPooledConnections", "NumberOfReclaimedConnections", "SoftConnectsPerSecond", "SoftDisconnectsPerSecond"
.NET Memory Cache 4.0 | "Cache Entries", "Cache Hit Ratio", "Cache Hits", "Cache Misses", "Cache Trims", "Cache Turnover Rate"
MSMQ Service | "Total bytes in all queues", "Total messages in all queues"

## IIS Web Server Performance Counters

|PerfMon Object| Perfmon Counter| 
---|---
Web Service | "Service Uptime", "Current Connections", "Bytes Sent/sec", "Total Bytes Sent",  "Bytes Received/sec", "Total Bytes Received", "Bytes Total/sec", "Total Bytes Transferred",  "Get Requests/sec","Total Get Requests", "Post Requests/sec","Total Post Requests",  "Put Requests/sec","Total Put Requests", "Delete Requests/sec","Total Delete Requests",  "Anonymous Users/sec", "NonAnonymous Users/sec", "Not Found Errors/sec", "Locked Errors/sec", "Total Method Requests", "Total Method Requests/sec"
Web Service Cache | "Current Files Cached", "Active Flushed Entries", "Total Files Cached", "Total Flushed Files",  "File Cache Hits", "File Cache Misses", "File Cache Hits %", "File Cache Flushes",  "Current File Cache Memory Usage", "Maximum File Cache Memory Usage", "Current URIs Cached",  "Total URIs Cached", "Total Flushed URIs", "URI Cache Hits", "URI Cache Misses", "URI Cache Hits %",  "URI Cache Flushes", "Current Metadata Cached", "Total Metadata Cached", "Total Flushed Metadata",  "Metadata Cache Hits", "Metadata Cache Misses", "Metadata Cache Hits %", "Metadata Cache Flushes",  "Kernel: Current URIs Cached", "Kernel: Total URIs Cached", "Kernel: Total Flushed URIs",  "Kernel: URI Cache Hits", "Kernel: Uri Cache Hits/sec", "Kernel: URI Cache Misses", "Kernel: URI Cache Hits %",  "Kernel: URI Cache Flushes"
ASP.NET | "Application Restarts", "Requests Queued",  "Requests Rejected"
ASP.NET Applications | "Requests/Sec", "Errors Total",  "Cache Total Entries", "Cache Total Turnover Rate",  "Cache Total Hits", "Cache Total Misses",  "Cache Total Hit Ratio", "Pipeline Instance Count", "Requests/Sec", "Requests In Application Queue", "Request Execution Time", "Request Wait Time", "Requests Executing"
HTTP Service Request Queues | "ArrivalRate", "CurrentQueueSize","RejectedRequests"
