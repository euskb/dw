+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "android", "iOS", "macos", "security" ]
description = "XML Profiles for WS1 Launcher and restrictions"
title = "WS1 Launcher and Restrictions"
+++

## Custom XML Profiles for Android
#### External link
Custom XML for Workspace ONE Launcher - https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2209/Launcher_Publication/GUID-CustomXML.html?hWord=N4IghgNiBcIMYFcDOAXA9gWwAQA8MSwAcAnNAMwEsIBTLCMBAOzgAtriQBfIA

When using Ws1 Launcher for Android, it will block Phone app and incoming phone call notifications. In order to allow phone dialer app (example: com.samsung.android.dialer) a custom profile has to be added:
```xml
<characteristic type="com.airwatch.android.androidwork.launcher" uuid="568bc89d-1df8-4ce9-a041-e5a24acdb7ec">
<parm name="SkipCosuSetup" value="True"/>
```

### Enable WiFi in Android Kiosk

By default, WiFi is blocked by kiosk mode. To unlock possibility for user to choose WiFi network do this:

-   Check the APP ID of "**Settings**" application on Android OS;
-   Add **Settings** app APP ID to AirWatch Launcher as Hidden Apps.

![](Launcher_Settings.jpg)

## Custom XML Profiles for iOS
**Custom Settings** profile payload allows admins to enter their own XML into a profile and apply the profile to devices.

### (For iOS 11.3+ and macOS 10.13.4+)
-   XML should contain the complete block of code as listed below, from <dict> to </dict>. 
-   Administrators should configure each setting from <true /> to <false /> as desired.
-   If certificates are required, then configure a Certificate payload within the profile and reference the **Payload UUID** in the Custom Settings payload.

## iOS 11.3
### Restrictions
```xml
<dict>
         <key>allowUSBRestrictedMode</key>
         <true/>
         <key>forceClassroomRequestPermissionToLeaveClasses</key>
         <true/>
         <key>forceDelayedSoftwareUpdates</key>
         <true/>
         <key>enforcedSoftwareUpdateDelay</key>
         <integer>30</integer>
         <key>PayloadDisplayName</key>
         <string>Restrictions</string>
         <key>PayloadDescription</key>
         <string>RestrictionSettings</string>
         <key>PayloadIdentifier</key>
         <string>7480b205-2e1c-40fe-bd59-b53db434652d.Restrictions</string>
         <key>PayloadOrganization</key>
         <string></string>
         <key>PayloadType</key>
         <string>com.apple.applicationaccess</string>
         <key>PayloadUUID</key>
         <string>99b5b40b-5683-4315-9ec2-f9e014a6XXXX</string>
         <key>PayloadVersion</key>
         <integer>1</integer>
 </dict>
```

### Home Screen Layout
{{% notice style="note" %}}
Web clips should be configured and deployed using the current web clip payload UI
{{% /notice %}}
```xml
<dict>
    <key>Dock</key>
    <array>
        <dict>
            <key>Type</key>
            <string>WebClip</string>
            <key>URL</key>
            <string>https://google.com</string>
        </dict>
    </array>
    <key>Pages</key>
    <array>
        <array>
            <dict>
                <key>Type</key>
                <string>WebClip</string>
                <key>URL</key>
                <string>https://yahoo.com</string>
            </dict>
            <dict>
                <key>Type</key>
                <string>Folder</string>
                <key>DisplayName</key>
                <string>My Web Clip</string>
                <key>Pages</key>
                <array>
                    <array>
                        <dict>
                            <key>Type</key>
                            <string>WebClip</string>
                            <key>URL</key>
                            <string>https://www.vmware.com</string>
                        </dict>
                    </array>
                </array>
            </dict>
        </array>
    </array>
    <key>PayloadDisplayName</key>
    <string>Home Screen Layout</string>
    <key>PayloadDescription</key>
    <string>HomeScreenLayout</string>
    <key>PayloadIdentifier</key>
    <string>97213d06-b750-466b-8a89-782d8a406f86.Home Screen Layout</string>
    <key>PayloadOrganization</key>
    <string></string>
    <key>PayloadType</key>
    <string>com.apple.homescreenlayout</string>
    <key>PayloadUUID</key>
    <string>2fa8fe03-30fa-4189-aa00-ba752eabXXXX</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
</dict
```

### Enable Bluetooth Command
Unlike custom profiles, the payload content and UUID are not required for custom commands. This command will not take place if the Allow Bluetooth Settings Modification restriction is enforced.
```xml
<dict>
  <key>RequestType</key>
  <string>Settings</string>
  <key>Settings</key>
  <array>
    <dict>
      <key>Item</key>
      <string>Bluetooth</string>
      <key>Enabled</key>
      <true/>
    </dict>
  </array>
</dict>
```

## macOS 10.13.4 Custom XML
### Restrictions
```xml
<dict>
         <key>forceDelayedSoftwareUpdates</key>
         <true/>
         <key>enforcedSoftwareUpdateDelay</key>
         <integer>30</integer>
         <key>PayloadDisplayName</key>
         <string>Restrictions</string>
         <key>PayloadDescription</key>
         <string>RestrictionSettings</string>
         <key>PayloadIdentifier</key>
         <string>7480b205-2e1c-40fe-bd59-b53db434652d.Restrictions</string>
         <key>PayloadOrganization</key>
         <string></string>
         <key>PayloadType</key>
         <string>com.apple.applicationaccess</string>
         <key>PayloadUUID</key>
         <string>99b5b40b-5683-4315-9ec2-f9e014a6XXXX</string>
         <key>PayloadVersion</key>
         <integer>1</integer>
 </dict>
```

### Autonomous Single App (ASA) Mode
```xml
<dict>
    <key>AllowedApplications</key>
    <array>
      <dict>
        <key>BundleIdentifier</key>
        <string>com.sample.app1</string>
        <key>TeamIdentifier</key>
        <string>ABCDEFG1HI</string>
      </dict>
      <dict>
        <key>BundleIdentifier</key>
        <string>com.sample.app2</string>
        <key>TeamIdentifier</key>
        <string>ABCDEFG1HI</string>
      </dict>
    </array>
    <key>PayloadDisplayName</key>
    <string>Autonomous Single App Mode</string>
    <key>PayloadDescription</key>
    <string>AutonomousSingleAppMode</string>
    <key>PayloadIdentifier</key>
    <string>7480b205-2e1c-40fe-bd59-b53db434652d.AutonomousSingleAppMode</string>
    <key>PayloadOrganization</key>
    <string></string>
    <key>PayloadType</key>
    <string>com.apple.asam</string>
    <key>PayloadUUID</key>
    <string>91b5e40b-5683-4376-9ec2-f9e214a6XXXX</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
</dict
```

{{% notice style="warning" %}}
-   Can only be installed on User Approved MDM Enrolled devices. Must be installed as Device profile. Only one payload allowed per machine.
-   To be granted access, applications must be signed with the specified Bundle Identifier and Team Identifier using an Apple-issued production developer certificate. Applications must specify the com.apple.developer.assessment entitlement with a value of true.
-   The application’s bundle identifier. BundleIdentifier must be unique. If two dictionaries contain the same BundleIdentifier but different TeamIdentifiers, this will be considered a hard error and the payload will not be installed.
{{% /notice %}}

To check if the .app has the correct entitlement noted above:

**codesign –d --entitlements - /Applications/Example.app**

This will print out XML with the entitlements. It needs to have the com.apple.developer.assessment entitlement with a value of true.

To get the Bundle & Team Identifier for an .app:

**codesign –dvvvv /Applications/Example.app**

The Bundle Identifier will be in the ‘Identifier’ field. The Team Identifier will be a 10 character string in the ‘TeamIdentifier’ field.

### Content Caching
```xml
<dict>
    <key>AllowPersonalCaching</key>
    <true/>
    <key>AllowSharedCaching</key>
    <true/>
    <key>AutoActivation</key>
    <true/>
    <key>CacheLimit</key>
    <integer>100000000</integer> <!--100 MB example-->
    <key>DataPath</key>
    <string>/Library/Application Support/Apple/AssetCache/Data</string>
    <key>DenyTetheredCaching</key>
    <false/>
    <key>ListenRanges</key>
    <array>
      <dict>
        <key>type</key>
        <string>IPV4</string>
        <key>first</key>
        <string>0.0.0.0</string>
        <key>last</key>
        <string>255.255.255.255</string>
      </dict>
    </array>
    <key>ListenRangesOnly</key>
    <false/>
    <key>ListenWithPeersAndParents</key>
    <true/>
    <key>LocalSubnetsOnly</key>
    <true/>
    <key>LogClientIdentity</key>
    <false/>
    <key>Parents</key>
    <array>
      <string>1.1.1.1</string>
      <string>2.2.2.2</string>
    </array>
    <key>ParentSelectionPolicy</key>
    
 <string>round-robin</string> <!-- Possible values are
round-robin, first-available, url-path-hash, random, and
sticky-available-->
    <key>PeerFilterRanges</key>
    <array>
      <dict>
        <key>type</key>
        <string>IPV4</string>
        <key>first</key>
        <string>0.0.0.0</string>
        <key>last</key>
        <string>255.255.255.255</string>
      </dict>
    </array>
    <key>PeerListenRanges</key>
    <array>
      <dict>
        <key>type</key>
        <string>IPV4</string>
        <key>first</key>
        <string>0.0.0.0</string>
        <key>last</key>
        <string>255.255.255.255</string>
      </dict>
    </array>
    <key>PeerLocalSubnetsOnly</key>
    <true/>
    <key>Port</key>
    <integer>0</integer>
    <key>PublicRanges</key>
    <array>
      <dict>
        <key>type</key>
        <string>IPV4</string>
        <key>first</key>
        <string>0.0.0.0</string>
        <key>last</key>
        <string>255.255.255.255</string>
      </dict>
    </array>
    <key>PayloadDisplayName</key>
    <string>Content Caching</string>
    <key>PayloadDescription</key>
    <string>ContentCaching</string>
    <key>PayloadIdentifier</key>
    <string>7480b205-2e1c-40fe-bd59-b53db434652d.ContentCaching</string>
    <key>PayloadOrganization</key>
    <string></string>
    <key>PayloadType</key>
    <string>com.apple.AssetCache.managed</string>
    <key>PayloadUUID</key>
    <string>98f5b40b-5683-2415-9ec2-f9e014a6XXXX</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
</dict>
```