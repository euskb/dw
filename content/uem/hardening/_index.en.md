+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "security" ]
description = "WS1 UEM hardening methods and tools"
title = "WS1 UEM Hardening"
+++

## Linked Articles

{{% children sort="weight" description="true" %}}

## Tools to check SSL/TLS certificates and protocol

-   **DeepViolet** - [https://github.com/spoofzu/DeepViolet](https://github.com/spoofzu/DeepViolet)
Java-based tool for SSL/TLS analysis, available in binary form or can be compiled.

-   **SSL Diagnos** - [https://sourceforge.net/projects/ssldiagnos](https://sourceforge.net/projects/ssldiagnos)
SSL protocol analyzer, detects Heartbleed, BEAST exploits. Besides HTTPS, also checks SSL for SMTP, SIP, POP3, FTPS.

-   **SSLyze** - [https://github.com/nabla-c0d3/sslyze](https://github.com/nabla-c0d3/sslyze)
Python library and CLI tools. Exports results in XML or JSON. Available as part of [Kali Linux](https://geekflare.com/kali-linux-installation-guide-vmware).

-   **OpenSSL** - [https://geekflare.com/openssl-commands-certificates](https://geekflare.com/openssl-commands-certificates)
Available for Windows, Mac, Linux. Generates CSR, verifies SSL, [converts certificate formats](https://digital-work.space/display/IDM/Identity+Manager+Appliance+and+certificates).

-   **SSL Labs Scan** - [https://github.com/ssllabs/ssllabs-scan](https://github.com/ssllabs/ssllabs-scan)
Good instrument for automatic and mass website SSL testing.

-   **SSL Scan** - [https://www.darknet.org.uk/2016/12/sslscan-detect-ssl-versions-cipher-suites-including-tls/](https://www.darknet.org.uk/2016/12/sslscan-detect-ssl-versions-cipher-suites-including-tls/)
C-based CLI tool for Windows, Mac, Linux. Highlights weak ciphers, checks TLS compression, Heartbleed exploit. Doesn't do grading of results, need to parse result manually.

-   **Test SSL** - [https://testssl.sh/](https://testssl.sh/)
Also available as a [Docker image](https://quay.io/repository/jumanjiman/testssl). CLI tool for Linux, Mac. Checks lots of parameters and exploits in ciphers.

-   **TLS Scan** - [https://github.com/prbinu/tls-scan](https://github.com/prbinu/tls-scan)
CLI tool for Linux, Mac. Exports results in JSON format. Supports TLS, SMTP, STARTTLS and MySQL.

-   **Cipher Scan** - [https://github.com/mozilla/cipherscan](https://github.com/mozilla/cipherscan)
Wrapper over OpenSSL. Detects weak ciphers used on websites. Exports results in JSON format.

-   **SSL Audit** - [https://code.google.com/archive/p/sslaudit](https://code.google.com/archive/p/sslaudit)
CLI tool for Linux and Windows. Verifies SSL certificate and supported protocols/ciphers of a SSL-enabled webserver. The result is graded according to [SSLLabs SSL Server Rating Guide](https://www.ssllabs.com/projects/rating-guide/index.html).