+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "security" ]
description = "Device Services IIS Hardening"
title = "IIS Hardening"
+++

## Device Services IIS

**Disable insecure TLS/SSL protocol support**  
- Yes, you can disable this and this will not have any impact on AirWatch Applications because we have made the necessary changes in our components as well.   
POODLE attack, SSLv3 etc have been taken care by our developers in console version 8.1 and above.  
  
**Remove the default page or stop/disable the IIS server**  
- Yes, you can remove the default page, but do not disable the IIS server. Recommended not to disable the IIS server.  
  
**Disable TLS/SSL support for RC4 ciphers**  
-  Please refer the below link for disabling RC4 cipher.  
[https://support.microsoft.com/en-us/kb/245030](https://support.microsoft.com/en-us/kb/245030)  
[https://support.microsoft.com/en-us/kb/2868725](https://support.microsoft.com/en-us/kb/2868725)   

**Disable SSLv2, SSLv3, and TLS 1.0**  
- This will not have any impact on the AirWatch application and you can have this disabled. Best solution for this will be to enable TLS1.2  
[https://support.microsoft.com/en-us/kb/187498](https://support.microsoft.com/en-us/kb/187498)  
  
**Regarding Ciphers suites**  
-  Be it any kind of Ciphers(Static key cipher, 3DES cipher, Strong cipher) the best solution for this  is to enable TLS. Also, the MicrosoftKB article 245030 as mentioned in the ticket is the best solution for all the Cipher questions.  
[https://support.microsoft.com/en-us/kb/245030](https://support.microsoft.com/en-us/kb/245030)  
  
  
The RC4 cipher can be completely disabled on Windows platforms by setting the "Enabled" (REG_DWORD) entry to value 00000000 in the following registry locations:  
  
• HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128/128  
• HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128  
• HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128  
  
Disabling SSL and enabling TLS is out of Airwatch's scope as you have to make registry changes locally on the server:  
  
Disabling SSL 3.0- [https://www.digicert.com/ssl-support/iis-disabling-ssl-v3.htm](https://www.digicert.com/ssl-support/iis-disabling-ssl-v3.htm)  
Enabling TLS - [https://support.quovadisglobal.com/KB/a433/how-to-enable-tls-12-on-windows-server-2008-r2.aspx](https://support.quovadisglobal.com/KB/a433/how-to-enable-tls-12-on-windows-server-2008-r2.aspx)  
[https://www.hass.de/content/setup-your-iis-ssl-perfect-forward-secrecy-and-tls-12](https://www.hass.de/content/setup-your-iis-ssl-perfect-forward-secrecy-and-tls-12)  
  
Disabling RC4 on Java:  
[http://stackoverflow.com/questions/18589761/restict-cipher-suites-on-jre-level](http://stackoverflow.com/questions/18589761/restict-cipher-suites-on-jre-level)  
  
Example:
```shell
jdk.certpath.disabledAlgorithms=MD2
jdk.tls.disabledAlgorithms=MD5, SHA1, RC4, RSA keySize < 1024
```

We can either do it at a JRE system wide level or at a JVM instance (such as AWCM) level adding RC4 as a disabled algorithm when a choice has to be made as part of SSL handshake.  
In the latter case, It will be a config change on AWCM Service parameters (only change being the added restriction option in $AWCM_HOME/service/AWCMService.exe.parameters).

{{% notice style="tip" %}}
[IIS Crypto Tool](https://www.nartac.com/Products/IISCrypto/Download) can be used to turn off weak ciphers in Windows Server 2008+
{{% /notice %}}

{{% notice style="warning" %}}
Usage of iiscrypto tool to disable Cipher Suites, as well as registry keys **can break communication** between AirWatch components.

Use with extreme caution, **ONLY AFTER** AirWatch was deployed and tested to be working. Disable Cipher Suites one by one and re-test AirWatch functionality after each change!
{{% /notice %}}

Security scanner sees IIS vulnerabilities:

-   SWEET32
-   POODLE
-   TLS_FALLBACK_SCSV

Hardening:

-   POODLE - need to disable SSL 3.0 protocol. Open registry: `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\Schannel\Protocols` and create keys `SSL 3.0`\Server (if not created previously), create a DWORD value Enabled = 0;
![](sberDeviceManager_regedit.jpg)

-   SWEET32 - need to disable weak ciphers. Open registry: `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168`, create a DWORD value Enabled = 0. Create keys "RC4 56/128", "RC4 40/128", "RC4 128/128" create a DWORD value in all keys called Enabled = 0;

{{% notice style="warning" %}}
When turning off Triple DES the RDP protocol to server may stop working. Need to patch RDP to use modern ciphers to solve this problem.
{{% /notice %}}

![](sberDeviceManager_regedit2.jpg)

-   TLS_FALLBACK_SCSV (**only for Windows 2003-2008**! see KB from Microsoft: [https://support.microsoft.com/kb/980436/en-us)](https://support.microsoft.com/kb/980436/en-us) - Open registry: `HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\SecurityProviders\SCHANNEL` and create DWORD called UseScsvForTls with parameters:
    -   UseScsvForTls = 0 # Client sends Renegotiation Info extension for TLS protocol
    -   UseScsvForTls = 1 # Client sends SCSV for TLS protocol - use this to solve problem

{{% notice style="warning" %}}
AirWatch Self-Service portal uses TLS1.0/RC4-type cryptography, and gives Error and blank page after IIS hardening!
{{% /notice %}}

## iOS 11 & Wi-Fi TLS 1.2 Requirements

With the release of iOS 11, TLS 1.2 will now be the default for EAP-TLS negotiation. This may cause an issue with older clients that still need to connect on TLS 1.0 or 1.1. Apple has allowed for a method to override this default setting with a configuration profile sent down to the device via MDM. In order to ensure your iOS devices maintain Wi-Fi connection when upgrading to iOS 11, please follow the steps below:  
  
**Note:** If you already have a successfully deployed iOS Wi-Fi with EAP-TLS configured, skip to step 3. 

1.  Create a new profile with a **Wi-Fi** payload using EAP-TLS and **General** payload configured.
2.  Ensure that the profile successfully configures Wi-Fi on an iOS device.
3.  From your profile list view, select the Wi-Fi with EAP-TLS created profile and choose to view XML.
4.  Export or copy the XML of the profile.
5.  Edit the XML to remove everything prior to the first <dict> and after its corresponding </dict>.
6.  Edit the XML again to add the following bolded key/values (accepted values are 1.0, 1.1, and 1.2). These should be a part of the EAPClientConfiguration key

```xml
<key>EAPClientConfiguration</key>
<dict>
<key>AcceptEAPTypes</key>
<array>
<integer>13</integer>
</array>
<key>TLSMaximumVersion</key>
<string>1.1</string>
<key>TLSMinimumVersion</key>
<string>1.0</string>
</dict>
```

- Edit the XML a final time to create a unique identifier for the payload. Locate the PayloadUUID key and edit the values that correspond to the 'X's to random values. Please ensure these values are as random as possible to avoid issues with duplicate identifiers (e.g. 123456, 111111, 101010).
```xml
<key>PayloadUUID</key>
      <string>352B3FD9-B875-45C5-AA0E-AAFEE3XXXXXX</string>
```
1.  Create another new profile and configure the **General** payload
2.  Paste your edited XML into the **Custom Settings** payload and publish to devices

## IISCrypto config from AirWatch

iOS supports all latest ciphers and encryptions – however there are questions with Android, so Android 4.4 will be the baseline.

Protocols:  SSL 2.0, SSL 3.0, TLS 1.0, TLS 1.1 can be turned off as all of the platforms supports newer protocols.

{{% notice style="warning" %}}
Test results at client site (AirWatch ver. 9.2.3): disabling TLS 1.0 showed 4 services failing at AirWatch Device Services Server role - proceed with caution!
{{% /notice %}}

{{% expand title="Cipher Suites, which can be turned off..." %}}
<span style="color:yellow">TLS_RSA_WITH_AES_256_GCM_SHA384 (0x9d)   **WEAK** 256</span>

<span style="color:yellow">TLS_RSA_WITH_AES_256_CBC_SHA256 (0x3d)   **WEAK** 256</span>

<span style="color:yellow">TLS_RSA_WITH_AES_256_CBC_SHA (0x35)   **WEAK** 256</span>

<span style="color:yellow">TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA (0xc012)   **WEAK** 112</span>

<span style="color:yellow">TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA (0xc008)  **WEAK** 112</span>

<span style="color:yellow">TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA (0x16)   **WEAK** 112</span>

<span style="color:yellow">TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA (0x13)   **WEAK** 112</span>

<span style="color:yellow">TLS_RSA_WITH_3DES_EDE_CBC_SHA (0xa)   **WEAK** 112</span>

<span style="color:yellow">TLS_RSA_WITH_AES_128_GCM_SHA256 (0x9c)   **WEAK** 128</span>

<span style="color:yellow">TLS_RSA_WITH_AES_128_CBC_SHA256 (0x3c)   **WEAK** 128</span>

<span style="color:yellow">TLS_RSA_WITH_AES_128_CBC_SHA (0x2f)   **WEAK** 128
</span>

<span style="color:red">TLS_ECDHE_RSA_WITH_RC4_128_SHA (0xc011) **INSECURE** 128</span>

<span style="color:red">TLS_ECDHE_ECDSA_WITH_RC4_128_SHA (0xc007) **INSECURE** 128</span>

<span style="color:red">TLS_RSA_WITH_RC4_128_SHA (0x5) **INSECURE** 128</span>

<span style="color:red">TLS_RSA_WITH_RC4_128_MD5 (0x4) **INSECURE** 128
</span>
{{% /expand %}}

## VMware Assist

External link: [https://stackoverflow.com/questions/52212866/how-can-i-disable-tls-rsa-with-aes-128-cbc-sha-without-disabling-others-as-well](https://stackoverflow.com/questions/52212866/how-can-i-disable-tls-rsa-with-aes-128-cbc-sha-without-disabling-others-as-well)

Hardening Assist is basically hardening of Java, disabling TLS_RSA_WITH_AES... in it.
Delete RSA Keysize, and add specific protocols.
