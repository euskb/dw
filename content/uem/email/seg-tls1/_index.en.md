+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg" ]
description = "Enable TLSv1.0 on SEG"
title = "SEG TLSv1"
+++

Please go through the following instructions in order to enable TLSv1.0 on SEG V2:

-   Go to SEG installation directory -> {SEG_DIRECTORTY}/**service/conf**
-   Edit file **conf**
-   Look for following properties:

### Property 1
`wrapper.java.additional.9=-Djdk.tls.disabledAlgorithms=MD5\, RC4\, TLSv1\, SSLv2Hello\, SSLv3\, DSA\, DESede\, DES\, 3DES\, DES40_CBC\, RC4_40\, MD5withRSA\, DH\, 3DES_EDE_CBC\, DHE\, DH keySize < 1024\, EC keySize < 224`

set this property as: (Removing **TLSv1** from the disabled list):

`wrapper.java.additional.9=-Djdk.tls.disabledAlgorithms=MD5\, RC4\, SSLv2Hello\, SSLv3\, DSA\, DESede\, DES\, 3DES\, DES40_CBC\, RC4_40\, MD5withRSA\, DH\, 3DES_EDE_CBC\, DHE\, DH keySize < 1024\, EC keySize < 224`

### Property 2
`wrapper.java.additional.12=-Dhttps.protocols=TLSv1.1\,TLSv1.2`

set this property as:

`wrapper.java.additional.12=-Dhttps.protocols= TLSv1\,TLSv1.1\,TLSv1.2`

![](tls1.png)

-   Restart SEG Service.
