+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email" ]
description = "Boxeron Android fingerprint activation"
title = "Boxer for Android Fingerprint"
+++

-   **VMware Boxer** v 4.5+ for Android 
-   **AirWatch** **Console** 9.0.5+

Following are the steps for fingerprint authentication on the Android Boxer app:

-   In the Apps SDK settings (**Groups & Settings > All Settings > Apps > Settings and Polices > Security Polices**), enable the Biometric Mode.
-   While deploying the Boxer app, enable the Application to use AirWatch SDK and Select the Global SDK for Android.
-   In the Email settings, enable the Application Configuration and enter "**AppForceActivateSSO**" (without the quotes) under Configuration Key and Value Type as **Boolean** and Configuration Type as **True**.
-   Make sure passcode is set as None 
-   Push the boxer app to the device and download it from the Play Store