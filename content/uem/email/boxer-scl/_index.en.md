+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email" ]
description = "Boxer and SCL for iOS and Android"
title = "Boxer and SCL Restrictions"
+++

What needs to be checked to restrict transfer of mail attachments and files transmission between Boxer and SCL for iOS and Android:

1.  To restrict transfer of documents in specific controlled apps, on Organization Group level:
    a. Allow DLP – SCL-step2 functionality
![](SCL-step2.png)

    b.  Search for and add Boxer for iOS/Android – SCL-step3
![](SCL-step3.png)

2. Next step for devices in Organization Group:
   a.  Create a new profile for all devices of a group – SLC-step4.

![](SCL-step4.png)

b.  Turn off "Allow documents from managed sources in unmanaged destinations"  – SCL-step5
![](SLC-step5.png)

1.  To restrict transfer of files from SCL into Boxer the file share, connected to SCL, has to be configured in the Security tab - SCL-step6:

a.  Allow Open in Email = **OFF**
b.  Allow Opent in Third Party Apps = **ON**
![](SCL-step6.png)

2.  Check that in the security profile properties, Enable Composing Email = **No** (SCL-step7).
![](SCL-step7.png)
