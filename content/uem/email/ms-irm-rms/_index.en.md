+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "microsoft", "irm", "rms", "security" ]
description = "Configuring Microsoft IRM-RMS with EMail"
title = "Integration - Microsoft IRM-RMS"
+++

## RMS features in Boxer

According to Boxer User Guide for iOS 4.5.1, Boxer User Guide for Android 4.5.0

### Main features

-   Edit
-   Reply
-   Reply All
-   Forward
-   Copy-Paste
-   Modify recipients
-   Extract
-   Print
-   Export
-   Content Expiry Date

### Other Features

-   Press and hold an email message to copy and paste it into the application.
-   You cannot copy data from the Boxer application and paste anywhere outside the application. However, you can copy data from outside the application and paste into the Boxer application.
-   If your email message has contact number details, tap hold on the number to immediately dial it.
-   If restricted by your administrator, attachments may open through the VMware Content Locker and other AirWatch approved apps. Hyperlinks may open only through the VMware Browser.
-   If configured by your administrator, you can preview emails and their attachments within Boxer (See [Boxer supported files' types](https://digital-work.space/display/AIRWATCH/Matrix)).
-   On the attachment preview screen, the **Share** icon will be unavailable. When tapped on Share icon, you are presented with a toast message "Disabled by your admin".
-   After performing an action on an email while viewing it, you can have Boxer either advance to the next message, the previous message, or return to the conversation list. This setting can be configured from Mail settings (navigate to **Settings > Mail > More mail settings > Auto Advance**).

## RMS Attachments

Boxer does not open RMS-restricted attachments - it transmits them to Content Locker. To use Content Locker on iOS device, the following has to be done:

1) Root certificate must be placed on device  
2) In new iOS version the root Trust has to be **ACTIVATED** in a special menu option

3) In order for Content Locker to access RMS attachments, it must be registered on the ADFS server with this command:

```powershell
Add-AdfsClient -Name "<App name>" -ClientId "<ID name>" -RedirectUri "<RedirectUri>"
```

Example: 
**Client ID** for VMware Content Locker for iOS is **e9fcfce0-a20b-4d34-b580-909332723090**

{{% notice style="tip" %}}
Client ID of application can be found in ADFS logs: every error Content Locker gives while trying to read a RMS-secured attachment is followed by its' current Client ID.
{{% /notice %}}