+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "programming" ]
description = "Powershell Mailbox Sync"
title = "Mailbox sync"
+++

![](allowblock01.jpg)

Sync Mailboxes flow:
1. Sync Mailboxes action is processed by webcosole and sent to MEG Queue for processing.  
2. MEG Queue Service invokes powershell fucntion to retrive all mailboxes.  
3. It then invokes powershell fucntion to retrive all EAS devices.  
4. Mailboxes and devices and reconciled and MEG Queue saves EAS device data to AirWatch database.

## WEB CONSOLE LOG
#### Sync Mailboxes Event processing:
After admin clicks Sync Mailboxes action, webconsole receives event for processing.  
Webconsole writes Sync Mailboxes event to Microsoft Messaging Queue. MEG Queue will read the queue and will process event.
```powershell
2017/03/10 14:42:29.637 MEMCON 3c749df0-c28d-409e-84ac-0a2d29cc5566 [0000068-0000000] (28) Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.Util.DeviceStateChangeTypeMessageQueue.PersistGenericPayload Writing sync eas mailbox payload for LG '627' to queue: 'Name: AWSegCompliance, Protocol: Tcp, Address: .\Private$\, Encoding: Binary, QueueLocation: Local, BulkReadLimit: 1, ConnectionRetryCount: 3, ReadTimeout: -1, RetryInterval: 30, AutoCreate: False, ' 9525ac55-84d8-4ae2-b8e1- b7b183f84afd
```

## MEG QUE LOG
#### Sync Mailboxes event received:
MEG Queue receives Sync Mailboxes event for processing from webconsole.  
Sync Mailboxes operation is initiated.
```powershell
Debug AW.Meg.Queue.Service.Util.EndpointQueueManager._SplitByType Received 'SyncEasDevicesPayload' message. MemConfig Id: '30' Info AW.Meg.Queue.Service.Util.SyncAllMailboxesTask.PerformTask Sync Mailboxes task will initiate for MEMConfig '30'.
```

#### Retrieving Mailboxes:
MEG Queue prepares to retrieve Mailboxes. It prints filter if applicable.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.SyncEasDevices Retrieving CAS mailboxes. MEMConfig: 30  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.SyncEasDevices Sync Result Type : None; No Filter Provided.
```

#### Powershell Admin details:
MEG Queue loads Powershell admin account details. It also loads ACC details if any.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseConfigurationProvider.LoadExchangeServiceConfiguration Loading exchange settings. MEMConfig: 30  

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseConfigurationProvider.LoadExchangeServiceConfiguration Getting service relay configuration. ACC location group overridden by MEM configuration: False. Location group:'627'
```

#### Powershell Session creation:
MEG Queue creates powershell session to execute powershell command.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession Initializing PowerShell session for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession Creating session for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession PowerShell session successfully initialized for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershellusing Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False
```

#### Sending powershell command to Retrieve Mailboxes:
MEQ Queue calls powershell function to Retrieve Mailboxes from exchange. Number of mailboxes retrieved are listed in log as shown.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.ListCasMailboxes Retrieving CAS mailbox list records 1 to 25000 at ExchangeService https://mail-mem13.ssdevrd.com/powershell.  

Debug AirWatch.CloudConnector.ExchangeServices.MailboxManagementService.ListCasMailboxes Retrieving CAS mailbox list records 1 to 25000.  
Debug AirWatch.CloudConnector.Common.PowerShell.CommandHelper.ListCasMailboxes Invoking command 'AW-Get-CASMailboxList'. Endpoint: 'BulkDeviceAction_mem13\svcPSTest@Microsoft.Exchange@https://mail-mem13.ssdevrd.com/powershell'  

Debug AirWatch.CloudConnector.ExchangeServices.MailboxManagementService.ListCasMailboxes Retrieved CAS mailbox list records 1 to 23 of 23.  

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.ListCasMailboxes Retrieved CAS mailbox list records 1 to 23 of 23 at ExchangeService https://mail-mem13.ssdevrd.com/powershell.
```

#### Powershell Session removal:
MEG Queue removes powershell session from memory.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.RemoveSession Removing session for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.RemoveSession Session removed for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False
```

#### Retrieving ActiveSync devices:
MEQ Queue calls powershell function to Retrieve Mailboxes from exchange. Number of devices retrieved are listed in log as shown.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.SyncEasDevices Retrieving ActiveSync devices. MEMConfig: 30  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.SyncEasDevices Sync Result Type : None; No Filter Provided.

Debug AirWatch.CloudConnector.Common.PowerShell.CommandHelper.ListActiveSyncDevices Invoking command 'AW-Get- ActiveSyncDeviceList'. Endpoint: 'BulkDeviceAction_mem13\svcPSTest@Microsoft.Exchange@https://mail-mem13.ssdevrd.com/powershell', PowerShellDeploymentType: '5'.

Debug AirWatch.CloudConnector.ExchangeServices.MailboxManagementService.ListActiveSyncDevices Retrieved ActiveSync device list records 1 to 217 of 217.  
Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.ListActiveSyncDevices Retrieved ActiveSync device list records 1 to 217 of 217 at ExchangeService https://mail-mem13.ssdevrd.com/powershell.
```

#### Reconciling Devices:
MEG Queue compare AirWatch MEM Devices with EAS devices retreived from exchange.  
If EAS device retrived from exchange matches with one of AirWatch device, MEG Queue will update AirWatch MEM Device with latest status. Otherwise new unmanged device record is created.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.SyncEasDevices Reconciling EAS devices with known devices in AirWatch. MEMConfig: 30
```

#### Updating Managed Device:
Below statement shows that there is 1 matched device after reconcilation process.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.ReconcileEasDevices Updating '1' known managed EAS devices. MEMConfig: '30'

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateManagedDevices Finding managed devices from payloads.  
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateManagedDevices Finding unmanaged devices from payloads.

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDevices() Save MEM Device  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDevices() Save MemDevice for ActiveSyncPayload Instance: MEMDeviceId: 5614, LocationGroupID: 627, MEMConfigId: 30, IsManaged: True, EasDeviceIdentifier: 0EANTNG9L56LL8FLD1SFVFNK64, DeviceId: 19, SyncAllowed: True  
, EasDeviceType: iPod, EasDeviceUserAgent: Apple-iPod5C1/1307.36, EasmailboxIdentity: MEM13.ORG/Users/MEM2, EasMailboxDisplayName: MEM2, EmailAddress: MEM2@mem13.ssdevrd.com  
, UserName: MEM2, Command: Reconciled Access State, GatewayHostName:  
, CreateNewUnmanaged: False, UpdateManaged: True, EmailClient: , TimeOfRequest: Friday, March 10, 2017  
, Allowed Reason: AllowedByDefault

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDevices() MEM Devices Saved Successfully Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateManagedDevices Updated managed devices. '1' Updated successfully.
```

#### Updating MemDeviceActivity:
MEG Queue updates MemDeviceActivity record with appropriate status.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceActivity ++UpdateMEMDeviceActivity Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceActivity  
ActiveSyncPayload Instance: MEMDeviceId: 5614, LocationGroupID: 627, MEMConfigId: 30, IsManaged: True, EasDeviceIdentifier: 0EANTNG9L56LL8FLD1SFVFNK64, DeviceId: 19, SyncAllowed: True, EasDeviceType: iPod, EasDeviceUserAgent: Apple-iPod5C1/1307.36, EasmailboxIdentity: MEM13.ORG/Users/MEM2, EasMailboxDisplayName: MEM2, EmailAddress: MEM2@mem13.ssdevrd.com

UserName: MEM2, Command: Reconciled Access State, GatewayHostName:  
CreateNewUnmanaged: False, UpdateManaged: True, EmailClient: , TimeOfRequest: Friday, March 10, 2017
Allowed Reason: AllowedByDefault

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() Saving MEM Device Activity  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() MEM Device Activity Saved Successfully... 1
```

#### Updating MemDeviceConfig:
MEG Queue updates MemDeviceConfig record with appropriate status.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig MemDeviceConfig for Managed Device: MemDeviceId: 5614, MemConfigId: 30  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig Total Mem Device Config Records Count: 1

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDeviceConfig ++SaveMemDeviceConfig
```

#### Creating unmanaged devices:
If EAS device retrived from exchange do not match with one of AirWatch device, MEG Queue will create unmanaged AirWatch MEM Device.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.ReconcileEasDevices Updating '216' discovered unmanaged EAS devices. MEMConfig: '30'  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateDeviceStatus Updating access state for '216' devices in database.
```

#### Creating unmanaged MemDevice:
MEG Queue creates unmanaged MEMDevice records as shown below.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.CreateUnmanagedMEMDevices ++CreateUnmanagedMEMDevices

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDevices() Save MemDevice for ActiveSyncPayload Instance: MEMDeviceId: 0, LocationGroupID: 627, MEMConfigId: 30, IsManaged: False, EasDeviceIdentifier: boxer1485882972781, DeviceId: 0, SyncAllowed: True  
, EasDeviceType: Android, EasDeviceUserAgent: AirWatch Boxer (Nexus 6P; Android 6.0.1) Version 4.1.0.12/352, EasmailboxIdentity: MEM13.ORG/Users/TBurgess, EasMailboxDisplayName: TBurgess, EmailAddress: TBurgess@mem13.ssdevrd.com

, UserName: TBurgess, Command: Discovered EAS Device, GatewayHostName:  
, CreateNewUnmanaged: True, UpdateManaged: False, EmailClient: , TimeOfRequest: Friday, March 10, 2017 , Allowed Reason: AllowedByDefault
```

#### Creating unmanaged MEMDeviceActivity:
MEG Queue creates unmanaged MEMDeviceActivity records as shown below.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceActivity ++UpdateMEMDeviceActivity Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceActivity  
ActiveSyncPayload Instance: MEMDeviceId: 5748, LocationGroupID: 627, MEMConfigId: 30, IsManaged: False, EasDeviceIdentifier: boxer1485882972781, DeviceId: 0, SyncAllowed: True

, EasDeviceType: Android, EasDeviceUserAgent: AirWatch Boxer (Nexus 6P; Android 6.0.1) Version 4.1.0.12/352, EasmailboxIdentity: MEM13.ORG/Users/TBurgess, EasMailboxDisplayName: TBurgess, EmailAddress: TBurgess@mem13.ssdevrd.com  
, UserName: TBurgess, Command: Discovered EAS Device, GatewayHostName:  
, CreateNewUnmanaged: True, UpdateManaged: False, EmailClient: , TimeOfRequest: Friday, March 10, 2017

, Allowed Reason: AllowedByDefault

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() Saving MEM Device Activity  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() MEM Device Activity Saved Successfully... 100

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() MEM Device Activity Saved Successfully... 200  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMEMDeviceActivities() MEM Device Activity Saved Successfully... 216

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceActivity Saved device activities. '216' Saved successfully.
```

#### Creating unmanaged MEMDeviceConfig:
MEG Queue creates MEMDeviceConfig records for unmanaged devices as shown below.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig Collecting Managed MEM Device Config Records.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig Collecting Unmanaged MEM Device Config Records.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig MemDeviceConfig for Unmanaged Device: MemDeviceId: 5748, MemConfigId: 30

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig MemDeviceConfig for Unmanaged Device: MemDeviceId: 5762, MemConfigId: 30

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMEMDeviceConfig Total Mem Device Config Records Count: 216
```

#### Discovered Mail Clients:
MEQ Queue collects all mail client names and saves in AirWatch database. These mailclients are presented to user for selection in Mail Client policy for configuring policy rules.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Finding new mail clients.  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.MailClientDataHandler Look for mail client list in cache.  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.MailClientDataHandler Mail client list not found in cache. Loading from database.  
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Found '17' new mail clients.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Saving new mail clients.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Save mail client successful for LG : '627'  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Saved new mail clients. '1' Saved successfully. '0' had errors.
```

#### Discovered User Accounts:
MEQ Queue collects all User Accounts and saves in AirWatch database.  
These User Accounts are presented to user for selection in User policy for configuring policy rules.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Finding new email account users.  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.ActiveSyncDataHandler.LoadAccountUserNames Look for account user list in cache.  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.ActiveSyncDataHandler.LoadAccountUserNames Account user list not found in cache. Loading from database.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Found '2' new email account users.  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Saving new email account users.  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.ActiveSyncDataHandler.BulkSaveAccountUserName Saving Account User(s)...  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmailGateway.ActiveSyncDataHandler.BulkSaveAccountUserName Saved Account User(s), index: 2  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Saved new email account user for location group: '627'  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmailGateway.ActiveSyncBusiness Saved new email account users. '1' Saved successfully. '0' had errors.
```

Finished:
```powershell
Info AW.Meg.Queue.Service.Util.SyncAllMailboxesTask.PerformTask Sync Mailboxes task finished for MEMConfig '30'.
```

## Log Files
[aw.meg.queue.service-syncmailboxes.log](./files/aw.meg.queue.service-syncmailboxes.log)
