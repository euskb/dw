+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "programming" ]
description = "Powershell Allowlist and Blacklist"
title = "Allowlist and Blacklist"
+++

![](allowblock01.jpg)

## ADMIN ACTIONS (BLOCKLIST)
#### Actions from Email dashboard:
1. User selects device and clicks allowlist/ blocklist action  
2. Meg Queue Service sendsends allowlist/ blocklist powershell command to Exchange server appropriately 3. Meg Queue Service updates database to show your device status on email dashboard

### Webconsole Log
#### Blocklist event:
After admin click Blocklist action for device, webconsole receives blocklist event for processing. Log prints device properties as described below:

**MEMConfig** - Email Settings used
**Device Count** - Total number of devices blocklisted
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.PeristDeviceAccessStateChanged Received device access state change. MEMConfig: 48 Device Count: 1 935c3f93-3f33-49fb-b6c9-a07d0bcc8619
```

#### Event written to Microsoft Messaging Queue:
Webconsole writes blocklist event to Microsoft Messaging Queue. MEG Queue will read the queue and will process event.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.Util.DeviceStateChangeTypeMessageQueue.PersistGenericPayload Writing change device access state payload for LG '646' to queue: 'Name: AWSegCompliance, Protocol: Tcp, Address: .\Private$\, Encoding: Binary, QueueLocation: Local, BulkReadLimit: 1, ConnectionRetryCount: 3, ReadTimeout: -1, RetryInterval: 30, AutoCreate: False, ' 935c3f93-3f33-49fb-b6c9-a07d0bcc8619
```

## MEG Que Log
#### Blocklist event received:
MEG Queue receives blocklist event for processing from webconsole. Device properties are printed in log identifying your device as shown below.

MemConfig Id - Email Settings used
MEMDevice Id - Email Device Record Id
number of devices - Total number of devices blocklisted
EasDeviceIdentifier - Exchange Device ID
User - Email user
AccessLevel - Email access status
Reasons - Reason for allow\\block device
Lg - Location Group ID
Device Id - AirWatch device id

```powershell
Debug AW.Meg.Queue.Service.Util.EndpointQueueManager._SplitByType Received 'DeviceAccessChangedPayload' message. MemConfig Id: '48', MEMDevice Id: '5744' 

Debug AW.Meg.Queue.Service.Util.EndpointQueueManager._ReceiveDeviceAccessChangedPayload Received 'DeviceAccessChangedPayload' message. MemConfigId: '48', number of devices: '1'.

Debug AW.Meg.Queue.Service.Processors.Office365DeviceAccessChangedProcessor.ProcessPayload Device access state changed. Device Id: '29'. Process ActiveSync command. Lg: '646', EasDeviceIdentifier: 'R4SQG79G556LPB3LFEVP66VO98', User: 'airwatchqa@airwatchpm.onmicrosoft.com', AccessLevel: 'Blocked', Reasons: Device is blacklisted: 29  

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.SetBulkEasDeviceAccess BulkDeviceRequest - MemConfigId: 48, IsRunCompliance: False.
```

#### Powershell Admin details:
MEG Queue loads Powershell admin account details.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.DoSetBulkEasDeviceAccess Loading Exchange settings for MEMConfig: 48  

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseConfigurationProvider.LoadExchangeServiceConfiguration Loading exchange settings. MEMConfig: 48
```

#### Powershell Session creation:
MEG Queue creates powershell session to execute powershell command.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession Initializing PowerShell session for Microsoft.Exchange @ PowerShell endpoint https://ps.outlook.com/powershell using Authentication type: Basic,  
User: airwatchadmin@airwatchpm.onmicrosoft.com, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession Creating session for Microsoft.Exchange @ PowerShell endpoint https://ps.outlook.com/powershell using Authentication type: Basic, User: airwatchadmin@airwatchpm.onmicrosoft.com, using service credentials: False, ViewEntireForest enabled: False
```

Blocklisting device:
MEQ Queue sends powershell block command to exchange so that email requests will be blocked.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.CommandHelper.SetActiveSyncDeviceIds Invoking command Set-CASMailbox -Identity 'airwatchqa@airwatchpm.onmicrosoft.com' -ActiveSyncBlockedDeviceIDs @{Add='R4SQG79G556LPB3LFEVP66VO98'} against the  
endpoint: SingleDeviceActionWithNormalPriority_airwatchadmin@airwatchpm.onmicrosoft.com@Microsoft.Exchange@https://ps.outlook.com/powershell
```

#### Powershell Session removal:
MEG Queue removes powershell session from memory.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession PowerShell session successfully initialized for Microsoft.Exchange @ PowerShell endpoint https://ps.outlook.com/powershell using Authentication type: Basic,  
User: airwatchadmin@airwatchpm.onmicrosoft.com, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.RemoveSession Removing session for Microsoft.Exchange @ PowerShell endpoint https://ps.outlook.com/powershell using Authentication type: Basic, User: airwatchadmin@airwatchpm.onmicrosoft.com, using service credentials: False, ViewEntireForest enabled: False
```

#### Database Update:
MEM Device Activity is saved to database so that Email List view reflects change.
```powershell
Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDeviceActivitiesForPSRunCompliance() MEM Device Activity Saved Successfully... 1
```

## Log files
[aw.meg.queue.serviceblacklist.log](./files/aw.meg.queue.serviceblacklist.log)
[weblogfile-blacklist.txt](./files/weblogfile-blacklist.txt)
