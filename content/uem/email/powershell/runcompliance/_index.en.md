+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "programming" ]
description = "Powershell Run Compliance"
title = "Run Compliance"
+++

![](allowblock01.jpg)

## WEBCONSOLE LOG
#### Run Compliance Event processing:

After admin clicks Run Compliance action, webconsole receives event for processing.  
Webconsole writes Sync Mailboxes event to Microsoft Messaging Queue. MEG Queue will read the queue and will process event.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.PersistDeviceStateChangeByLg Received state change event. LG: 627 Type: PolicyPublish 6e76d060-02d6-4515-ab6d-1f90ff41ec1b  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.Util.DeviceStateChangeTypeMessageQueue.PersistGenericPayload Writing device state change payload for LG '627' to queue: 'Name: AWSegCompliance, Protocol: Tcp, Address: .\Private$\, Encoding: Binary, QueueLocation: Local, BulkReadLimit: 1, ConnectionRetryCount: 3, ReadTimeout: -1, RetryInterval: 30, AutoCreate: False, ' 6e76d060-02d6-4515-ab6d-1f90ff41ec1b
```

## MEG QUE LOG
#### Run Compliance event received:
MEG Queue receives Run Compliance event for processing from webconsole. Run Compliance operation is initiated.
```powershell
Debug AW.Meg.Queue.Service.Util.EndpointQueueManager.Process A message is added to the queue for endpoint 'https://mail- mem13.ssdevrd.com/powershell-.-mem13\svcPSTest', for MemConfig Id: '30'.  

Debug AW.Meg.Queue.Service.Util.EndpointQueueManager._SplitByType Received 'DeviceStateChangePayload' message. MemConfig Id: '30', Device Id: '', StateChangeType: 'PolicyPublish'  

Debug AW.Meg.Queue.Service.Processors.Office365DeviceStateChangedProcessor.DoProcess Processing 'PolicyPublish' event for LG '627', Device Id: '', MEM Config Id: '30'
```

#### Powershell Admin details:
MEG Queue loads Powershell admin account details. It also loads ACC details if any.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseConfigurationProvider.LoadExchangeServiceConfiguration Getting service relay configuration. ACC location group overridden by MEM configuration: False. Location group:'627'  

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseConfigurationProvider.LoadExchangeServiceConfiguration Loaded exchange settings. MEMConfig: 30

Debug WanderingWiFi.AirWatch.BusinessImpl.EnterpriseIntegrationHelper.TestExchangeConnection Load Exchange service configuration for location group 627
```

#### Policy Evaluation:
MEG Que evaluates policy and determine all devices that needs access state change (allow or block). Below log shows example of device evaluated to be blocked by user policy.
```powershell
Debug AW.Meg.Queue.Service.Util.RunComplianceTask.ProcessUpdateDevicePolicies Total 1 policies retrieved from database for the process.  

Debug AW.Meg.Queue.Service.Util.RunComplianceTask.ProcessUpdateDevicePolicies Total 1 mailboxes were found with known devices. Debug AW.Meg.Queue.Service.Util.RunComplianceTask.ProcessUpdateDevicePolicies Known Policy: EasDeviceIdentifier 0EANTNG9L56LL8FLD1SFVFNK64, MemDeviceId: 5614, Allowed: False, MailboxIdentity: , LastCommand: Mail Server Update, DeviceAccessStateReason: , LastMailAccessAllowed: True

Debug AW.Meg.Queue.Service.Util.RunComplianceTask.ProcessUpdateDevicePolicies Evaluated device. DeviceId: '19', EasDeviceIdentifier: '0EANTNG9L56LL8FLD1SFVFNK64', Allowed: 'False', Reason(s): Account user mem2 is blocked
```

#### Powershell Session creation:
MEG Queue creates powershell session to execute powershell command.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession Initializing PowerShell session for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.InitializeSession PowerShell session successfully initialized for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershellusing Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False
```

#### Blacklisting device:
MEQ Queue sends powershell block command to exchange so that email requests will be blocked.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.CommandHelper.SetActiveSyncDeviceIds Invoking command Set-CASMailbox -Identity 'mem2@mem13.ssdevrd.com' -ActiveSyncBlockedDeviceIDs @{Add='0EANTNG9L56LL8FLD1SFVFNK64'} against the endpoint: BulkDeviceAction_mem13\svcPSTest@Microsoft.Exchange@https://mail-mem13.ssdevrd.com/powershell
```

#### Powershell Session removal:
MEG Queue removes powershell session from memory.
```powershell
Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.RemoveSession Removing session for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False  

Debug AirWatch.CloudConnector.Common.PowerShell.SessionHelper.RemoveSession Session removed for Microsoft.Exchange @ PowerShell endpoint https://mail-mem13.ssdevrd.com/powershell using Authentication type: Basic, User: mem13\svcPSTest, using service credentials: False, ViewEntireForest enabled: False
```

#### Updating AirWatch Database:
After successfully blocking device, AirWatch database is updated with current status.
```powershell
Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMemDeviceActivitiesForPSRunCompliance ++UpdateMemDeviceActivitiesForPSRunCompliance  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMemDeviceActivitiesForPSRunCompliance Total 1 MEMDeviceActivity records will be updated.

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDeviceActivitiesForPSRunCompliance()

2 of 3 19.07.2022, 13:11

VMWare Workspace ONE MEM Team - PowerShell - Run Compliance https://onevmw.sharepoint.com/teams/VMWareAirWatchMEM/SitePage...

++SaveMemDeviceActivitiesForPSRunCompliance  
Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDeviceActivitiesForPSRunCompliance() MEM Device Activity Saved Successfully... 1  

Debug WanderingWiFi.AirWatch.ProviderImpl.MobileEmail.MobileEmailDataHandler.SaveMemDeviceActivitiesForPSRunCompliance() --SaveMemDeviceActivitiesForPSRunCompliance  

Debug WanderingWiFi.AirWatch.BusinessImpl.MobileEmail.MobileEmailBusiness.UpdateMemDeviceActivitiesForPSRunCompliance --UpdateMemDeviceActivitiesForPSRunCompliance
```

Finished:
```powershell
Info AW.Meg.Queue.Service.Util.SyncAllMailboxesTask.PerformTask Run compliance task finished for MEMConfig '30'.
```

## Log files
[aw.meg.queue.service-runcompliance.log](./files/aw.meg.queue.service-runcompliance.log)
[weblogfile-runcompliance.txt](./files/weblogfile-runcompliance.txt)