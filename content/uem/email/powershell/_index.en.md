+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "programming" ]
description = "Powershell commands for troubleshooting EMail"
title = "Powershell for MEM"
+++

## Linked Articles
{{% children sort="weight" description="true" %}}

## EMail Architectures
![](architectures.jpg)

## Common Powershell Commands
#### Initializing a Session
This command is used for AirWatch to initialize a session. The two parameters required as the $creds and the PowerShell endpoint.
```powershell
> $cred = Get-Credential

> $session = New-PSSession –ConfigurationName Microsoft.Exchange -ConnectionUri “https://<mailserver>/powershell” –Credential  
$cred –Authentication Basic –AllowRedirection

> Import-PSSession $session
```

#### Look at a user's basic mailbox information

This command pulls basic information about a mailbox using an email address as the identity. 
```powershell
> Get-CASMailbox –identity “userguy” | fl
```
  
#### Viewing a user's list of devices
This command will list each device partnered with the CasMailbox. 
```powershell
> Get-ActiveSyncDevice –mailbox “userguy” | fl (2010)  
> Get-MobileDevice –mailbox “userguy” | fl (2013+)  
```

#### Additional device information
WS1 UEM does not pull from this listing, however, you can find some additional details (ex: when the device last synced) from this table.
```powershell
> Get-ActiveSyncDeviceStatistics –mailbox “userguy” | fl
```

#### Setting ActiveSync Devices to Allowed/Blocked

This is the form of a cmdlet used to issue an Allow/Block command to Exchange. This will insert "DeviceIDX" into the appropriate list.
```powershell
> Set-CasMailbox –identity “userguy” –ActiveSyncAllowedDeviceIDs @{Add = “DeviceId1”} 
> Set-CasMailbox –identity “userguy” –ActiveSyncBlockedDeviceIDs @{Add = “DeviceId2”}
```

#### Selecting specific information or exporting data

This command is helpful when comparing AirWatch data to Exchange data. 
```powershell
> Get-ActiveSyncDevice –ResultSize Unlimited | Select-Object  
DeviceID, DistinguishedName, DeviceType | Export-CSV  
ASD_selection.csv
```

## WS1 UEM with Office 365

Disable the native access in O365 -> redirect to WS1 UEM 
First-time access will be denied, PowerShell command will be sent to O365 to whitelist the device, 2-3min later the email will flow

![](architecture_o365.jpg)

Set WS1 UEM as IDP to control other ways of accessing (Exchange Web Access, OWA etc)

This lacks some features (encrypt attachments, strip attachments etc), but can be mitigated using Boxer Needs ESC between Cloud AW and On-Prem Exchange

AW-PS Service Account  
Remote Shell access to the Exchange Server associated mailbox on the server to issue remote commands

Required PowerShell roles: Mail Recipients
Organization Client Access Recipient Policies
Settings --> Email --> Email Settings Configure - Direct

Features:
-   Configure email over-the-air
-   Block unmanaged devices
-   Discover existing unmanaged devices
-   Require device encryption
-   Prevent compromised devices
-   Block mail client, user, device model or OS
-   Integrate or revoke certificates

