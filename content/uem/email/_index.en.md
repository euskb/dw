+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email" ]
description = "Integration with EMail Section"
title = "Integration - EMail"
+++

## External Links

-   ENSv2 with SEGv2 Architecture - [https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/services/WS1_ENS2_Doc/GUID-9555A362-174D-4689-8A67-8850EE279EC4.html]
-   ENS Pre-Requisites - https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/services/WS1_ENS2_Doc/GUID-AWT-REQUIREMENTS-ENSV2.html
-   ENSv2 Deep Dive (by Jon Towles) - [https://mobile-jon.com/2019/06/26/taking-a-closer-look-at-ensv2/](https://mobile-jon.com/2019/06/26/taking-a-closer-look-at-ensv2/)
-   (Old) Boxer for iOS Deep Dive (by Jon Towles) - [https://mobile-jon.com/2017/09/03/boxer-ios-deepdive/](https://mobile-jon.com/2017/09/03/boxer-ios-deepdive/)

## Linked Articles

{{% children sort="weight" description="true" %}}

![EMail Integration Schema](email_schema.png)
