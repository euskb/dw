+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg" ]
description = "SEG enforcing and SEG as MS Exchange OWA Proxy"
title = "SEG on Windows Proxy"
+++

### External Link
SEG Guide - [VMware AirWatch SEG Guide](https://resources.air-watch.com/view/497tbx9mjvzgqwkhvyqk/en)

## Prevent usage of Native EMail on enrolled devices

**Problem:** Users can gain access to Exchange ActiveSync from uncontrolled devices and mail clients on them. Usage of SEG solves the problem of uncontrolled devices access.

You can enforce using Boxer/Inbox by creating an email compliance policy from the AirWatch console:

**Email> Compliance Polices > General Email Policies > Mail Client**

## SEG as MS Exchange OWA Proxy
{{% notice style="warning" %}}
Article is for OLD separate SEG. NOT about SEG on UAG.
{{% /notice %}}

You can restrict mobile traffic to seg.company.com by installing IP and Domain restrictions on the IIS on the Exchange server, and then enable IP filtering to deny everyone but the SEG on the ActiveSync endpoint on IIS. This will ensure all enrolled mobile devices will access email through SEG. You can also implement email policies to ensure that unmanaged devices do not access the SEG.  
  
AirWatch cannot block access to OWA for unenrolled mobile devices since **SEG does not manage OWA**. The only way to do so would be checking through the AD for unenrolled users and preventing them from webmail access from there.

{{% notice style="note" %}}
OWA traffic can be routed through the SEG however it will act as a simple pass through.
{{% /notice %}}

{{% notice style="warning" %}}
The OWA through SEG & proxying Webmail through SEG is not a supported setup as it could lead to a single point of failure for email access.
{{% /notice %}}

### SEG Java Keystore
{{% notice style="warning" %}}
Article is for OLD separate SEG. NOT about SEG on UAG.
{{% /notice %}}

Default password for SEG Java Key store = **changeit**

## SEG on Windows Java Memory
{{% notice style="warning" %}}
Article is for OLD separate SEG. NOT about SEG on UAG.
{{% /notice %}}
Zulu is the new Java Corporate middleware.

### Resolution

-   Upgrade to latest version of SEG (2.18+);
-   Set the max heap size to 5Gb;
-   Use Shenandoah as the garbage collection method.

Follow these steps to apply the settings:  
1. Stop SEG service;  
2. Go to SEG install directory and edit file **SecureEmailGateway-2.18/service/conf/segServiceWrapper.conf**  
3. Update max heap to 5Gb, look for "**Xmx**" and update the property to: 

**wrapper.java.additional.3=-Xmx5120m**

4. Use Shenandoah GC, look for "**#wrapper.java.additional.38**" and in the next line add: 

**wrapper.java.additional.39=-XX:+UseShenandoahGC**  
**wrapper.java.additional.40=-Xlog:gc*=debug:file=tmp/gc-%p-%t.log:time,level,tags:filecount=10,filesize=50m**

5. Save file and start the SEG service. 

-   Observe the system resources once this change is placed + enable GC logs in the above settings.

If no issue is seen, then remove the GC logging setting by following these steps:   
1. Stop SEG service.   
2. Go to SEG install directory and edit file **SecureEmailGateway-2.18/service/conf/segServiceWrapper.conf**  
3. Remove line: 

**wrapper.java.additional.40=-Xlog:gc*=debug:file=tmp/gc-%p-%t.log:time,level,tags:filecount=10,filesize=50m**

4. Save and start SEG service.

## SEG Clustering

If multiple SEG servers are load balanced, single policy broadcast messages apply to only one SEG. This includes the messages sent from the AirWatch Console to SEG upon enrollment, compliance violation, or correction. Use Delta Sync with a refresh interval of ten minutes to facilitate newly enrolled or compliant devices. These devices experience a waiting period of maximum ten minutes before email begins to sync. Benefits of this approach include:

-    Updated policies from the same API source for all SEG servers. 
-   Smaller performance impact on API server.  
-   Reduced implementation or maintenance complexity compared to the SEG clustering model.   
-   Fewer failure points as each SEG is responsible for its own policy sets.
-    Improved user experience.   
     

 SEG Clustering is also available to facilitate the sharing of single policy updates to all nodes of a SEG cluster.