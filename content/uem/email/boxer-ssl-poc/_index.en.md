+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email" ]
description = "Boxer setup for PoC"
title = "Boxer and SSL for PoC"
+++

Boxer has 2 types of accounts:
-   **main** – is being added on install, is usually configured with policies. **Cannot be deleted**.
-   **additional** – is added by users if this is allowed by policies. **Can be configured manually.**

{{% notice style="warning" %}}
(15.08.2017) For policies which are being distributed centrally for the main account there is no possibility to configure "Ignore SSL errors". If in a PoC of Boxer you have to connect to a private/test mail server, this may pose a problem.
{{% /notice %}}

**Variant 1** (recommended): server is signed by a cert, which was given by a inner CA (issued by =/= issued to)

**Solution**: on all devices in the test, you have to manually deploy the certificate chain in the trusted section:

**iOS**: [http://longwhiteclouds.com/2013/01/03/installing-corporate-ca-certificates-on-iphone-or-ipad-for-use-with-vmware-view/](http://longwhiteclouds.com/2013/01/03/installing-corporate-ca-certificates-on-iphone-or-ipad-for-use-with-vmware-view/)

**Android**: [https://support.google.com/nexus/answer/2844832?hl=en](https://support.google.com/nexus/answer/2844832?hl=en)

{{% notice style="warning" %}}
In Android 7.0+, by default, apps don't work with CA certificates that you add. But app developers can choose to let their apps work with manually added CA certificates. [https://support.google.com/nexus/answer/2844832?hl=en](https://support.google.com/nexus/answer/2844832?hl=en)
{{% /notice %}}

**Variant 2**: server is signed by a self-signed cert (issued by == issued to)

**Solution**: use the first account to distribute policies from any public EMail. For example, Office365 from our VMTestDrive

{{% notice style="note" %}}
Account can be only one for all devices - this account will not be used
{{% /notice %}}

1.  After **Boxer** installation connect to the account provided by policies (VMTestDrive)
2.  Choose **Add Account ("Добавить аккаунт")** in Boxer left menu (IMG_1455.jpg)
3.  Insert an address (IMG_1456.jpg) and later choose **Manual configuration** (**"Ручная настройка"**). Type -> Exchange Server.
4.  See config in IGM_1457.jpg ang IGM_1458.jpg as an example of such a connection. It is important to choose **SSL (Accept any certificates / "принимать любые сертификаты")**.

{{% notice style="warning" %}}
After adding the account first time, Inbox may sync for a long time (>30min)
{{% /notice %}}

5.  Choose the default account (IMG_1459.jpg)

![](IMG_1455.jpeg)![](IMG_1456.jpeg)

![](IMG_1457.jpeg)
![](IMG_1458.jpeg)

![](IMG_1459.jpeg)
