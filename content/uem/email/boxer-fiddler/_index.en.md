+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "troubleshooting" ]
description = "Boxer troubleshooting with Fiddler"
title = "Boxer via Fiddler"
+++

From time to time it may be useful to troubleshoot Boxer, Inbox, and native mail issues using Fiddler. Fiddler will let you log all of the traffic between the device and the email server/SEG, etc, end-point. This is useful in proving that communication is happening and that some errors are being generated externally from the device (401, 403, etc...).

Configure the Windows 10 computer (which will act as the proxy) and an example Android device.

- Set your Windows 10 computer up as a mobile hotspot. On Windows 10, click on the start menu and type in "Mobile Hotspot". 
- Select "Change mobile hotspot settings"
![](fiddler01.jpg)

• Configure your hotsptot settings and turn on the hotspot.

![](fiddler02.jpg)
- Download and install Fiddler from https://www.telerik.com/fiddler  
- In Fiddler, go to Tools\\Options make these changes. See HTTPS sub-option
![](fiddler03.jpg)

- Make these changes on the Connections sub-heading
![](fiddler04.jpg)

- You will need your Proxy servers IP address to enter on the mobile device for later. To get this hover over the "online" option and view the list of IP addresses. In this example, I'll be using 10.84.145.96.
![](fiddler05.jpg)

## Android

- On the mobile device, go to your wireless settings and long tap the right-hand side of the connection you want to connect to. This will by the wireless connection you set up on your computer. On Android, click on "Modify Network".
![](fiddler06.jpg)

- Enter the proxy hostname and proxy port information. The port will be 8888 if you kept the default settings for Fiddler.
![](fiddler07.jpg)

## iOS

On iOS use the following process to set the proxy

How to configure your iPad/iPhone proxy settings

1. Start the iPhone/iPad.  
2. Tap on the Settings app. ...  
3. Tap on the Wi-Fi settings category. ...  
4. You will now be at the Wi-Fi network settings screen for the connected network. ... 5. Tap on the Manual button. ...  
6. When you are done setting up your proxy server, tap on the Wi-Fi Networks button.

On the mobile device, go to the IP address of the proxy followed by :8888/fiddler. See the example below. Click on "FiddlerRoot Certificate" to download and install the certificate.
![](fiddler08.jpg)

• In the Fiddler application, click on Tools\\WinNet Options.
![](fiddler09.jpg)

- Click on "Lan settings" and uncheck the "Use a proxy server..." from the following window.
![](fiddler10.jpg)

- Click OK and OK. This will show a yellow bar on the Fiddler application indicating that it's not collecting any traffic from the local computer in the logs. For your test, you will only want device traffic.
- You can clear the current logs by doing a CTRL-A and selecting everything. 
- Now you can begin your replication testing.
