+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "API", "REST", "python" ]
description = "WS1 UEM REST API config and usage"
title = "REST API"
+++

```
### URI

Old API URI example: https://host/api/v1/mam/apps/public/{applicationid}/addsmartgroup/{smartgroupid}

New API URI example: https://host/api/mam/apps/public/{applicationid}/smartgroup/{smartgroupid}
```

### External links

-   REST API Guide - [https://resources.workspaceone.com/view/zv5cgwjrcv972rd6fmml/en](https://resources.workspaceone.com/view/zv5cgwjrcv972rd6fmml/en)
-   REST APIv1 dropped functions - [https://resources.workspaceone.com/view/6z89m664plrjdjjr7fcb/en](https://resources.workspaceone.com/view/6z89m664plrjdjjr7fcb/en)
-   How to use VMware AirWatch API Explorer: https://kb.vmware.com/s/article/2960676?lang=en_US
-   REST API Check tools:
    -   Postman REST Client - [https://www.getpostman.com/](https://www.getpostman.com/)
    -   Cocoa REST Client - [https://github.com/mmattozzi/cocoa-rest-client](https://github.com/mmattozzi/cocoa-rest-client)
-   Base64 encoder web tool: [https://www.base64encode.org/](https://www.base64encode.org/)

## Attached Documents

[REST API SDK Guide](./_index.en.files/170620_RESTAPI_SDK_AppConfig.pdf)
[Deprecated REST API update](./_index.en.files/Deprecated_API.pdf)
[Main REST API Guide](./_index.en.files/REST_API_9-1.pdf)
[VMware AirWatch Peripheral API Guide](./_index.en.files/VMware_AirWatch_API_Peripheral_Guide-v9-1.pdf)
[VMware AirWatch API Programming Quick Start Guide](./_index.en.files/API_Programming_Quick_Start_Guide-v9-1.pdf)

## Articles in section

{{% children sort="weight" description="true" %}}

### Preparation

You need a user account to run scripts in AirWatch REST API. Create a local account, take its' credentials: **domain\login:password**, and go encode it at [https://www.base64encode.org/](https://www.base64encode.org/)

For example,  `'lab\restguy:P@ssw0rd' ==> 'bGFiXHJlc3RndXk6UEBzc3cwcmQ='._`

Turn API access ON for this account in the AirWatch console **System -> Advanced -> API -> REST**, we also should get an API Key for it.

{{% notice style="warning" %}}
-   AirWatch requires all requests to be made over SSL.
-   AirWatch limits the number of API requests that can be made during certain time periods. Rate limiting is done per Organization Group (OG) based on the API key used in the request.

        **Server Throttling** - The limit imposed on an Organization Group for a 1 minute interval.  
        **Daily Quota** - The limit imposed on an Organization Groupvfor a 24-hour interval.
{{% /notice %}}

### Local REST API Help

REST API Help is built into the AirWatch API Server host, go to link: **https://<airwatch api server domain name>/api/help/**

### Examples of using REST API
Let's take some device from those enrolled in AirWatch and check out the apps list on it.
```python
import requests
consoleURL = 'https://airwatch.example.com'
b64EncodedAuth = 'bGFiXHJlc3RndXk6UEBzc3cwcmQ='
API_Key = 'S390lvIVeFRBSO4InP73MFG3YSFDNKL+BXKW7Wv5Wwo='
basic_headers={"Authorization": "Basic " + b64EncodedAuth, "aw-tenant-code": APItenantCode,"Accept": "application/json"}
 
appsTest = requests.get(consoleURL + "/api/mam/apps/internal/46", headers=basic_headers)
print(appsTest.json())
```

46 is the device ID inside AirWatch. How do you find the ID of a device? - simplest way to do this is open AirWatch console, device list page, and hover your mouse over any device name: you will see the ID in the browser URL status bar. Now, let's see where the device has been for the last 2 days:
```python
gps2days = requests.get(consoleURL + "/api/mdm/devices/46/gps?dayrange=2", headers=basic_headers)
print(gps2days.json())
```
If there are any GPS points recorded for the device, we will get a list of them here. How can we use this? For example, customers say they hate Bing Maps embedded in AirWatch console. So we can build a little portal for searching devices, embed Google Maps into it and use the coordinates list to draw points.

One step deeper: suppose we need to show a customer how GPS coordinates are being collected, but the device enrolled is fresh, did not catch any sattelites yet or has problems with GPS module. Since GPS data is actually stored and taken from the SQL database, we can insert some coordinates there manually and see them right away on the Bing map in the console. See article on [inserting false GPS history for a device in SQL section](uem/SQL/index.html)
  
Last example in this article I got from a cool client. They give out corporate iPhones to their employees, and all of those devices are supposed to be supervized. Only supervized devices are to be enrolled in AirWatch and get corp data, no personal gadgets coming through! AirWatch does have a tag for Apple device status (is it Supervized/DEP/Education?), but uses it only for reporting - there is no Compliance rule or filtering around this currently (a ticket in Jira on the topic is promised to be closed in AirWatch 9.6+). So the client gets a list of all enrolled devices and filters them manually:

```python
import json
devicelist = requests.get(consoleURL + '/api/mdm/devices/search', headers=basic_headers)
for i in devicelist.json()["Devices"]:
    if i["Platform"] == "Apple" and i["IsSupervised"] == False and i["EnrollmentStatus"] == "Enrolled":
        # Tag the toxic devices, or enterprise wipe them
```

By running this script every half-hour, all unsupervised devices are Enterprise Wiped shortly after they are enrolled.

