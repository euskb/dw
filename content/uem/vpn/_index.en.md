+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "vpn", "network" ]
description = "Solving issues with VPN"
title = "VPN"
+++

## Articles in section

{{% children sort="weight" description="true" %}}

## Troubleshooting Per-App-VPN 
Check device profile has VPN section and Per-App-VPN enabled:
![](perapp1.png)
View profile in XML, record VPN profile unique identifier (**VPNUUID**):
![](perapp2.jpg)

Verify the Application has “Use Per-App VPN” enabled:
![](perapp3.png)
Verify on device: check-in command is delivered to device, device receives **App Install** command:
![](perapp4.jpg)

-   Verify on device: Device receives Managed Application Attributes command
-   Managed Application Attributes command links app with VPN profile
![](perapp5.jpg)

Verify on device: (on iOS) **Settings → General → Profiles & Device Management → MDM Profile → Apps**  
Application displays VPN information under Device Management settings: ”App will use a VPN for all network access”

Device Services (DS) server (front-end communication with managed device) logs:
-   DeviceServicesLogs (C:\\AirWatch → Logs → DeviceServices)

Search for:
-   ApplicationAttributes
-   App Bundle ID
-   VPNUUID

## Troubleshooting Windows 10 Tunnel Client
### Client is unable to connect to Tunnel Server
-   Check Network connection
-   Check DNS
-   Check Configuration of Tunnel Server
-   Check Firewall configuration - rules denying outbound session
-   Check Tunnel Server port with telnet/curl

### No applications configured 
-   Add the application to the UEM Console   
    -   Configure the Tunnel.
    -   Add application details (Example: add chrome and Firefox ) and DTR rules from that added application to Block, Tunnel ,By Pass or Proxy and provide destinations ( like *company-site.com)
    -   Set default rule action to the Tunnel.
    -   Create a user VPN profile and publish it to the device.
-   Check for application to be whitelisted in DTR, if not then add it with proper spelling/format
-   Check the logs for registration status of application

### No Traffic Rules configured
-   Check for addition of application in Device Traffic Rule configuration in **Windows Registry**  
    -   Open **\HKLM\SOFTWARE\VMware, Inc.\VMware Tunnel**
    -   Open the **DeviceTrafficRules** file
    -   Check for the application to be whitelisted

### Mutli-Auth. failure or compliance failures
-   Device must be whitelisted in Tunnel Configuration in Registry
-   Check for Device to be compliant
-   Check for validation of certificates  
    Go to Manage Compute Certificate → Trusted Root Certificate Authorities → Certificate → Check for Tunnel Server Authorized certificate

  

### Whitelisted App's traffic is not getting tunneled
The app's executable may not be the one which is creating the connection.  
Turn off the tunnel service, open the app in question and browse to an end-point. Run command **netstat -aonb**  to check what executable is connecting to the end point. If this executable is different than the whitelisted exe then use this exe instead.
{{% notice style="warning" %}}
DO NOT whitelist svchost.exe. This is the common service used for many functions in windows. This may lead to BSOD.
{{% /notice %}}

### Unable to open internal website (in configured domain) from whitelisted application
NRPT may be corrupted. Stop the vmware tunnel service. This should ideally clear all NRPT entries. Now open **NRPT- Edit Group Policy → Windows Settings → Name Resolution Policy**.
Check if there any entries left, if there are, then delete them.  
  

### Browsing experience via whitelisted app seems to be staggered. And unable to access configured domain websites
Turn on Debug logs for tunnel client. It is possible there is an issue with tunnel connectivity. Either tunnel client cannot reach tunnel server (there is an SSL error while trying to connect to server) or there is Multi-Auth Failure/ Whitelist failure for the device in server.

