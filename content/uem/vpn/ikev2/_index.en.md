+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "vpn", "network" ]
description = "Solving issues with IKEv2 on iOS"
title = "IKEv2 on iOS"
+++

### Example of device profile config
![](ikev2.jpg)

{{% notice style="warning" %}}
IKE2 cipher must be configured as well as CHILD cipher. Or else XML of config will be incorrect! Error example: ChildSecurityAssociationParameters does not specify keys 'EncryptionAlgorithm' and 'IntegrityAlgorithm' in the dictionary on the console XML.
{{% /notice %}}

Configure IKE as follows:
![](ikev2b.jpg)

Configure Child as follows:
![](ikev2c.jpg)

{{% notice style="warning" %}}
If configuring Always-On mode, the same thing applies to WIFI/Cellular radio buttons and IKE2/Child in this mode: both must be configured for correct XML to be formed!
{{% /notice %}}