+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "android", "troubleshooting" ]
description = "Boxer for Android logging and troubleshooting"
title = "Boxer for Android Logs"
+++

## Boxer for Android Architecture

Boxer communicates with a few different systems to provide these services.  
The flow of the Android Boxer log file looks like this:
-   Boxer reaches out to the console to get the profile information;
-   Boxer reaches out to the console to get any S/MIME certificates and client authentication certificates available from the console;
-   Boxer reaches out to the Exchange ActiveSync(EAS) email endpoint to make the options, provision, foldersync, and ping request;
-   Boxer reaches out to the Email Notification S ervice (ENS) and provides the Exchange Web     Services (EWS) credential information that was used for ActiveSync so it can subscrib e to email notifications.

```mermaid
graph TD
CS[Console Server] --> Boxer;
Boxer --> CS;
Exchange --> Boxer;
Boxer --> Exchange;
Boxer --> ENS;
ENS --> Boxer;
```

-   Boxer logs are in GMT. That means the entries there are 5 hours ahead of EST time. In addition, the logs are in 24 hour time so you will need to subtract 12 if you want am/pm.
-   The very first line in the Boxer logs has the last date of the logs in the file. For example `2019-01-08T21:28:04.539Z - [-] - ###--HEADER--###`
-   Boxer logs can persist in the app for up to 3 days, so they can be pulled for review even if the issue hasnt occurred recently, but at least within the 3 day period.
    This is useful for intermittent types of issues, or where there is a delay in an affected user reporting the issue to their techsupport.

## Boxer Communication Logs

Here is an example of what you might see in the logs followed by notes (in bold) that detail what the log entry tracks:

{{% expand title="Boxer log sample..." %}}
```json
2019-02-05T13:51:52.636Z - [-] - ###--HEADER--### | This is the time of the last log entry in this log file for easy reference.

-------------------------------------------------------------- DEVICE INFO

--------------------------------------------------------------  
DEVICE ID = boxerc59d6cd3d34aba549 | the EAS device identifier  
MANUFACTURER = samsung  
MODEL = SM-G930F  
SDK VERSION = 26  
COMPROMISED = false | shows whether or not the device is jail broken  
APP VERSION CODE = 758  
APP VERSION NAME = 5.3.0.3  
STANDALONE = false  
ANCHOR APP = com.airwatch.androidagent | shows how the device is enrolled through the AirWatch agent, Boxer, or other method CONSOLE VERSION = 19.3.0.0  
PASSCODE ENABLED = false | shows whether or not Boxer will prompt the user for a password when opening Boxer  
AUTO-REFRESH FOLDERS = true | shows the sync frequency specified in the profile. "true" will be returned if it's set to "automatic" -- Stats --  
EnsRegister_1 => {"total_events_today":1,"average_events_per_hour":1.0}

AttachmentServiceWakeLock => {"total_events_today":4,"average_events_per_hour":4.0} Eas.SSLException => {"total_events_today":30,"average_events_per_hour":3.0}  
IrmSync => {"total_events_today":1,"average_events_per_hour":1.0} AccountProtocolCache.Cache_Used => {"total_events_today":103,"average_events_per_hour":103.0} EnsUpdateEnsConfig_-1 => {"total_events_today":1,"average_events_per_hour":1.0}

1/Socket Timeout => {"total_events_today":4,"average_events_per_hour":0.22222222} FetchManagedConfig => {"total_events_today":3,"average_events_per_hour":3.0} Eas.SyncAdapter => {"total_events_today":5,"average_events_per_hour":5.0} EnsGetSubscriptionStatus_1 => {"total_events_today":1,"average_events_per_hour":1.0} -- End Stats --

-- Power Info --  
Power saving mode enabled = false | shows whether or no the application is excluded from power saving mode which will cause application and networking issues Ignoring battery optimizations = true | shows whether or not the application is excluded from battery optimizations which will cause application and networking issues Device idle enabled = false  
-- End Power Info --

-- Permissions Info -- | shows whether or not the proper permissions are enabled for the application Calendar read: true  
Calendar write: true  
Contacts read: true

Contacts write: true Call: false  
Phone state: false Storage: true Camera: false

-- End Permissions Info --

-------------------------------------------------------------- ACCOUNT SETTINGS

-------------------------------------------------------------- DESCRIPTION = nas98.airwlab.com:8002 DISPLAY_NAME = Legend Wilcox  
EMAIL = lwilcox@labmail.airwlab.com

USERNAME = lwilcox@milkyway.local SERVER_ADDRESS = nas98.airwlab.com:8002 SSL_REQUIRED = null  
PORT = null

USER_DOMAIN = milkyway  
SIGNATURE =  
SYNC_EMAIL = true  
SYNC_CALENDAR = true  
SYNC_CONTACTS = true  
SYNC_EMAIL_LOOKBACK = 5  
MAX_SYNC_EMAIL_LOOKBACK = 5  
SYNC_CALENDAR_LOOKBACK = 7  
MAX_SYNC_CAL_LOOKBACK = 7  
LOGIN_CERTIFICATE = null  
LOGIN_CERTIFICATE_NAME = null  
TRUST_ALL_SSL_CERTS = null  
SMIME_SIGNING_CERT_ISSUER = 09e219f7-7864-437c-8c26-cd6613375e09  
SMIME_ENCRYPTION_CERT_ISSUER = 09e219f7-7864-437c-8c26-cd6613375e09  
SMIME_SIGNING_CERT_ISSUER_TOKEN = 3D9B54CEC239582BFD471D2BB1BE4E55580DF302 | this is the SMIME signing certificate thumbprint ID SMIME_ENCRYPTION_CERT_ISSUER_TOKEN = 3D9B54CEC239582BFD471D2BB1BE4E55580DF302 | this is the SMIME encryption cetificate thumbprint ID ACCOUNT_AUTHENTICATION_TYPE = 1 | 0 for basic, 1 for certificate, 2 for both (dual factor), 3 Oauth, 4 Oauth with CBA

ALLOW_MOBILE_FLOWS = false MOBILE_FLOWS_HOST_URL = null MOBILE_FLOWS_VIDM_URL = null SMIME_POLICY_TRUST_STORE = 0 SMIME_POLICY_REVOCATION_STRICTNESS = 0 SMIME_POLICY_STRICT_EKU_CHECK = false TLS_POLICY_TRUST_STORE = 0 DEFAULT_SIGNING_ALGORITHM = null DEFAULT_ENCRYPTION_ALGORITHM = null SMIME_ENCRYPTION_CONFORMING_ALGORITHMS = null SMIME_SIGNING_CONFORMING_ALGORITHMS = null STRICT_TLS_TRUST_CHECK = false

-------------------------------------------------------------- ACCOUNT POLICY

--------------------------------------------------------------  
ALLOW_CALLER_ID = true | caller ID restricted = false, caller ID unrestricted = true CALLER_ID_DEFAULT = true  
APP_MUST_USE_AIRWATCH_BROWSER = false  
APP_ALLOW_CRASH_REPORTING = true  
ATTACHMENT_VIEWER_PACKAGE_NAMES = []  
APP_PASSCODE_COMPLEXITY = 0 | passcode mode 0 for numeric, 1 for alphanumeric MIN_COMPLEX_CHARS_IN_APP_PASSCODE = 0  
MIN_APP_PASSCODE_LENGTH = 4 | how many characters required for the Boxer application password MAX_APP_PASSCODE_FAIL_ATTEMPTS = 10  
MAX_AGE_FOR_APP_PASSCODE = 90  
MAX_LOOKBACK_FOR_REPEAT_PASSCODE_CHECK = 5  
APP_PASSCODE_TIMEOUT = 2  
ALLOW_OTHER_ACCOUNTS = true  
ALLOW_COPY_PASTE = true  
ALLOW_SCREEN_CAPTURE = true  
ALLOW_ATTACHMENTS = true  
MAX_ATTACHMENT_SIZE = 2147483647  
DOCUMENT_SHARING_RESTRICTION = 0  
ALLOW_CALENDAR_WIDGET = false  
SMIME_POLICY_ALLOWED_KEY = 1  
ALLOW_EMAIL_WIDGET = false  
ALLOW_METRICS = true  
APP_FORCE_ACTIVATE_SSO = false  
ALLOW_ATTACHMENTS_FROM_DOC_PROVIDERS = true  
POLICY_ALLOW_OPEN_IN = 1  
APP_DEFAULT_BROWSER_EXCEPTIONS = null  
EMAIL_NOTIFICATION_TEXT_POLICY = -1  
SUPPORT_API_KEY having value = false  
ALLOW_ACTION_ARCHIVE = true  
ALLOW_ACTION_SPAM = true  
POLICY_APP_SWIPES_LEFT_SHORT_DEFAULT = -1  
POLICY_APP_SWIPES_LEFT_LONG_DEFAULT = -1  
POLICY_APP_SWIPES_RIGHT_SHORT_DEFAULT = -1  
POLICY_APP_SWIPES_RIGHT_LONG_DEFAULT = -1  
APP_AVATAR_ENABLED = true  
ALLOW_LOCAL_CALENDAR = true  
ALLOW_LOCAL_CONTACTS = true  
APP_DOMAINS_INTERNAL = null  
APP_DOMAINS_WARNING = false  
APP_RE_FETCH_EMPTY_LINKS_USING_MIME = false  
SHOULD_USE_PLAIN_TEXT_FOR_SYNC = false  
APP_LOG_AGGREGATION_URL = null  
APP_LOG_AGGREGATION_MODE = 0  
ALLOWED_MAIL_SERVER_CIPHERS = null

-------------------------------------------------------------- BOXER SETTINGS

-------------------------------------------------------------- DEVICE_ID = 1D62C4D972BB4A4799474C5C0E5BA437 NEW_SYNC_ENGINE = false ALLOW_ENTERPRISE_CONTENT = false VIP_NOTIFICATIONS = true

-------------------------------------------------------------- BOXER POLICY

-------------------------------------------------------------- ALLOW_LOGGING = null  
ALLOW_METRICS = null  
ALLOW_PRINT = null

ALLOW_ACTION_EVERNOTE = null IS_KEY_ESCROW_DISABLED = false

-------------------------------------------------------------- EMAIL CLASSIFICATION SETTINGS --------------------------------------------------------------

ENABLED = false DEFAULT CLASS = null X-HEADER KEY = null VERSION = null  
SORT ORDER = 1 CLASSIFICATIONS = []

-------------------------------------------------------------- THROTTLE SETTINGS

-------------------------------------------------------------- POLICY_CMD_FREQUENCY = null POLICY_SYNC_CMDS = null POLICY_RECENT_CMDS = null

-------------------------------------------------------------- ENS SETTINGS

--------------------------------------------------------------  
ENS_LINK_ADDRESS = https://europa.airwlab.com/MailNotificationService/api/ens ENS_API_TOKEN = 091d6a8a897b41528ea452088e7fc599 POLICY_ACCOUNT_NOTIFY_PUSH = true  
EWS_URL = https://nas98.airwlab.com:8002/ews/exchange.asmx  
ENS_STATE = (1 -> Subscribed) | the state of ENS when the logs were captured

PendingAccountEmail = null PendingAccountName = null UserDisabled = false

-------------------------------------------------------------- THREAD INFO

--------------------------------------------------------------  
com.boxer.common.e.a@811edcf[pool size = 2, active threads = 1, queued tasks = 0, completed tasks = 59, largest pool size = 7]

[com.boxer.unified.utils.at:a x 2, com.boxer.email.activity.MailActivityEmail:a, com.boxer.app.d:a x 2, com.boxer.email.provider.AttachmentProvider:onCreate, com.boxer.common.e.f:a x 18, com.boxer.common.e.f:b x 7, com.boxer.unified.widget.BaseWidgetProvider:b, com.boxer.emailcommon.provider.i:a, com.boxer.email.a.b:a x 2, com.boxer.settings.fragments.CombinedSettingsFragment:s, com.boxer.sdk.AirWatchAccountSetupService:e, com.boxer.unified.ui.ConversationViewFragment:a x 2, com.boxer.unified.providers.MailAppProvider:g, com.boxer.unified.ui.ConversationListFragment:U, com.boxer.email.service.ImapPopWorkerService:b, com.boxer.common.passcode.n:t, com.boxer.app.ag:f, com.boxer.common.activity.n:e x 2, com.boxer.common.app.v26support.k:o x 2, com.boxer.common.activity.n:c x 6, com.boxer.calendar.provider.CalendarProvider2:e, com.boxer.common.activity.ManagedActivity:P x 2, com.boxer.unified.ui.MailActionBarView:a, com.boxer.exchange.service.EmailSyncAdapterService:a]

-------------------------------------------------------------- HEALTH STATS

--------------------------------------------------------------  
[sync] com.boxer.contacts/com.boxer.exchange/eM_ADDR count=0 [sync] com.boxer.contacts/com.boxer.exchange/eM_ADDR time_ms=0 [sync] com.boxer.calendar/com.boxer.exchange/eM_ADDR count=0
[sync] com.boxer.calendar/com.boxer.exchange/eM_ADDR time_ms=0  
[sync] com.boxer.email.provider/com.boxer.exchange/eM_ADDR count=0  
[sync] com.boxer.email.provider/com.boxer.exchange/eM_ADDR time_ms=0  
[jobs] com.boxer.email/com.boxer.calendar.CalendarProviderChangedJobService count=0 [jobs] com.boxer.email/com.boxer.calendar.CalendarProviderChangedJobService time_ms=0 [jobs] com.boxer.email/.AirWatchSDKIntentService count=0  
[jobs] com.boxer.email/.AirWatchSDKIntentService time_ms=0  
[jobs] com.boxer.email.provider/com.boxer.exchange/eM_ADDR:android count=0  
[jobs] com.boxer.email.provider/com.boxer.exchange/eM_ADDR:android time_ms=0  
[jobs] com.boxer.email/com.airwatch.bizlib.gcm.GCMReceiverService count=0  
[jobs] com.boxer.email/com.airwatch.bizlib.gcm.GCMReceiverService time_ms=0  
[jobs] com.boxer.email/com.boxer.ens.EnsStateManager$RequestHandlerService count=0 [jobs] com.boxer.email/com.boxer.ens.EnsStateManager$RequestHandlerService time_ms=0 [jobs] com.boxer.email/com.boxer.service.EasOptionJobSchedulerService count=0  
[jobs] com.boxer.email/com.boxer.service.EasOptionJobSchedulerService time_ms=0  
[jobs] com.android.contacts/com.boxer.exchange/eM_ADDR:android count=0  
[jobs] com.android.contacts/com.boxer.exchange/eM_ADDR:android time_ms=0  
[jobs] com.boxer.email/com.boxer.sdk.SDKConfigurationUpdateHandler count=0  
[jobs] com.boxer.email/com.boxer.sdk.SDKConfigurationUpdateHandler time_ms=0  
[jobs] com.android.calendar/com.boxer.exchange/eM_ADDR:android count=0  
[jobs] com.android.calendar/com.boxer.exchange/eM_ADDR:android time_ms=0  
[jobs] com.boxer.email/.service.AttachmentsDeleteIntentService count=0  
[jobs] com.boxer.email/.service.AttachmentsDeleteIntentService time_ms=0  
[jobs] com.boxer.email/com.boxer.contacts.services.BoxerCallerIdIntentService count=0 [jobs] com.boxer.email/com.boxer.contacts.services.BoxerCallerIdIntentService time_ms=0 [jobs] com.boxer.email/com.boxer.irm.IRMTemplatesSyncService count=0  
[jobs] com.boxer.email/com.boxer.irm.IRMTemplatesSyncService time_ms=0  
[jobs] com.boxer.email/com.airwatch.storage.SessionDataStorageService count=0  
[jobs] com.boxer.email/com.airwatch.storage.SessionDataStorageService time_ms=0  
[jobs] com.boxer.email/com.boxer.exchange.provider.GALDirectoryUpdateService count=0 [jobs] com.boxer.email/com.boxer.exchange.provider.GALDirectoryUpdateService time_ms=0 [jobs] com.boxer.contacts/com.boxer.exchange/eM_ADDR:android count=0  
[jobs] com.boxer.contacts/com.boxer.exchange/eM_ADDR:android time_ms=0  
[jobs] com.boxer.calendar/com.boxer.exchange/eM_ADDR:android count=0  
[jobs] com.boxer.calendar/com.boxer.exchange/eM_ADDR:android time_ms=0  
[foreground activity] count=0  
[foreground activity] time_ms=0  
[foreground_service] count=0  
[foreground_service] time_ms=0  
[top] count=0  
[top] time_ms=0  
[top_sleeping] count=0  
[top_sleeping] time_ms=0  
[background process state] count=0  
[background process state] time_ms=0  
[cached process state] count=0  
[cached process state] time_ms=0  
[cpu] user_cpu_time_ms=0  
[cpu] system_cpu_time_ms=0  
[user activity] button_user_activity_count=0  
[user activity] touch_user_activity_count=0  
[process] sessionTokenStorage.data anr_count=0  
[process] sessionTokenStorage.data crash_count=0  
[process] sessionTokenStorage.data foreground=0  
[process] sessionTokenStorage.data starts=10  
[process] sessionTokenStorage.data system_time=60  
[process] sessionTokenStorage.data user_time=130

[process] com.boxer.email [process] com.boxer.email [process] com.boxer.email [process] com.boxer.email [process] com.boxer.email [process] com.boxer.email  
[wifi] wifi_running_ms=0  
[wifi] wifi_full_lock_ms=0  
[wifi] wifi_rx_bytes=0  
[wifi] wifi_tx_bytes=0  
[mobile] mobile_rx_bytes=0 [mobile] mobile_tx_bytes=0 [mobile_radio_active] count=0 [mobile_radio_active] time_ms=0

anr_count=0 crash_count=0 foreground=289340 starts=10 system_time=58290 user_time=153290

-------------------------------------------------------------- ACCOUNTS INFO

--------------------------------------------------------------  
NAME: lwilcox@labmail.airwlab.com, TYPE: 5, IS_MANAGED: true, FETCHES_MIME: false
```
{{% /expand %}}

## Common Ways to Search Through the Logs

On the console where you configure your Boxer profile, you will have places where you can enter profile information like Account Name, Domain, User, etc... However, in the Boxer logs, when you are looking for the corresponding data there, the wording might be slightly different. You can use the mapping information below to see what this would say in the Boxer logs. The value on the left is what you will see in the console. The value after "=" is what you will see in the Boxer logs.

```shell
Account Name = DESCRIPTION  
Exchange ActiveSync Host = SERVER_ADDRESS  
Domain = USER_DOMAIN  
User = USERNAME  
Email Address = EMAIL  
Email Signature = SIGNATURE  
Authentication Type = ACCOUNT_AUTHENTICATION_TYPE | 0 basic, 1 certificate, 2 both Copy Paste = ALLOW_COPY_PASTE  
Screenshots = ALLOW_SCREEN_CAPTURE  
Allow email widget = ALLOW_EMAIL_WIDGET  
Allow calendar widget = ALLOW_CALENDAR_WIDGET  
Hyperlinks = POLICY_ALLOW_OPEN_IN  
Sharing = DOCUMENT_SHARING_RESTRICTION  
Caller ID = ALLOW_CALLER_ID  
Personal Accounts = ALLOW_OTHER_ACCOUNTS  
Personal Contacts = ALLOW_LOCAL_CONTACTS
```

Boxer reaches out to the console to get the profile information

```shell
2019-02-07T15:21:55.345Z I [18459-BoxerWorker-5] - App initialization step complete: 1

2019-02-07T15:21:55.578Z E [18459-main] - FLF.setSelectedAccount(null) called! Destroying existing loader.

2019-02-07T15:21:55.589Z I [18459-BoxerWorker-3] - Waiting for app restrictions
```

This entry is the best example we have of when Boxer reaches out to the console. You can use [Fiddler](https://www.telerik.com/fiddler) to further determine when Boxer is reaching out to the console. In the Boxer logs, you can't directly see this. The other place you can see this is in the [ADB](https://developer.android.com/studio/command-line/adb) device logs. The line here that says "waiting for app restrictions" just indicates that Boxer is waiting to read from the database. Boxer reaches out to the console to get any S/MIME certificates and client authentication certificates available from the console.

```shell
2019-02-07T15:21:56.256Z I [18459-IntentService[AirWatchAccountSetupService]] - Fetching S/MIME signing certificate

2019-02-07T15:21:58.021Z I [18459-AsyncTask #2] - Certificate being fetched for : AccountAuthenticationCertificateId
2019-02-07T15:21:59.013Z I [18459-AsyncTask #2] - Certificate fetch successful 
2019-02-07T15:21:59.755Z I [18459-Thread-7] - TrackingKeyManager: requesting a client cert alias for 66.170.96.7 
2019-02-07T15:22:05.776Z I [18459-Thread-9] - Registering socket factory for certificate alias [AW-be6fad91fe2b4291b1d392c9eabf90be] 
2019-02-07T15:22:05.846Z D [18459-Thread-9] - Found cert chain: [ [0] Version: 3
SerialNumber: 468315651040541492877707779630191635557515624
IssuerDN: DC=local,DC=milkyway,CN=milkyway-SUN-CA Start Date: Thu Feb 07 10:11:47 EST 2019  
Final Date: Sat Mar 09 10:11:47 EST 2019  
SubjectDN: CN=lwilcox
Public Key: RSA Public Key
modulus: ccbf28975eafd49dedfcfc43f9d66a309d36c4c636fbece2bf9faa87fe6544c8e025c3da260eb4bc28096f665a231202c5cf53dbb5f1d08ceac4d9af90f0605ee2951e6239576749d39d17e8d411b39c03e6a68e155083866a69de610853ab83d149c0228b6f0b0d67d4e9e1093ce8a3316a563c48d91fcc000bae01c150644113a527d611d83f7303825a58a4382c1818d8fd56b2064ef495c709e4ae4366e7793e29cd3b376e9660028269c8233ba8cd9a42e57dc2f07087d25fc6ce536d3bcbc8fabe2f6c408ddb2fefd8b80fdc029ba16237b48d26072d0f88f9e982ab37720883ff7bfb35bc409637fb314c00cbc1cb262e7406732c45c400ca45d9357b
public exponent: 10001
```

Boxer reaches out to the Exchange ActiveSync (EAS) email endpoint to make the options, provision, foldersync, and ping request.

Making the Options Request
{{% expand title="Boxer Options Request..." %}}
```shell
2019-02-07T15:22:05.859Z D [18459-Thread-9] - EasServerConnection about to make request OPTIONS https+clientCert+aw-be6fad91fe2b4291b1d392c9eabf90be://nas98.airwlab.com:8002/Microsoft-Server-ActiveSync HTTP/1.1 X-VMware-Boxer-RequestId:5bde2467-38bc-4a8e-9599-256217cb8c6d
2019-02-07T15:22:05.859Z V [18459-Thread-9] - query: uri=content://com.boxer.email.provider/account/1, match is 1
2019-02-07T15:22:06.080Z I [18459-Thread-9] - Requesting a client cert alias for [RSA, EC]
2019-02-07T15:22:06.081Z I [18459-Thread-9] - Requesting a client private key for alias [AW-be6fad91fe2b4291b1d392c9eabf90be]
2019-02-07T15:22:06.082Z I [18459-Thread-9] - Requesting a client certificate chain for alias [AW-be6fad91fe2b4291b1d392c9eabf90be] 
2019-02-07T15:22:06.178Z V [18459-Thread-9] - SSL socket using protocol: TLSv1.2 
2019-02-07T15:22:11.485Z V [18459-Thread-9] - Response headers:
Header [Strict-Transport-Security], value [max-age=31536000;includeSubDomains] Header [X-Frame-Options], value [sameorigin]  
Header [X-XSS-Protection], value [1;mode=block]  
Header [X-Content-Type-Options], value [nosniff]

Header [Content-Security-Policy], value [default-src 'self'; font-src 'self' data:; script-src 'unsafe-eval' 'self'; style-src 'unsafe-inline' 'self'; object-src 'none';] Header [Cache-Control], value [private]  
Header [Allow], value [OPTIONS,POST]  
Header [Content-Type], value [application/vnd.ms-sync.wbxml]

Header [Server], value [Microsoft-IIS/8.5]  
Header [request-id], value [73ab0f67-308f-45d2-a76b-04eb72fde4e8]  
Header [X-CalculatedBETarget], value [earth.milkyway.local]  
Header [MS-Server-ActiveSync], value [15.0]  
Header [MS-ASProtocolVersions], value [2.0,2.1,2.5,12.0,12.1,14.0,14.1]  
Header [MS-ASProtocolCommands], value [Sync,SendMail,SmartForward,SmartReply,GetAttachment,GetHierarchy,CreateCollection,DeleteCollection,MoveCollection,FolderSync,FolderCreate,FolderDelete,FolderUpdate,MoveItems,GetItemEstimate,MeetingResponse,Search,Settings,Ping,ItemOperations,Provision,ResolveRecipients,ValidateCert] Header [Public], value [OPTIONS,POST]  
Header [X-MS-BackOffDuration], value [L/-470]  
Header [X-DiagInfo], value [EARTH]  
Header [X-BEServer], value [EARTH]  
Header [X-AspNet-Version], value [4.0.30319]  
Header [Persistent-Auth], value [false]  
Header [X-Powered-By], value [ASP.NET]  
Header [X-FEServer], value [MERCURY]  
Header [WWW-Authenticate], value [Negotiate YIGVBgkqhkiG9xIBAgICAG+BhTCBgqADAgEFoQMCAQ+idjB0oAMCAReibQRr1j3vRSNgnWRAjRmh9+c4H68rnNGVHQxp2bq87jvJsxPfGt+Wi1ciWxUKztM5Rjq2HiDSiMttELVzigj9rrUB00pGrB/LemrzZeB1NU3oyWSlYfGMd1uvo96mRGykkn8vA0kW3mlXDKacEz0=]  
Header [Date], value [Thu, 07 Feb 2019 15:22:00 GMT]  
Header [X-AW-SEG-SERVER-INFO], value [***.***.***.43|2.9.0.1|JSEG-SEG368-JOB1-2|Tue 08 Jan 2019 03:32:23 PM EST -0500]  
Header [X-AW-SEG-TRANSACTION-ID], value [c42418f6-55a1-4ffc-b4fd-6e94c2d70fe3]  
Header [X-Correlation-ID], value [c42418f6-55a1-4ffc-b4fd-6e94c2d70fe3]  
Header [Content-Length], value [0]  
Header [Set-Cookie], value [ClientId="NGLCEUYUWLVTAAWHW";$Path="/";$Domain="nas98.airwlab.com:8002";HttpOnly]  
Header [Set-Cookie], value [X-BackEndCookie="S-1-5-21-174156188-4291662551-709137977-1610=u56Lnp2ejJqByprLzcrHyMzSx8aantLLz8ic0p7Kmc7Sz52bnZqZy8nIy8mZgYHNz87G0s/M0s/Gq87Kxc3Nxc/P";$Path="/Microsoft-Server-ActiveSync";Secure;$Domain="nas98.airwlab.com:8002";HttpOnly]

2019-02-07T15:22:11.535Z D [18459-Thread-9] - Server supports versions: 2.0,2.1,2.5,12.0,12.1,14.0,14.1 
2019-02-07T15:22:11.625Z V [18459-Thread-9] - <SyncKey> 
2019-02-07T15:22:11.625Z V [18459-Thread-9]-0 
2019-02-07T15:22:11.625Z V [18459-Thread-9] - </SyncKey> 
2019-02-07T15:22:11.625Z V [18459-Thread-9] - </FolderSync>
```
{{% /expand %}}

Perform a FolderSync
{{% expand title="Boxer FolderSync..." %}}
```shell
2019-02-07T15:22:11.634Z D [18459-Thread-9 ] - EasServerConnection about to make request POST https+clientCert+aw-be6fad91fe2b4291b1d392c9eabf90be://nas98.airwlab.com:8002/Microsoft-Server-ActiveSync?Cmd=FolderSync&User=milkyway%5Clwilcox%40milkyway.local&DeviceId=1D62C4D972BB4A4799474C5C0E5BA437&DeviceType=BoxerManagedAndroid HTTP/1.1 X-VMware-Boxer-RequestId:572f2ab2-cdf1-4960-b30a-6b078365b3aa  
2019-02-07T15:22:11.805Z V [18459-Thread-9 ] - SSL socket using protocol: TLSv1.2  
2019-02-07T15:22:15.574Z V [18459-Thread-9 ] - Response headers:
Header [Strict-Transport-Security], value [max-age=31536000;includeSubDomains] Header [X-Frame-Options], value [sameorigin]  
Header [X-XSS-Protection], value [1;mode=block]  
Header [X-Content-Type-Options], value [nosniff]

Header [Content-Security-Policy], value [default-src 'self'; font-src 'self' data:; script-src 'unsafe-eval' 'self'; style-src 'unsafe-inline' 'self'; object-src 'none';] Header [Cache-Control], value [private]  
Header [Content-Type], value [application/vnd.ms-sync.wbxml]  
Header [Vary], value [Accept-Encoding]

Header [Server], value [Microsoft-IIS/8.5]  
Header [request-id], value [3cd7bb11-e3c4-4292-be4a-87cb5d388473]  
Header [X-CalculatedBETarget], value [earth.milkyway.local]  
Header [MS-Server-ActiveSync], value [15.0]  
Header [X-MS-BackOffDuration], value [L/-469]  
Header [X-DiagInfo], value [EARTH]  
Header [X-BEServer], value [EARTH]  
Header [X-AspNet-Version], value [4.0.30319]  
Header [Persistent-Auth], value [false]  
Header [X-Powered-By], value [ASP.NET]  
Header [X-FEServer], value [MERCURY]  
Header [WWW-Authenticate], value [Negotiate YIGVBgkqhkiG9xIBAgICAG+BhTCBgqADAgEFoQMCAQ+idjB0oAMCAReibQRrSwxIlKGW3nSAU46FtN7BJ3AO5tTFRrU/dbnLqIXVdl6/ySBXRFmuQ/AsFOP95xNJf0ze6ZG5Vrk/KeCaxlmsaSqlShiRmjPAxAyuIzJ8vKnjzQFf6MPFepwruykQpNDqjdGtRs7RV1/uy8M=] Header [Date], value [Thu, 07 Feb 2019 15:22:04 GMT]  
Header [X-AW-SEG-SERVER-INFO], value [***.***.***.43|2.9.0.1|JSEG-SEG368-JOB1-2|Tue 08 Jan 2019 03:32:23 PM EST -0500]  
Header [X-AW-SEG-TRANSACTION-ID], value [51e38c4f-5114-4465-a956-6f66f86c967d]  
Header [X-Correlation-ID], value [51e38c4f-5114-4465-a956-6f66f86c967d]  
Header [Set-Cookie], value [X-BackEndCookie="S-1-5-21-174156188-4291662551-709137977-1610=u56Lnp2ejJqByprLzcrHyMzSx8aantLLz8ic0p7Kmc7Sz52bnZqZy8nIy8mZgYHNz87G0s/M0s/Gq87Kxc3Nxc/L";$Path="/Microsoft-Server-ActiveSync";Secure;$Domain="nas98.airwlab.com:8002";HttpOnly] Header [content-encoding], value [gzip]  
Header [transfer-encoding], value [chunked]
2019-02-07T15:22:15.667Z V [18459-Thread-9] - <Status>  
2019-02-07T15:22:15.668Z V [18459-Thread-9] - Status: 144 
2019-02-07T15:22:15.671Z V [18459-Thread-9] - </Status> 
```
{{% /expand %}}

Perform a provision request
{{% expand title="Boxer provision request..." %}}
```shell
2019-02-07T15:22:15.681Z I [18459-Thread-9 ] - Received needs provision response in EasOperation.performOperation  
2019-02-07T15:22:15.723Z D [18459-Thread-9 ] - EasServerConnection about to make request POST https+clientCert+aw-be6fad91fe2b4291b1d392c9eabf90be://nas98.airwlab.com:8002/Microsoft-Server-ActiveSync?Cmd=Provision&User=milkyway%5Clwilcox%40milkyway.local&DeviceId=1D62C4D972BB4A4799474C5C0E5BA437&DeviceType=BoxerManagedAndroid HTTP/1.1 X-VMware-Boxer-RequestId:08e64270-501d-4732-9675-5309dd6a3675  
2019-02-07T15:22:15.834Z V [18459-Thread-9 ] - SSL socket using protocol: TLSv1.2  
2019-02-07T15:22:19.828Z V [18459-Thread-9 ] - Response headers:
Header [Strict-Transport-Security], value [max-age=31536000;includeSubDomains] Header [X-Frame-Options], value [sameorigin]  
Header [X-XSS-Protection], value [1;mode=block]  
Header [X-Content-Type-Options], value [nosniff]

Header [Content-Security-Policy], value [default-src 'self'; font-src 'self' data:; script-src 'unsafe-eval' 'self'; style-src 'unsafe-inline' 'self'; object-src 'none';] Header [Cache-Control], value [private]  
Header [Content-Type], value [application/vnd.ms-sync.wbxml]  
Header [Vary], value [Accept-Encoding]

Header [Server], value [Microsoft-IIS/8.5]  
Header [request-id], value [dcda9893-be8c-4b17-8fe5-067f71dd57e0]  
Header [X-CalculatedBETarget], value [earth.milkyway.local]  
Header [MS-Server-ActiveSync], value [15.0]  
Header [X-MS-BackOffDuration], value [L/-469]  
Header [X-DiagInfo], value [EARTH]  
Header [X-BEServer], value [EARTH]  
Header [X-AspNet-Version], value [4.0.30319]  
Header [Persistent-Auth], value [false]  
Header [X-Powered-By], value [ASP.NET]  
Header [X-FEServer], value [MERCURY]  
Header [WWW-Authenticate], value [Negotiate YIGVBgkqhkiG9xIBAgICAG+BhTCBgqADAgEFoQMCAQ+idjB0oAMCAReibQRr9huiSZNPIN39dB0GIaIF4gSACDj8yrV879OKH5dUeSEQzFnRcPGIShPvLFbDCrDS+ii/N9SAvk/B20xq+MXtxXRBsHyJXFJo5vIRjkWq+3Qis5c/4+gP5Vi0bsvlVSKIW4yqRyGe5ZfGoNc=] Header [Date], value [Thu, 07 Feb 2019 15:22:08 GMT]  
Header [X-AW-SEG-SERVER-INFO], value [***.***.***.43|2.9.0.1|JSEG-SEG368-JOB1-2|Tue 08 Jan 2019 03:32:23 PM EST -0500]  
Header [X-AW-SEG-TRANSACTION-ID], value [13206ae4-e2c0-4579-a992-a3d9664a5763]  
Header [X-Correlation-ID], value [13206ae4-e2c0-4579-a992-a3d9664a5763]  
Header [Set-Cookie], value [X-BackEndCookie="S-1-5-21-174156188-4291662551-709137977-1610=u56Lnp2ejJqByprLzcrHyMzSx8aantLLz8ic0p7Kmc7Sz52bnZqZy8nIy8mZgYHNz87G0s/M0s/Gq87Kxc3Nxc/H";$Path="/Microsoft-Server-ActiveSync";Secure;$Domain="nas98.airwlab.com:8002";HttpOnly] Header [content-encoding], value [gzip]  
Header [transfer-encoding], value [chunked]
2019-02-07T15:22:19.882Z D [18459-Thread-9 ] - Provision status: 1
```
{{% /expand %}}

Perform another FolderSync, this time it works. Start email sync.
{{% expand title="Boxer another FolderSync..." %}}
```shell
2019-02-07T15:22:24.586Z I [18459-AccountsUpdateListener] - Accounts changed - requesting FolderSync for unsynced accounts  
2019-02-07T15:22:24.975Z I [18459-SyncAdapterThread-1] - Calendar sync for account lwilcox@labmail.airwlab.com, with extras Bundle[{}] 
2019-02-07T15:22:25.236Z I [18459-SyncAdapterThread-1] - Email sync for account lwilcox@labmail.airwlab.com, with extras Bundle[{}]

2019-02-07T15:22:29.810Z I [18459-Thread-10] - Initial sync requested for account: [nas98.airwlab.com:8002:lwilcox@labmail.airwlab.com:Legend Wilcox]  
2019-02-07T15:22:30.430Z I [18459-BoxerWorker-9] - requestSync EmailProvider startSync Account {name=lwilcox@labmail.airwlab.com, type=com.boxer.exchange}, Bundle[{do_not_retry=false, __userRequest__=true, callback_method=sync_status, force=true, expedited=true, callback_uri=content://com.boxer.email.provider}] 
2019-02-07T15:22:30.447Z I [18459-SyncAdapterThread-2] - Email sync for account lwilcox@labmail.airwlab.com, with extras Bundle[{ignore_settings=true, do_not_retry=false, __userRequest__=true, callback_method=sync_status, force=true, expedited=true, ignore_backoff=true, callback_uri=content://com.boxer.email.provider}] 
2019-02-07T15:22:34.950Z I [18459-pool-21-thread-4] - Starting sync command 
2019-02-07T15:22:34.993Z I [18459-pool-21-thread-2] - Syncing account 1 mailbox "Sent Items" (class Email) with syncKey 0, operation name = HtmlMailSync 
2019-02-07T15:22:39.191Z I [18459-pool-21-thread-2] - Committing new sync key (684740726) for mailbox (Sent Items) 
2019-02-07T15:22:39.216Z I [18459-pool-21-thread-2] - Sync command finished with result 1 
2019-02-07T15:23:00.737Z I [18459-SyncAdapterThread-2] - Initial sync completed
```
{{% /expand %}}

Ping example
```shell
2019-02-07T15:40:12.911Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - Ping task starting for 1  
2019-02-07T15:40:12.912Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - Exchange ping starting  
2019-02-07T15:43:00.125Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - Changes found in: 8  
2019-02-07T15:43:00.126Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - Ping found changed folders for account 1  
2019-02-07T15:43:00.137Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - requestSync EasOperation requestSyncForMailboxes Account {name=lwilcox@labmail.airwlab.com, type=com.boxer.exchange}, Bundle[{__mailboxCount__=1, force=true, expedited=true, __mailboxId0__=5, PING_ERROR_COUNT=3}] 2019-02-07T15:43:00.139Z I [18459-PingTask-lwilcox@labmail.airwlab.com] - Exchange ping finished with result 2

2019-02-07T15:43:00.200Z I [18459-SyncAdapterThread-8 ] - Email sync for account lwilcox@labmail.airwlab.com, with extras Bundle[{ignore_settings=true, __mailboxCount__=1, force=true, expedited=true, ignore_backoff=true, __mailboxId0__=5, PING_ERROR_COUNT=3}] 2019-02-07T15:43:00.316Z I [18459-SyncAdapterThread-8 ] - Starting sync command
```

Boxer reaches out to the Email Notification Service (ENS) and provides the Exchange Web Services (EWS) credential information that was used for ActiveSync so it can subscribe to email notifications.
```shell
2019-02-07T15:23:08.315Z I [18459-AsyncTask #4 ] - Ens registration for account (id=1) is successful! 
2019-02-07T15:43:30.041Z I [18459-main ] - Sync triggered from distance
```

You can use Notepad++ to search the logs using the following search terms.

• **"error"** - This is a very general way to look for errors. You will have a lot of false/positives when using this search criteria.  

• **"exception "** - You can use this to search the file for exceptions that are generated when the application runs into an error.  

• **"Current Network"** - This will show you what network connection the mobile device was connected to 4g, wi-fi, etc...  

• **"distance"** - This shows you each time the ENS server has reached out to the mobile device to wake it up, send a notification, and trigger a sync • "transitioning" - Use this search term to tell when the application is transitioning from background to foreground and vice versa.

• **"requestPing"** - This will show you when the device reaches out to the OS to use the Android SyncAdaptor which it uses to request the ping. The response to this request will be "Email sync for account".  

• **"Email sync for account"** - This will be a response from the OS for a command "requestPing" or "requestSync ". Take note of how long between the request and the reply from the OS. If it's a long time, it's likely being throttled by the OS due to battery optimization or a third party product. 

• **"Changes found in: "** - is the response to a ping request. This tells us if the ping found any changes in the inbox. If it did, we will now do a "requestSync " to bring those emails or changes into Boxer.  

• **"requestSync"** - This will show you when the device reaches out to the OS to use the Android SyncAdaptor which it uses to request email sync. The response to this request will be "Email sync for account".  

• **"Q-AppInitialization "** - This shows you when the application is starting up. You will see this when you force close and reopen the application or when you start the application after a crash. 

• **"PingTask "** - shows you all activities related to pings  

• **"SyncAdapterThread "** - shows you all activities related to sync operations.  

• **"Email sync for account "** - The response from the OS after a "requestSync" or "RequestPing" operation.  

• **"ens"** - Search the log files for ens errors and settings.

### Collecting Boxer Logs When You Can't Get Into Android Boxer

Depending on what model phone you have, you can follow one of the processes below to collect Boxer logs in the event that you can't get into Boxer due to an error. You may have a different model device than the ones listed, but the process will be very similar.

#### Samsung S9+

• Go to "Settings" on the phone followed by "Apps" • Select "Boxer"  
• Select "Mobile Data"  
• Select "View App settings"
• Select "Send logs"
• Use one of the other mechanisms to send the logs (either by copying the logs out or another email client). 

#### On Motorola X Pure

• Go to "Settings" on the phone followed by "Apps" 
• Select "Boxer"
• Select "Data Usage"  
• Select "App settings"  
• Select "Send logs"
• Use one of the other mechanisms to send the logs (either by copying the logs out or another email client).

