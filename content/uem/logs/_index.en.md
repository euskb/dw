+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "log" ]
description = "WS1 UEM Logs collection"
title = "Logs"
+++

## Linked Articles

{{% children sort="weight" description="true" %}}

## Collecting AirWatch Services Logs (On-Premise)

### AirWatch Enterprise Systems Connector (ESC) / Cloud Connector (ACC)

To verbose the ACC log, perform the following:

-   Open Windows Explorer on the ACC server, and browse to the C:\AirWatch\CloudConnector\folder
-   Note the presence of two folders: Bank1 and Bank2. Every time the Cloud Connector software is updated, the update is applied to the **inactive** bank folder. The updated bank folder then becomes the active bank folder.
-   Open each Bank folder and sort the file list by date modified. Compare the most recent date modified in each file. The current bank file has the **most recent** date modified.
-   Within the current bank folder (C:\AirWatch\CloudConnector\Bank#), open the **CloudConnector.exe.config** file and change the level value in the <loggingConfiguration> from **error** to **verbose** and save the file.
-   After reproducing the error, open Windows Explorer on the ACC server and browser to the C:\AirWatch\Logs directory. Copy the appropriate log to a new location for use in support/troubleshooting.
-   Be sure to change the loggingConfiguration level value from **verbose** to **error** and save the file to prevent unnecessary impact to the ACC server.

### AirWatch API Services (API)

To verbose the API Service Log, perform the following:

-   On the server running API services, open Windows Explorer and browse to C:\AirWatch\AirWatch #.#\Websites\AirWatchApi\. **Note**: You can determine the API server by browsing to **Groups & Settings > All Settings > System > Advanced > Site URL’s**.
-   Open the **web.config** file, and look for the loggingConfiguration key.
-   Change the value for level from **error** to **verbose** and save the web.config file.
-   Restart IIS services.
-   Reproduce your issue and then copy the log from C:\AirWatch\Logs\AirWatchAPI\webserviceapi.log.
-   Change the value for level from **verbose** back to **error** and save the web.config file.
-   Restart IIS Services.

### AirWatch Cloud Messaging (AWCM)

To verbose the AWCM logs, please perform the following steps:

1.  Open the logback.xml file. The path to access the file:**\AirWatch\AirWatch x.x\AWCM\config\logback.xml**.
2.  Search for the following:
    -   **<logger name="com.airwatch" level="info" />**
    -   **<logger name="com.airwatch.awcm.jvm" level="info" />**
    -   **<logger name="com.airwatch.awcm.channel" level="info" />**
3.  Change the state from **error** to **debug**.
4.  Save the file and restart the AWCM services.

{{% notice style="note" %}}
Once the issue is reproduced, return logging level back to **info** and restart the AWCM services. Or the AWCM disk may overflow with logs.
{{% /notice %}}

Folder = AWCM
Log name = **Awcm.log**
Contains information on AWCM such as status, history, properties, and additional sub-services.

Log name = **AWCMservice.log**
Contains log information on AWCM Java service wrapper.

### ACC Logs
Use these steps below to verbose ACC logs:

-    On the ACC server navigate to **\AirWatch\AirWatch #.#\CloudConnector\Bank#\**
-    **#.#** will be the AirWatch version you are using, if there are multiple choose the most recent.
-    ACC utilizes two distinct banks: one active and one is used for installation of automatic updates. If you are unsure of which bank is active, make changes to the **CloudConnector.exe.config** file in each bank. If one bank is empty or does not have the file, it is not the active bank.
-    Edit the **CloudConnector.exe.config** lines:
    -   level = **"Verbose"**
    -   tracingEnabled = **"true"**  
        
        ```
        <loggingConfiguration filePath="..\..\Logs\CloudConnector\CloudConnector.log" tracingEnabled="true" level="Verbose" logFileRollSize="10240" maxArchivedFiles="20"/>
        ```
        
-    Locate the log by looking at the **filePath** attribute from the line above. The path is included below as well **\AirWatch\Logs\CloudConnector\CloudConnector.log**
-   ACC doesn't need to be restarted to pick up the logging level configuration change.

### Console Services (CS)
To enable verbose logging for console and scheduler services, please perform the following steps:

-   Log in to the AirWatch console in question.
-   With the Global organization group selected, browse to **Groups & Settings > All Settings > Admin > Diagnostics > Logging**.
-   Change the logging level for the services in question to **verbose** and click Save.
    -   Admin Console
    -   Self-Service Portal
    -   API
    -   Scheduled Services (such as Inventory, Workflow, and Monitor services)
    -   Reproduce your error, then open Windows Explorer and browser to C:\AirWatch\Logs\_Service Folder_ and look for the latest log.
    -   Change the Device Services logging level back to Error. This prevents logging from impacting system performance.

### Device Services – Targeted
Depending on the version of AirWatch, it is possible to collect verbose logs for an individual device without having to verbose the logs for **all** devices. This is particularly helpful when troubleshooting a single device in a large production deployment. To do this, perform the following steps:

-   From within the AirWatch console device list view, click on your device to take you to the device details page.
-   Click **More > Targeted Loggin**g. If necessary, click the **Continue Targeted Logging File Path** to ensure the logging path is configured.
-   From the targeted logging page, click **Create New Log** and select the timeframe you want the logs to collect. Click **Start**.  At any point, you can click **Stop Logging** to stop log collection for the device (such as after you have reproduced the issue).
-   Once the tests are completed, go to the appropriate server and look for the TargetedLogging folder.
-   Inside the folder is a zip file with the current date and time. Unzip the file to view the files.

### Device Services – General
When you wish to verbose device services logging for **all** devices, perform the following:

-   Log in to the AirWatch console in question.
-   With the Global organization group selected, browser to **Groups & Settings > All Settings > Admin > Diagnostics > Logging**.
-   Change the Device Services logging level to **verbose** and click Save.
-   Reproduce your error, then open Windows Explorer and browser to C:\AirWatch\Logs\DeviceServices\ and look for the latest log.
-   Change the Devices Services logging level back to Error. This prevents logging from impacting system performance.

### SEG Console, Setup, and Integration Logs
To verbose the SEG logs for console/setup/integration, please perform the following steps:

-   Open Windows Explorer on the SEG server and browser to **C:\AirWatch\Logs**
-   Note the following Folders and change the appropriate config log level from error to verbose:
    -   Services – Contains the **AW.EAS.IntegrationService.log** file which details communications between the AirWatch API server and SEG server. **Note**: This log is verbosed by changing the level value in the <loggingConfiguration> key of the **AW.ES.IntegrationService.Exe.config** file in the **C:\AirWatch\AirWatch #.#\AW.Eas.IntegrationService** folder.
    -   SEG Setup – Contains the AW.EAS.Setup.log file which details activity related to the [http://localhost/SEGSetup](http://localhost/SEGSetup) website. **Note**: This log is verbosed by changing the level value in the <loggingConfiguration> key of the web.config file in the C:\AirWatch\AirWatch #.#\AW.Eas.Setup folder.
    -   SEG Console – Contains the AW.EAS.Web.log file which details activity related to the [http://localhost/SEGConsole](http://localhost/SEGConsole) website. **Note**: This log is verbosed by changing the level value in the <loggingConfiguration> key of the web.config file in the C:\AirWatch\AirWatch #.#\AW.Eas.web folder.
    -   Before reproducing your issue, making the necessary change to the LoggingConfiguration key for the service in question.
    -   After reproducing the error, open Windows Explorer on the SEG server and browse to the appropriate subfolder in the C:\AirWatch\Logs\ directory. Copy the appropriate log to a new location for use in support/troubleshooting.
    -   Be sure to change the loggingConfiguration level value (currently verbose) in the appropriate configuration file back to error to prevent unnecessary impact to the SEG server.

### SEG Exchange ActiveSync (EAS) Listener Logs
To verbose the SEG EAS Listener logs, please perform the following steps:

-   On the SEG server, open an Internet browser and navigate to [http://localhost/SEGSetup](http://localhost/SEGSetup)
-   In the “Log Level” drop down, select **verbose** and click Save. **Note**: This changes the level value in the <loggingConfiguration> key of the web.config file in the C:\AirWatch\AirWatch #.#\AW.Eas.Web.Listener folder.
-   Copy the AW.Eas.Web.Listener.log to a new location for use in support/troubleshooting.
-   In your Internet browser, change the “Log Level” drop down back to **error** and click Save.

### FTP Relay Server (Rugged Management)
Logging for the relay server is saved in the following location:

-   Browse to the C:\AirWatch\Logs\Service folder.
-   The logging for the Relay Server is saved in the **ContentDeliveryService.log** file.

### Collecting logs from the Admin Console
If you do not have immediate access to the on-premise servers to access the logs, you can retrieve SEG/ACC logs directly from the console from the following page:

-   Navigate to **System > Admin > Diagnostics > System Health**. Click on the installed service you wish to pull the logs from.
-   In the pop-up box now displayed, click on the “Acquire Logs,” for the required service, from the four-button menu on the right.
-   Now the “Download” button is activated and you can click on it to download and view logs remotely.

**Note**: The System Health dashboard will be populated only if you have any of the services (ACC/SEG) already installed and running.

### EMail Notification Service (ENS)
ENSv2 is a Windows Service 'AWSubscription'.  Like other AirWatch services, relevant logs can be found in the path **{Installation Path}/Logs**, and the logging level can be configured by editing parameters to **traceEnabled="true" Level="Verbose"** in the app config (**{Installation Path}\Config\WebSites\Web.config**) file located in the installation folder

ENSv2 Errors are in **ENS.log** and **ReSubscriptionMechanism.log**

{{% notice style="note" %}}
The service must be restarted for logging changes to take effect.
{{% /notice %}}

When set to verbose, you will be able to identify log messages pertaining to both new subscriptions being created, as well as any device compliance state-changes being identified.  For example, if a device becomes compromised and is then marked as non-compliant.  In the logs, a message indicating that a device's access state is **True** indicates that the device is allowed, whereas **False** means the device is blocked.

