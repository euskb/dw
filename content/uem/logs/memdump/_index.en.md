+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "log" ]
description = "Mobile app memory dumping"
title = "Memory Dumping"
+++

Dumping memory for certain mobile app processes and analyzing the data files generated.

### External links
[https://github.com/Nightbringer21/fridump](https://github.com/Nightbringer21/fridump)
[https://www.frida.re/docs/installation/](https://www.frida.re/docs/installation/)
[https://github.com/frida/frida/releases](https://github.com/frida/frida/releases)

### General plan
-   Perform key generation operations 
-   Dump the memory using fridump
-   Pull the boxer_generated_keys.txt file
-   Use the MemDumpAnalyzer.py script with -f flag,  providing the boxer_generated_keys.txt
-   Check the output and total finds

## Detailed guide (for Android) to analyze AirWatch Boxer:
{{% notice style="tip" %}}
For information regarding iOS frida-server installation, please follow [https://www.frida.re/docs/ios/](https://www.frida.re/docs/ios/).
{{% /notice %}}

**1.** Have a rooted/jailbroken device or an emulator running in your computer.

**2.** Install python if you haven't already [https://www.python.org/downloads/](https://www.python.org/downloads/), this will allow us to run the scripts.

**3.** Open a new terminal and install [frida](https://www.frida.re/docs/installation/), this will be the main framework that will be used by the underlying scripts
```bash
pip install frida
```
**4.** Once the frida is installed successfully, make sure to download the frida-server binary depending on the device type. The frida-server binary version and the frida version must match for proper dump.

The download links can be found in [https://github.com/frida/frida/releases](https://github.com/frida/frida/releases) 

Once the correct device frida-server binary is downloaded, unarchive the compressed file. The frida-server binary file should look like _"frida-server-10.7.5-android-x86_64"_ which will be different depending on the device architecture.

**5.** Put the frida-server binary in to the Android device (using ADB tool):
```shell
adb root # might be required
adb push frida-server /data/local/tmp/ # push the binary frida-server file into the device
adb shell "chmod 755 /data/local/tmp/frida-server" # make the binary file executable via permissions
adb shell "/data/local/tmp/frida-server &" #run
```
Check if frida-server is running properly, type the following in the terminal:
```shell
frida-ps -U
```
This command will show you all the process running inside the mobile device. 

**6.** At this point, we have frida-server as well as our application running on the device. It is time to download the fridump library that will allow us to dump the process memory in the device.

Clone the [fridump](https://github.com/Nightbringer21/fridump) Git repository:
```shell
git clone https://github.com/Nightbringer21/fridum
```

**7.** Now lets go ahead and try to dump the memory using fridump 

Provide flag **-s** so that at the end of the dump process, there will be a separate script which will capture all the strings in generated dump files.
```shell
python fridump.py -s com.example.name
```
Dump files will be located under _fridump/dump/*_
strings.txt file is located under _fridump/dump/strings.txt_
For generating dump files in another location, please provide flag -o with the full path.

**8.** At this point, you will be able to analyze all the strings that reside in memory in strings.txt. However what if you are looking for something that consists of set of random bytes? For this purpose, you will need a separate python script that will analyze the dump files. 

Go to the fridump repo and make sure you are at the root. 

Download the python script MemDumpAnalyzer.py and copy the script to fridump folder root. For this script to work properly, your dump files must be located at the original destination which is _fridump/dump/*_.

### Attached Script
[MemDumpAnalyzer Python Script](./_index.en.files/MemDumpAnalyzer.py)

The script supports two functionality which are to search a string in dump files or a file with hex encoded strings to be searched in the file separed with new lines. The string option allows you to search normal strings while file option will let you search random byte in hex. This is particularly useful since our keys are randomly generated keys which fail during encoding. Here are some examples of usages.

-f : file path to search for
-s : string to search for
-h : help for usage

```shell
python MemDumpAnalyzer.py -h
python MemDumpAnalyzer.py -s boxer
python MemDumpAnalyzer.py -f boxer_generated_keys.txt
```

**9.** Generate hex encoded keys and store them in a file shown below. All the keys will have to be separated by new lines and have to hex encoded. The scripts supports file with upper and lower case or with '0x' suffix in hex encoding.

```shell
58F75A01A1A5AE0F9B4C92AFF742468B751DD293517B6FEAE3A71BA126155922
35FD53F845FE5C01711002AF719F5DCBDFE8E45BE8611357B7D4AC44A28401DE
72F74A5B8D94DE8C3E2A6341DF09B0C414CCB7DB34FE367138015DB923505AE9
76E3368BE3EF445E18FD6E036F69258332E79F95B2D4A1CE9B2CBF0459E40C11
4D0309AFCB4CD47C2EC19F0B569ACEACD318FB0BDFB7C20A00570C1E2CA916A6
71A2AFC7C6B87211F0C2DD36245041B251AF92D66AEC2FD78E4FE2C2C8A48C7A
E7CBE67D09D7C4A9BB06FE5CF9E2B3ACC09BC30672DCD21B7A3156B20DEEB501
61C4425D8305C6859EB88565FC7F4CC220A3F1C8CEFE4F020C9B7B2EAC7CBDB3
80B5E111C27136FDDEDB6A3868F2E0FE5B15D4E55C03A72C07A6647CDBAE0054
7489346AE746E258775364CADC80B7E26751708B3688A210AAC36F99B4072E13
64D4BC15DBD0E0014DE7A6C568CB47160F037A95D9EE3D92FE6B8AC70AA99CD3
55291C59B69BDCF136DFB1995F174D7B3D2813189E575EB13DC41609E29F262C
DCD9C7FB65D31094CF3F75271722D8A23CA417AD6082921E5A78126A0ED10973
424C3550A53335FE1BBFE844E00EC51622D1D670BD22C6380664C94B6A50740D
EDFCDA5996A84300C14D627FE32392219E9B67D4F6D8515B757464C0FFE8C5CE
A0B20AABF91ACDF27C90DE8265042BB4282C245668F1C855C51C9638B38FFD12
```

**10.** To pull the file from the device to the current folder, use
```shell
adb pull data/data/com.boxer.email/files/boxer_generated_keys.tx
```