import argparse
import re
import binascii
import os.path
import utils


def argParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, help='the hex file to search for in dump')
    parser.add_argument('-s', '--string', type=str, help='string to search for in dump')
    return parser.parse_args()


def printTotalFinds(totalFinds):
    print '\n\n** total finds: %d' % totalFinds


def search(filepath, hexText):
    pattern = re.compile(hexText)
    with open(filepath, "rb") as f:
        found = re.search(pattern, binascii.hexlify(f.read()))
        if found:
            print '     Found \'{0}\' in file \'{1}\' '.format(hexText, filepath)
        return found


def searchMemDumpFiles(searchText):
    print " -- Searching '%s' in dump files" % searchText
    totalFinds = 0
    progress = 0
    numFiles = len(os.listdir(os.getcwd()))
    for path, directories, files in os.walk(os.getcwd()):
        for memdump in files:
            utils.printProgress(progress, numFiles, prefix='Progress:', suffix='Complete', bar=50)
            if memdump != 'strings.txt':
                if search(memdump, searchText):
                    totalFinds += 1
            progress += 1
    return totalFinds


# MAIN
print "\n *** Memory Dump Analyzer ***\n"
arguments = argParser()

if arguments.file is not None:
    total = 0
    with open(arguments.file, "r") as searchFile:
        os.chdir(os.getcwd() + '/dump/')
        for line in searchFile.readlines():
            hex = re.sub(r'(\n)|(0[xX])', '', line)
            if hex != '':
                total += searchMemDumpFiles(hex.lower())
    printTotalFinds(total)

elif arguments.string is not None:
    os.chdir(os.getcwd() + '/dump/')
    total = searchMemDumpFiles(binascii.hexlify(arguments.string))
    printTotalFinds(total)
