+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "log" ]
description = "Screen recording from laptop systems"
title = "Screen Recording"
+++

## Screen Recording

### Windows 7/8/8.1/10/11

To document click-through steps on Windows machines, perform the following:

-   Click on **Start > Run** and type **psr.exe** to bring up the Problem Steps Recorder (or PSR, a built-in Windows utility).
-   Click on **Start Record** to begin capturing steps. **Note**: PSR captures screenshots of ALL monitors; no scoping.
-   Each Mouse-Click you make captures a screenshot. At any time during the session, click on **Add Comment** to provide more details about the screen, error, etc.
-   When finished, click **Stop Record**.
-   Choose where to Save the PSR file – it outputs a zip file containing a pre-compiled HTML (*.mhtml) file with all your screenshots and comments.

### macOS

To document click-through steps on macOS machines, perform the following:

-   Launch QuickTime Player. You’ll find it in the Other folder within Launchpad.
-   From the QuickTime menu bar, click **File > New Screen Recording**. Click the red record button.
    -   Optionally you may wish to select **View > Float on Top** before you start recording.
    -   Optionally, you can select the upside-down triangle in the record screen to include audio recording during the screen capture for annotation.
    -   Click the screen (or Click-Drag to select part of the screen) for recording.
    -   When complete, click the **Stop** button that appears in the menu bar of the screen where you’re recording.
    -   Click **File > Save** (or simply quit QuickTime) to be prompted with a location to save the screen capture. **Note**: Keep it moving when you record these; they create full-blown movies and the file gets large quickly.