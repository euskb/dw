+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "log" ]
description = "Device Side Logs collection"
title = "Device-side logs collection"
+++

## Collecting Device-Side Logs
### macOS Devices
With version 2.x of the macOS agent, you can now collect logs via the agent by performing the following steps:
-   Open the agent by right-clicking the agent icon from the menu bar, and then clicking **Preferences**
-   From the **Status** screen, click on **Diagnostics**
-   Click the button to **Send Logs to Administrator**
    -   The agent will gather logs and zip them into an email for you automatically

To manually gather logs on older versions of the AirWatch agent, please perform the following:
-   Enable Debug Logging in the Mac OS X Terminal:
    -   $ sudo defaults write /Library/Preferences/com.apple.MCXDebug debugOutput -2
    -   $ sudo defaults write /Library/Preferences/com.apple.MCXDebug collateLogs 1
    -   $ sudo touch /var/db/MDM_EnableDebug
    -   Open the Console application from the Launchpad. Select **All Messages** and then click **Clear Display** to clear out old logs.
    -   Reproduce the issue on Mac.
    -   Click **File > Save a Copy As.** and save a copy of the logs to be sent to AirWatch.
    -   Disable the Debug logging in the Mac OS X Terminal:
        -   $ sudo rm –rf /var/db/MDM_EnableDebug
        -   $ sudo defaults delete /Library/Preferences/com.apple.MCXDebug debugOutput
        -   $ sudo defaults delete /Library/Preferences/com.apple.MCXDebug collateLogs

### iOS Devices (iPhone, iPad, Apple TV)
To gather logs from iOS devices (iOS 7.x and below), please perform the following steps:

-   Install iPhone Configuration Utility (iPCU) on your workstation.
-   With iPCU started, connect the iOS device to your computer via the USB cable.
-   On the left-hand side of iPCU, select the iOS device where you wish to collect logs.
-   Click on the **Console** tab at the top right-hand corner.
-   Reproduce the issue with your iOS devices.
-   Click the **Save Console As** button to save the text in the console to a file.

To gather logs from iOS devices (iOS 8.0+), please perform the following steps:

-   Install xCode 6 on your OS X Device. **Note**: You cannot gather iOS 8 logs from a Windows-based computer. You must use a Mac OS X computer.
-   With xCode started, connect the iOS device to your computer via the USB cable.
-   From within xCode, click on the **Window** menu and click **Devices**.
-   Select your iOS device from the left hand side, then select the up arrow at the bottom corner of the right hand side.
-   Reproduce the issue with your iOS devices.
-   Save the contents of the activity log to a file.

### Android
To gather logs using Console, please see the [following documentation](http://digital-work.space/display/AIRWATCH/AirWatch+Log+Collection+from+Android+-+Misc).  

To gather logs using ADB, please perform the following steps (see [the following page](http://digital-work.space/display/AIRWATCH/AirWatch+Log+Collection+from+Android+-+ADB) for details):

-   Download and set up the Android SDK per the SDK documentation.
-   Open Windows Explorer and browse to the <SDK Install Root>\platform-tools folder. Ensure you see the **adb.exe** file.
-   Open a CMD window and navigate to the platform-tools folder. Or, in Windows 7, navigate back to the SDK folder, then Shift + Right-Click on the platform-tools folder, and select **Open Command Window Here**.
-   Ensure USB Debugging is enabled on your Android device (from the Developer Settings menu). For more information on how to enable the Developer Settings menu, browse to [http://www.androidcentral.com/how-enable-developer-settings-android-42](http://www.androidcentral.com/how-enable-developer-settings-android-42)
-   In your Notification Center, you may need to make sure the device is **not** connected in USB Media Device mode.
-   In the CMD window, type adb logcat –v long > androidlog.txt
-   On the Android device, recreate whatever error you are trying to log.
-   When complete, from within the CMD window use CTRL + C to end the logging.
-   Go back to the platform-tools folder and find the log file (androidlog.txt) that you just created.

### Windows 7/8/8.1/10/11
To gather logs, please perform the following steps:

-   Click on **Start > Run**, type eventtvwr.msc and click **OK**. On Windows 8/8.1, from the start menu you can simply start typing Event and select the View Event Logs item returned from universal search.
-   Expand **Event Viewer (Local) > Windows Logs** and select the **Application** log.
-   You can filter logs by Event ID or Source if desired.
-   To export for support, click on either **Save All Events As** or **Save Selected Events** to export the log entries as an *.evtx file which can be sent to support.
-   You can also find logs in the following location: <InstallPath of Agent>\AgentUI\Logs
    -   AwclClient.log - AWCM-related Issues
    -   AWProcessCommands.log - Issues with sending commands to the device
    -   NativeEnrollment.log - Issues with Enrollment
    -   TaskScheduler.log - Issues with samples sent to console

### Windows Phone 8.1 (deprecated)
To gather logs, please perform the following steps:

-   Ensure you have Visual Studios 2013 Update 3 installed. If not, perform the following:
    -   From your Windows Laptop of VM, browse to [Windows Phone SDK Archives](http://dev.windows.com/en-us/develop/download-phone-sdk)
    -   Download the Visual Studio Express 2013 for Windows and Install it.
    -   From within Visual Studios, click on **Tools > Windows Phone 8.1 > Developer Unlock**. Follow the prompts to unlock your Windows Phone 8.1 device.
    -   From within Visual Studios, click on **Tools > Windows Phone 8.1 > Developer Power tools**.
    -   Select **Device** from the Select Device dropdown, then click **Connect**. If prompted, click **Install** to install the Phone Tools Update Pack.
    -   Select the Performance Recorder tab, then check the **Enterprise Management** option under the Extras profile category.
    -   Click the Start button in the Developer Power Tools window to start a log.
    -   Run your scenarios and re-create the issue you’re experiencing.
    -   Click the Stop button in the Developer Power Tools window to stop logging and save the ETW to a local location.
    -   You will need to download the Windows Performance Analyzer to view the logs. This can be found in the Windows Performance Toolkit included in the Windows Assessment & Deployment Toolkit (ADK) and Windows Software Development Kit (SDK).

**Windows Performance Toolkit**
-   Open the Windows Performance Analyzer and Open the ETL file.
-   In the Graph Explorer window, expand System Activity and view the **Generic Events** window.
-   Double-click the graphic bars in the **Generic Events** window to display an **Analysis** window.
-   In the Analysis window, click Open View Editor to show a **Generic Events View Editor** window.
-   In the Generic Events View Editor window, ensure the Message box is checked and click Apply:
    -   The Message field in the analysis window provides the MDM specific log message under various providers.
    -   Microsoft-WindowsPhone-Enrollment-API-Provider -- ETW logs for MDM Enrollment and MDM Client Cert Renew Process.
    -   Microsoft-WindowsPhone-SCEP-Provider -- SCEP Cert enrollment logging
    -   Microsoft-WindowsPhone-CmCspVpnPlus -- VPN Configuration logging

### Windows Mobile Devices with Agent 5.x
All log settings are configured in the log_config.cfg file in the \Program Files\AirWatch directory on the device. The file will resemble the following:

```shell
[*] 
trace_level=5 
max_file_size_kb=256 
files_to_keep=2 
log_file_path=\Program Files\AirWatch\Logs 
use_local_time=false

[aw_setup] 
trace_level=5 
max_file_size_kb=256 
files_to_keep=2 
log_file_path=\ 
use_local_time=false

[awregisterdevice.exe]
trace_level=3 
max_file_size_kb=256 
files_to_keep=2 
log_file_path=\Program Files\AirWatch\Logs 
use_local_time=false

[awapplyprofile.exe] 
trace_level=5 
max_file_size_kb=256 
files_to_keep=2 
log_file_path=\Program Files\AirWatch\Logs 
use_local_time=false

[awremotecontrol.exe] 
trace_level=1 
max_file_size_kb=256 
files_to_keep=2 
log_file_path=\Program Files\AirWatch\Logs 
use_local_time=false
```

In general, the following notes apply to Windows Mobile device logging:

-   The logging level can be modified as a whole, or on an individual basis:
    -   The asterisk configuration is the default config for all logs. Trace levels vary from 1 (basic) to 5 (verbose/debug).
    -   Each individual section, which can be used to increase logging to override the default setting from the asterisk section.
    -   The log files which are available can vary (based on configuration and OEM), but the following are the most common:
        -   **aw_setup** - Provides logging information relating to the AWMasterSetup utility, which is responsible for initiating the agent install and uninstall process on a device. This is the only log file that is not located in the "\Program Files\AirWatch" directory and is instead located in the root of the file system.
        -   **awacmclient** - Provides logging information relating to the AWCM client on the device
        -   **awapplicationmanager** - Provides logging information relating to product provisioning
        -   **awprocesscommands** - Provides logging information relating to the execution of MDM commands and installation of profiles
        -   **AWService** - Provides information about the AWService.exe component, which is responsible for managing beacon and interrogator samples
        -   **awapplyprofile** - Relates to installation of the agent settings xml file which occurs during the enrollment process
        -   **awregisterdevice** - Provides information about the registering of the device that occurs during the enrollment process
        -   **awapplauncher** - Provides information about the Application Launcher executable. This log will only be present if the App Launcher utility is assigned to and being used by a device.
        -   **fusionwlansetup** - Provides information about configuring and setting up the Fusion WiFi driver on Motorola devices.

The general process for configuring log files is as follows:

-   Transfer the log file to your machine. This can be done through the file manager utility in device details or through remote management if a client has that configured.
-   Open the log file via a basic text editor such as notepad.
-   Edit the desired trace level to the needed value.
-   Save the log file.
-   Transfer the log file back down to the “Program Files\AirWatch” directory on the devices. This can be accomplished via file manager, remote manager, or product provisioning. To be safe, you may elect to first delete the old log_config.cfg file.
-   Restart **AWService** on the device once it has the updated log_config.cfg file. This can be accomplished by directly restarting the AWService through the “Restart AirWatch Agent” or the “Warm Boot” MDM commands that are available in the AirWatch Console.
-   Once the AWService has been restarted, the new logging configuration will take effect. Reproduce your issue and then repeat the steps to turn the logging back down on the device.

## Collecting Service/Functionality Specific Logs

### Product Provisioning

Review the AirWatch Agent Logs and look for the following items to help you troubleshoot what is occurring:

-   If the device is newly enrolled, you’ll see the following in the logs: A message from [AWProductHandler sendProductResponses] stating "Products: No products with results to be sent!"
-   A message from [AWEnhancedProductsHandler handleCommand:] stating "Got Products New Manifest"
    -   **Note**: In the manifest will be a line entry called **ProductID**". You'll want to save this for later on.
    -   Depending on the number of products being installed, you may see an entry for each product that is required.
    -   Messages from [AWAppDataManager readJobProduct:] looking to see if the product is downloaded to the local cache
    -   Messages from [AWOSXUtils deleteFile:] where it attempts to delete any pre-existing plist file for the products.
    -   Messages from [AWJob printJob] which show the sequence number assigned to the Product which will be installed.
    -   From this point forward you can search the log by the sequence number assigned to the product install job:
        -   Messages about the job being queued
        -   Messages about the job being started.
            -   The line will look like this: airwatchd[**PID**] <Info>: - [AWJobQueue doJob] [Line 98] THREAD: Current Job: **<JOBID>** where PID is the AirWatch Agent Process ID and the JobID is the Sequence Number assigned to the product.
            -   You can get additional information about the product actions occurring by searching from that point forward for entries from the process ID!
-   Messages about any files being downloaded to the product cache
-   Messages about Job Status Change. You'll want to search for a line ending in Job Status changed ========> :AWJobStatusFailed!" From that point, search up in the log for messages relating to the JobID and/or the ProductID (as found in the manifest). All these messages should be coming from the Process ID of the Airwatch agent that initially started the install.