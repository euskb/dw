+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "SyncML for Windows 10 Custom Settings"
title = "SyncML"
+++

### Create a Complete Block of SyncML
A complete block of SyncML code consists of the following attributes:

-   Runs from`<[characteristic]>`to`<[characteristic]>`
-   Uses Add, Delete, Replace, or Exec as a characteristic.
-   Does not contain text before or after the characteristics.
-   May or may not remove all whitespace and linearize the code block to condense its size.

##### For Example:

`<Replace><CmdID>2</CmdID><Item><Target> <LocURI>./Device/Vendor/MSFT/AssignedAccess/KioskModeApp</LocURI></Target> <Meta><Format xmlns="syncml:metinf">chr</Format></Meta><Data> {"Account":"standard","AUMID":"AirWatchLLC.AirWatchBrowser_htcwkw4rx2gx4!App"}</Data></Item></Replace>`

#### Update or Delete Settings
Manually apply tags to update or delete settings using a Windows 10 custom settings profile.

-   To update settings, use the replace tag: `<Replace> to </Replace>`
-   To remove settings, use the delete tag: `<Delete> to </Delete>`

##### For Example: Remove Kiosk Assigned Access Setting
`<Delete><CmdID>2</CmdID><Item><Target> <LocURI>./Device/Vendor/MSFT/AssignedAccess/KioskModeApp</LocURI></Target></Item></Delete>`

## Configure a Windows 10 Custom Settings Profile

Create a block of SyncML code:
1.  Go to [VMware Code Sample Exchange](http://code.vmware.com/samples).
2.  Find the correct sample.
3.  Copy the text.

### Copy the Profile Settings from the Latest AirWatch Console
1.  Log into a version of the AirWatch console that supports the desired profile functionality.
2.  Configure and **Save** this payload to create a profile.
3.  Find the new profile in the list view: 1) Click its **radio button**. 2) Click the **</>XML** option. 3) Copy the SyncML that appears.
4.  Paste the SyncML into a text editor, and edit it: 1) Remove lines of text so that all the code falls between the tags: `<[ Add, Delete, Replace, or Exec ]>` to `<[ Add, Delete, Replace, or Exec ]> 2)` Optionally, remove the whitespace, and linearize the SyncML.
5.  Copy the formatted code.

### Create New SyncML
1.  Go to the Configuration Service Provider (CSP) Reference.
2.  Access the newest Windows Insider features.
3.  Follow the site’s available guidelines to create the code sample.
4.  Copy the text.

Publish SyncML code:

1.  Navigate to **Devices** > **Profiles** > **List View** > **Add** > **Add Profile** > **Windows** > **Windows Desktop**.
2.  Refer to the **LocURI** to determine the profile’s context.
    -   **User Profile:** Select if the LocURI begins with `./User/`.
    -   **Device Profile:** Select if the LocURI begins with `./Device/`.

3. Configure General settings to determine how the profile deploys and who receives it.
4. Select the **Custom Settings** payload.
5. Click **Configure**, and paste the complete block of SyncML code in the text box.
6. Select **Save & Publish**.