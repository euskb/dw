+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "Troubleshooting profiles and apps for Windows"
title = "SQL Queries for Win Apps"
+++

### Links
- See [internal apps in SQL](../../../uem/sql/internal-apps/index.html)

### Check DB
- Check DB command id for Profile Installation
```sql
SELECT * FROM deviceCommandQueue.DeviceQueueAudit WHERE deviceid='';
```
![](SQLQueryProfile.png)

-   Copy transaction id from SQL DB 'CBDFAD47-28F4-4D63-(A1D-91C3542840' and search in syncml inside Device Services logs
-   Request Sent to Device
<SyncML xmlns="SYNCML:SYNCML1.2"><SyncHdr><VerDTD>1.2</VerDTD><VerProto>DM/1.2</VerProto><SessionID>72</SessionID><MsgID>1</MsgID><Target><LocURI>174828C828F0E444B325860CB5C3037D</LocURI></Target><Source><LocURI>https://migtest.ssdevrd.com/DeviceServices/Dm.svc/token/uq7Qn</LocURI></Source></SyncHdr><SyncBody><Atomic><CmdID>**cbdfad47-28f4-4d63-9a1d-91c354284016**</CmdID><Exec><CmdID>1ca07aea-16ea-4989-b793-0fe0d44d28ae</CmdID><Item><Target><LocURI>./cimv2/MDM_AppInstallJob/MDM_AppInstallJob.JobID=%22WC_96%22/Exec=CreateJob</LocURI></Target><Meta><Format xmlns="syncml:metinf">chr</Format><Type xmlns="syncml:metinf">text/plain</Type></Meta><Data>JobData=&lt;AppInstallJob id="96"&gt;&lt;WebApplication PackageFullName="webclicp" ActionType="1" DeploymentOptions="1" IsBundle="false"&gt;&lt;ContentURLList&gt;&lt;ContentURL&gt;https://google.com&lt;/ContentURL&gt;&lt;/ContentURLList&gt;&lt;FrameworkDependencies/&gt;&lt;/WebApplication&gt;&lt;/AppInstallJob&gt;</Data></Item></Exec></Atomic><Final /></SyncBody></SyncML> 

-   Response Received from Device
<SyncML xmlns="SYNCML:SYNCML1.2"><SyncHdr><VerDTD>1.2</VerDTD><VerProto>DM/1.2</VerProto><SessionID>72</SessionID><MsgID>2</MsgID><Target><LocURI>https://migtest.ssdevrd.com/DeviceServices/Dm.svc/token/uq7Qn</LocURI></Target><Source><LocURI>174828C828F0E444B325860CB5C3037D</LocURI></Source></SyncHdr><SyncBody><Status><CmdID>1</CmdID><MsgRef>1</MsgRef><CmdRef>0</CmdRef><Cmd>SyncHdr</Cmd><Data>200</Data></Status><Status><CmdID>2</CmdID><MsgRef>1</MsgRef><CmdRef>**cbdfad47-28f4-4d63-9a1d-91c354284016**</CmdRef><Cmd>Atomic</Cmd><Data>200</Data></Status><Status><CmdID>3</CmdID><MsgRef>1</MsgRef><CmdRef>1ca07aea-16ea-4989-b793-0fe0d44d28ae</CmdRef><Cmd>Exec</Cmd><Data>**200**</Data></Status><Final/></SyncBody></SyncML> 

-   DB Queries
```sql
-- GENERAL QUERIES
 
SELECT * FROM dbo.Device WHERE DeviceID = '';
SELECT * FROM dbo.DeviceMappingWindowsPhone WHERE DeviceID = '';
SELECT * FROM deviceCommandQueue.DeviceQueueCommand;
SELECT * FROM deviceCommandQueue.DeviceQueue WHERE DeviceId = '';
SELECT * FROM deviceCommandQueue.DeviceQueueAudit WHERE DeviceId = '';
SELECT * FROM deviceCommandQueue.DeviceQueue WHERE DeviceID IN (9275,9276);
SELECT * FROM deviceCommandQueue.DeviceQueueAudit WHERE DeviceID IN (9275,9276);
 
DELETE FROM deviceCommandQueue.DeviceQueue WHERE DeviceID IN (9275,9276);
DELETE FROM deviceCommandQueue.DeviceQueueAudit WHERE DeviceID IN (9275,9276);
 
-- PROFILE QUERIES
 
SELECT * FROM deviceProfile.DeviceProfile WHERE DeviceProfileID = '';
SELECT * FROM deviceProfile.DeviceProfileVersion WHERE DeviceProfileID = '';
SELECT * FROM deviceProfile.DevicePlatformSettingGroup WHERE DevicePlatformID IN (11,12);
SELECT * FROM deviceProfile.DevicePlatformSetting WHERE DevicePlatformSettingGroupID = '';
SELECT * FROM deviceProfile.DeviceProfileSettingValue WHERE DeviceProfileVersionID = '' AND DevicePlatformSettingID = '';
 
EXEC [deviceProfile].[DeviceProfile_Delete] '';
 
SELECT * FROM deviceProfile.DevicePlatformSetting WHERE DevicePlatformSettingGroupID = 417;
SELECT * FROM deviceProfile.DeviceProfileSettingValue WHERE DeviceProfileVersionID = 42866 AND DevicePlatformSettingID IN (40646,40647,40674,40675);
SELECT * FROM deviceProfile.DeviceProfile WHERE DeviceProfileID = 9407;
SELECT * FROM deviceProfile.DeviceProfileVersion WHERE DeviceProfileID = 9407;
 
-- APPLICATION SPECIFIC
 
SELECT * FROM device application;
SELECT awapptargetidentifier,* FROM deviceapplication.application WHERE DeviceTypeID=12 AND ISSystemApplication=1;
 
-- DELETE DEVICE
 
EXEC Device_delete '';
 
-- SCHEDULER
 
SELECT * FROM interrogator.Scheduler WHERE DeviceID = '';
 
-- PRODUCTS
 
SELECT * FROM [provisioningPolicy].[PolicyEngineQueue];
SELECT * FROM [provisioningPolicy].[DevicePolicy];
SELECT * FROM [provisioningPolicy].[DevicePolicyJob];
SELECT * FROM [provisioningPolicy].[DevicePolicyJobStatus];
SELECT * FROM [provisioningPolicy].[PolicyEngineAction];
 
-- PATCH MANAGEMENT
 
SELECT * FROM osupdate.RevisionAssignment;
SELECT * FROM smartGroup.AWEntitySmartGroupAssignmentMap WHERE RevisionAssignmentID IN (18);
SELECT * FROM smartGroup.AWEntitySmartGroupAssignmentMap_Audit WHERE RevisionAssignmentID IN (18);
SELECT * FROM osUpdate.UpdateDeviceAssignment;
 
DELETE FROM smartGroup.AWEntitySmartGroupAssignmentMap WHERE RevisionAssignmentID IN (18);
DELETE FROM smartGroup.AWEntitySmartGroupAssignmentMap_Audit WHERE RevisionAssignmentID IN (18);
DELETE FROM osupdate.RevisionAssignment;
DELETE FROM osUpdate.UpdateDeviceAssignment;
 
SELECT * FROM osUpdate.UpdateEula;
SELECT * FROM osUpdate.UpdateEulaAcceptance;
SELECT * FROM osUpdate.UpdateMetaDataUpdateEulaMap WHERE UpdateMetadataID = 21117;
SELECT * FROM osUpdate.UpdateMetadata WHERE UpdateMetadataID = 21117;
SELECT * FROM osUpdate.Revision WHERE RevisionID = 189518;
SELECT * FROM osUpdate.UpdateDescription WHERE UpdateMetadataID = 21117;
 
EXEC osUpdate.LoadRevisionAssignments  @LocationGroupID=3711 , @RevisionID = @RevisionID;
 
SELECT * FROM osUpdate.UpdateEULA;
SELECT * FROM osUpdate.UpdateEulaAcceptance WHERE LocationGroupID = 3711;
SELECT * FROM osUpdate.UpdateEulaLocationGroupMap;
SELECT * FROM osUpdate.UpdateMetaDataUpdateEulaMap;
SELECT * FROM osUpdate.UpdateMetadata WHERE RevisionID IN (794,799,800);
 
--INSERT INTO osUpdate.UpdateEULA([Language],DescriptionText,[Hash],MSEULAID) VALUES ('en','EULA','rKUV1NrYBUeMDv3uQ8dOD0LqUX4=','3c7d5240-94f7-4df7-9c8c-489b8cf2b58a');
--INSERT INTO osUpdate.UpdateEULA([Language],DescriptionText,[Hash],MSEULAID) VALUES ('en','EULA','rKUV2NrYBUeMDv3uQ8dOD0LqUX4=','3c7d5240-94f7-4df7-9c8c-489b8cf2b58b');
--INSERT INTO osUpdate.UpdateEULA([Language],DescriptionText,[Hash],MSEULAID) VALUES ('en','EULA','rKUV3NrYBUeMDv3uQ8dOD0LqUX4=','3c7d5240-94f7-4df7-9c8c-489b8cf2b58c');
--INSERT INTO osUpdate.UpdateMetaDataUpdateEulaMap(UpdateMetaDataID,UpdateEULAID) VALUES (36,10);
--INSERT INTO osUpdate.UpdateMetaDataUpdateEulaMap(UpdateMetaDataID,UpdateEULAID) VALUES (37,11);
 
DELETE FROM smartGroup.AWEntitySmartGroupAssignmentMap WHERE RevisionAssignmentID IN (30,31,32);
 
-- WINDOWS AUTO-ENROLLMENT
 
SELECT * FROM dbo.SystemCodeCategory WHERE SystemCodeCategoryID = 420;
SELECT * FROM dbo.SystemCodeGroup WHERE SystemCodeGroupID = 420;
SELECT * FROM dbo.SystemCode WHERE SystemCodeGroupID = 420;
```

### Command ID with Command Name details
<table class="wrapped confluenceTable"><colgroup><col /><col /></colgroup><tbody><tr><th class="confluenceTh" colspan="1">Command Name</th><th class="confluenceTh" colspan="1">Command ID</th></tr><tr><td class="confluenceTd">DeviceInformation</td><td class="confluenceTd">3</td></tr><tr><td class="confluenceTd">DeviceLock</td><td class="confluenceTd">4</td></tr><tr><td class="confluenceTd">AppList Sample</td><td class="confluenceTd">5</td></tr><tr><td class="confluenceTd">CertificateListSample</td><td class="confluenceTd">6</td></tr><tr><td class="confluenceTd">InstallProfile</td><td class="confluenceTd"><p>7</p></td></tr><tr><td class="confluenceTd" colspan="1">ProfileList</td><td class="confluenceTd" colspan="1">8</td></tr><tr><td class="confluenceTd" colspan="1">ProvisioningProfileList</td><td class="confluenceTd" colspan="1">9</td></tr><tr><td class="confluenceTd" colspan="1">RemoveProfile</td><td class="confluenceTd" colspan="1">10</td></tr><tr><td class="confluenceTd" colspan="1">SecurityInformation</td><td class="confluenceTd" colspan="1">12</td></tr><tr><td class="confluenceTd" colspan="1">BreakMDM</td><td class="confluenceTd" colspan="1">15</td></tr><tr><td class="confluenceTd" colspan="1">InstallApplication</td><td class="confluenceTd" colspan="1">21</td></tr><tr><td class="confluenceTd" colspan="1">ManagedApplicationList</td><td class="confluenceTd" colspan="1">23</td></tr><tr><td class="confluenceTd" colspan="1">RemoveApplication</td><td class="confluenceTd" colspan="1">24</td></tr><tr><td class="confluenceTd" colspan="1">RemoteControl</td><td class="confluenceTd" colspan="1">34</td></tr><tr><td class="confluenceTd" colspan="1">ChangePasscode</td><td class="confluenceTd" colspan="1">40</td></tr><tr><td class="confluenceTd" colspan="1">InstallProvisioningPolicy</td><td class="confluenceTd" colspan="1">60</td></tr><tr><td class="confluenceTd" colspan="1">RemoveProvisioningPolicy</td><td class="confluenceTd" colspan="1">61</td></tr><tr><td class="confluenceTd" colspan="1">EnterpriseReset</td><td class="confluenceTd" colspan="1">64</td></tr><tr><td class="confluenceTd" colspan="1">OSUpdates</td><td class="confluenceTd" colspan="1">100</td></tr><tr><td class="confluenceTd" colspan="1">HealthAttestation</td><td class="confluenceTd" colspan="1">104</td></tr><tr><td class="confluenceTd" colspan="1">HealthAttestationCertificate</td><td class="confluenceTd" colspan="1">105</td></tr><tr><td class="confluenceTd" colspan="1">HealthAttestationStatus</td><td class="confluenceTd" colspan="1">106</td></tr><tr><td class="confluenceTd" colspan="1">SelectiveAppListSample</td><td class="confluenceTd" colspan="1">116</td></tr><tr><td class="confluenceTd" colspan="1">ApproveUpdate</td><td class="confluenceTd" colspan="1">126</td></tr><tr><td class="confluenceTd" colspan="1">UnApproveUpdate</td><td class="confluenceTd" colspan="1">127</td></tr><tr><td class="confluenceTd" colspan="1">RemoveUpdate</td><td class="confluenceTd" colspan="1">130</td></tr><tr><td class="confluenceTd" colspan="1">SyncSensors</td><td class="confluenceTd" colspan="1"><p>143</p></td></tr></tbody></table>