+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "Packet sniffing during Windows 10 Enrollment"
title = "Packet Sniffing"
+++

External link: [https://techzone.vmware.com/troubleshooting-windows-10-vmware-workspace-one-operational-tutorial#968025](https://techzone.vmware.com/troubleshooting-windows-10-vmware-workspace-one-operational-tutorial#968025)

Use Fiddler. Fiddler is a free web debugging proxy server tool (local MitM-attack) which logs HTTP(S) (with decryption, using fake certificate) traffic to quickly obtain all network communications to and from the device.

### Installation

-   Download and install Fiddler on Windows 10 client device

[https://www.telerik.com/download/fiddler](https://www.telerik.com/download/fiddler)

-   Run Fiddler, click **Cancel** to disable warning

### Configuration
-   Select **WinConfig** button
![](content/uem/windows/packetsniff/Fiddler01.jpg)

-   Choose **No** in "Orphaned Exemption Record Found" message window
-   In "AppContainer Loopback Exemption Utility" window, choose **Exempt**, then **Save Changes**, then close the window
![](content/uem/windows/packetsniff/Fiddler02.jpg)
This setting captures UWP application traffic and setting on Windows 10. By default, Fiddler captures traffic only for Win32 app types.

-   Use Menu **Tools → Options...**
-   Check **Decrypt HTTPS Traffic**
![](content/uem/windows/packetsniff/Fiddler03.jpg)

-   Confirm all warnings: **Yes, Yes, Yes, OK**
-   Configure filters: most simple way is to only show traffic from specific hosts
![](content/uem/windows/packetsniff/Fiddler05.jpg)

-   Toggle Capture traffic in Menu **File → Capture Traffic**, OR use **F12** hotkey

### Traffic Inspection
-   Click **Inspectors**. 
-   Select **Raw**. Because most MDM/IDM communication is in SyncML format, for Windows 10, always select XML.
-   If inspecting HTTPS packets, they may be encoded, the click "**Response body is encoded. Click to decode"** message.
![](content/uem/windows/packetsniff/Fiddler06.jpg)

### Enrollment Troubleshooting
The most important sessions which deal with enrollment are the **Policy.aws** and **Enrollment.aws** messages and the authentication traffic in them.
![](Fiddler07.png)

