+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "AppLocker Management"
title = "AppLocker"
+++

External link: [https://docs.microsoft.com/en-us/windows/configuration/lock-down-windows-10-to-specific-apps](https://docs.microsoft.com/en-us/windows/configuration/lock-down-windows-10-to-specific-apps)

AppLocker contains capabilities and extensions that allow you to create rules to allow or deny applications from running based on unique identities of files and to specify which users or groups can run those applications.

Using AppLocker, you can:

-   Control the following types of applications: executable files (**.exe** and **.com**), scripts (**.js, .ps1, .vbs, .cmd, and .bat**), Windows Installer files (**.msi** and **.msp**), DLL files (**.dll** and **.ocx**), and packaged apps (**.appx**).
-   Define rules based on file attributes derived from the digital signature, including the publisher, product name, file name, and file version. For example, you can create rules based on the publisher attribute that is persistent through updates, or you can create rules for a specific version of a file.
-   Assign a rule to a security group or an individual user.
-   Create exceptions to rules. For example, you can create a rule that allows all Windows processes to run except Registry Editor (**Regedit.exe**).
-   Use audit-only mode to deploy the policy and understand its impact before enforcing it.
-   Import and export rules. The import and export affects the entire policy. For example, if you export a policy, all of the rules from all of the rule collections are exported, including the enforcement settings for the rule collections. If you import a policy, all criteria in the existing policy are overwritten.

{{% notice style="note" %}}
AppLocker is only supported on Windows 10 Enterprise and Education SKUs when using GPOs, however, when configuring via MDM (AirWatch) all versions are supported
{{% /notice %}}

### Create AppLocker Rule (Windows side)
Use a test Windows 10 device.

#### Creating the AppLocker Configuration File

-   Click on the Windows logo
-   Enter "group policy"
-   Click **Edit group policy**

#### AppLocker GPO
-   Go to **Windows Settings** → **Security Settings** → **Application Control Policies →** **AppLocker**
-   Click **Configure rule enforcement**

#### Enforce Packaged App Rules
In this example we will block the Xbox application (.appx). If you wanted to block RegEdit then you would configure the Executable rules.

-   Check **Configured** under **Package app Rules**; Enforce rules option is default,
-   You should **ALWAYS** test the rules before applying them, to do so, change the “Enforce rules” option to "**Audit mode**". Once you’ve confirmed that the policy you've created at the end of the lab is working as expected then you return and change this back to “Enforce rules”
-   Click **Apply** & **OK**

#### Create Default Rules

-   Click **Packaged app Rules**, to start configuring the rules.
-   Right click in the white space to the right of the window
-   Select **Create Default Rules**

#### Edit Default Rule
For blacklisting only a few apps, start with an **Allow** rule and add your blacklisting exceptions. If you want to only allow a few apps then convert the default to a **Block** and whitelist your exceptions.

1.  Right click on the default rule
2.  Click **Properties**

#### Exceptions
1.  Click **Exceptions**
2.  Click **Add...**

#### Packaged App Reference
1.  Select **Use an installed packaged as a reference**
2.  Click **Add...**

#### Select Packaged Application
1.  Using the scroll bar, scroll to the bottom
2.  Check the **Xbox** app with Package Name of **Microsoft.XboxApp** **&** click **OK**

#### Package Name
All of the package's information is pre-populated. You can block the Xbox app based on the specific version, package name, or by the publisher. We want to block any version of the Xbox application.

Raise the lever from **Package version** to **Package name** & click **OK**

#### Confirm Exceptions
-   Click **Apply** & **OK**
-   Now is the time to test if the policy is functioning correctly, if you can no longer access the xbox application on the device then you know that the policy working as expected.
-   As long as you do not see any other issues with the current configuration, go back to the app locker settings referred to in section 1.2 and change the “Audit mode” option to “Enforce rules”

#### Export AppLocker Policy
-   Right click **AppLocker**
-   Click **Export Policy...**
-   Save Policy as XML

#### Clear Policy
Now that we have exported our policy, we want to remove it from our test device.

-   Right click **AppLocker**

Click **Clear Policy** & **Yes** & **OK**

### Create AppLocker Profile (AirWatch side)
#### Creating the Application Control Profile

-   In the AirWatch console go to click **Device → Profiles → Add** **Profile → Windows → Windows Desktop → Device**
-   Enter a profile name and select a Smart Group for the **Assigned Groups**
-   Select **Application Control** at the bottom of the policy list
-   Check the **Import Sample Device Configuration** box & click **Upload**
-   Upload the XML file create in the previous steps
-   **Save & Publish** the profile

#### Verify Profile
You should now see your Block Xbox Application Control (AppLocker) profile.