+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "Running bat-scripts"
title = "Bat Scripts"
+++

**Device -> Staging & Provisioning -> Components -> Files/Actions -> Add Files/Actions button -> Windows Desktop button**.

-   Files tab - script file itself
-   Manifest - permissions to run script with, folder where script is downloaded and run from, and type of action. To circumvent a bug in AirWatch 9.1.1 a batch file can be run by using the "**Install**" command instead of the "**Run**" command...

Note though that the ECHO command is always suppressed on the endpoint and even the pause command is escaped.

The script is run using System, Admin or User permissions - defined in the script Manifest.

The script is inserted in the Product (**Device -> Staging & Provisioning -> Product List View → Add Product**) with deployment options like Compliance triggers or Schedule for installation.