+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "Windows Management"
title = "Win10 Management"
+++

## External Links
- Planning Your Windows Deployment - https://techzone.vmware.com/planning-your-windows-deployment-workspace-one-operational-tutorial#introduction

- Troubleshooting Windows Devices - https://techzone.vmware.com/troubleshooting-windows-devices-workspace-one-operational-tutorial

- Managing Updates for Windows Devices - https://techzone.vmware.com/managing-updates-windows-devices-workspace-one-operational-tutorial

- Deploying Traditional Win32 Applications to Windows Devices - https://techzone.vmware.com/deploying-traditional-win32-applications-windows-devices-workspace-one-operational-tutorial

## Articles in section

{{% children sort="weight" description="true" %}}

## Clean PC

External links:
-   [Windows 10 Configuration Service Provider Reference](http://aka.ms/CSPList)
-   [CleanPC CSP Reference](https://docs.microsoft.com/en-us/windows/client-management/mdm/cleanpc-csp)

CleanPC is the ability to remotely execute a PC Refresh (via MDM) which users can do manually on their device by going to **Settings > Update & Security > Recovery > Reset this PC > Get Started**, then you are presented with ****Keep my Files**** or ****Remove Everything****.

This best explains the differences between Retaining User Data and without Retaining User Data. Calling these CSPs will un-enroll your device. If you are using the AirWatch Agent this will also be removed when calling retaining user data option. When the AirWatch Agent is removed this will un-enroll your device.

