+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows" ]
description = "Software Distribution - 2018"
title = "Software Distribution"
+++

## External Links
-   IE CSP - [https://code.vmware.com/samples/3328/windows-10---internet-explorer-csp?h=Airwatch](https://code.vmware.com/samples/3328/windows-10---internet-explorer-csp?h=Airwatch)
-   MS ADMX templates - [https://docs.microsoft.com/en-us/windows/client-management/mdm/understanding-admx-backed-policies](https://docs.microsoft.com/en-us/windows/client-management/mdm/understanding-admx-backed-policies)
-   MS ADMX Group Policy Syntax - [https://technet.microsoft.com/en-us/library/cc753471(v=ws.10).aspx](https://technet.microsoft.com/en-us/library/cc753471(v=ws.10).aspx)

This feature is available through the **Apps & Books** section. The article also suggests ways to get the executable commands to enter for your Win32 applications in the AirWatch Console. It ends with steps to troubleshoot issues.

![](flowchart.png)

## Validated Use Cases

AirWatch validated the success of the software distribution feature in the listed use cases. Review the list and see if your deployment is similar to the validated use cases.

-   Silent deployment of MSI applications
-   MSIs with multiple transforms, and the ability to deploy different transforms to different sets of users
-   64 and 32 bit apps on 64 bit devices
-   Installers with registry validations and file checks after installation
-   Patch applied to an already deployed application
-   Application installation on system context and user context
-   A complete silent application installation
-   Application installation with dependencies
-   Packages with scripts that invoke multiple files (ZIP files that contain PowerShell scripts, EXE, and MST)
-   Installation of applications that require reboot
-   Applications with disk space, battery, and RAM checks
-   Uninstallation of installed applications

## Application-specific templates
### Internet Explorer

External link - [Internet Explorer CSP Documentation](https://docs.microsoft.com/en-us/windows/client-management/mdm/policy-csp-internetexplorer)

To deploy this sample, navigate to  **Devices & User > Profile > Add > Windows > Desktop > Device > Custom Settings**, then copy and paste the SyncML into the box and publish the profile.

-   Modify the values inside of the data tags.
-   Change the target of the policies to either device or user. Inside of you will want to change to either ./Device/ or ./User/ but be careful as some policies support User, Device, or Both[](https://docs.microsoft.com/en-us/windows/client-management/mdm/policy-csp-internetexplorer).

### Google Chrome

Deploy attached Chrome CSPs samples via AirWatch. To deploy navigate to **Devices & User > Profile > Add > Windows > Desktop > Device > Custom Settings**, then copy and paste the SyncML into the box and publish the profile.

-   Modify the values inside of the data tags.
-   Change the target of the policies to either device or user. Inside of you will want to change to either ./Device/ or ./User/ but be careful as some policies support User, Device, or Both, you can reference which are support by looking at the Chrome ADMX template.

## Tips to Get Configurations

Review some ways to get the commands and criteria for the Win32 application. Enter the data in the AirWatch Console when you upload the Win32 application package.

## Get the Install Command

Review a few ways to get install commands for Win32 applications.

**Note**: If an install command prompts for user interaction on the UI, then enter these commands with the **User** option in the **Install Context** option.

-   Call any script from the command-line that results in a successful installation of the Win32 application.
-   The MSI file has the install command pre-populated with silent parameters. You can edit and update these in the AirWatch Console.
-   If the EXE or ZIP file contains the MSI file of the Win32 application, use the **msiexec** command to install.

## Get the Uninstall Command

Review some ways to get uninstall command for Win32 applications.

-   In a command-line session, use the **/?** or **/help** parameters to display supported actions. For example, **Mysampleapp.exe /?**.
-   Look at the HKEYs in the listed registries on the device.
    -   HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall
    -   HKLM\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\
    -   HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall
    -   HKCU\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\
-   If the EXE contains an underlying MSI, use the **msiexec** uninstall command. For example, **msiexec /x <path_to_file>**.

## Get Detection Criteria
Use detection criteria to determine if the Win32 application is on devices. To get the detection criteria, install the application and identify the checks on the device.

-   **Product ID check**
    -   Run the **wmic** command and use **WMIC Product where name=”<app name>”**.
    -   Look at the HKEYs in the listed registries on the device for the product ID.
        -   HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall
        -   HKLM\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\
        -   HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall
        -   HKCU\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\
-   **File check**
    -   Look at the HKEYs listed for Product ID check to find the file criteria.
    -   Look in the Program Files folder or the Program Files(X86) folder to find the file criteria.
-   **Registry check** 

-   Look at the HKEYs listed for Product ID check to find registries.
-   Look in HKEY_CLASSES_ROOT\\Installer\\Products.

## Get Exit Codes

Use the environmental variable, **%errorlevel%**, to get exit codes. Use it in conjunction with built-in DOS commands like **ECHO**, **IF**, and **SET** to preserve the existing value of %errorlevel%.

1.  In a command-line session, run the install command for the Win32 application.
2.  Run **ECHO %errorlevel%**.
3.  The %errorlevel% variable returns the reboot exit code, if the Win32 application requires a reboot for installation.

## Troubleshoot Software Distribution Issues

Win32 application installations involve the successful execution of multiple steps. If your application installation fails, follow the troubleshooting steps to find the issue.

## Win32 Package Received Reported by App Deployment Agent

The App Deployment Agent on the user’s device handles Win32 application installations. The system deploys the agent to devices either upon enrollment or when it collects the latest App List sample from devices that are already enrolled.

The system holds the app-install commands in the queue until the agent reports back that the application installed.

### Steps

Check the following components to see that the agent installed on your end-users’ devices.

-   In the AirWatch Console, check that the device successfully enrolled and syncs with the console.
-   Check the registry for the AW App Deployment Agent.

1.  Open a command-line session and run **regedit**. This opens the Registry Editor.
2.  In the Registry Editor, go to **HKEY_LOCAL_MACHINE > SOFTWARE > Microsoft > EnterpriseDesktopAppManagement.**
3.  Look for the AW App Deployment Agent. The correct status value for the AW App Deployment Agent is **70**.

-   Check services on the device to ensure that the AW App Deployment Agent is running.
-   Check the registry for the AW MDM nodes.

1.  Open a command-line session and run **regedit**. This opens the Registry Editor.
2.  In the Registry Editor, go to **HKEY_LOCAL_MACHINE > SOFTWARE > AirWatchMDM > AppDeploymentAgent.**
3.  Find three nodes. If these three nodes are missing, then the device did not receive the Win32 application package.

-   **App Manifest** – This node contains information about the options set in the AirWatch Console on the Deployment Options tab.
-   **Content Manifest** – This node contains information about the options set in the AirWatch Console on the Files tab.
-   **Queue** – This node contains detailed logs about the installation of the application. You can view the logs to check the progress of the download of the application.

## Win32 Application Installation Status

After the agent installs on devices, you can track the application installation to troubleshoot issues. The install status for the Win32 application displays the listed statuses. 

1.  **Install command ready for device** – The install command is queued on the device but the device has not checked in to the AirWatch system.
2.  **Install command dispatched** – The device checks in to the system and consumes the install command.
3.  **Installing** – The Win32 application is downloading and the installation is in progress on the device. 
4.  **Installed** – The installation is complete and the device sent an alert to the AirWatch Console.

## Status Codes

{{% expand title="Expand to see..." %}}

<table class="confluenceTable wrapped"><colgroup><col /><col /></colgroup><tbody><tr><th class="confluenceTh" colspan="1">Status Code</th><th class="confluenceTh" colspan="1">Description</th></tr><tr><td class="confluenceTd" colspan="1">TRANSFORM_CACHE_INPROGRESS = 0x700,<br />TRANSFORM_CACHE_FAILED,<br />TRANSFORM_CACHE_SUCCESSFUL,</td><td class="confluenceTd" colspan="1"><p>Transform cache refers to any transformation on content downloaded. For example, unzip a zip package.</p><p><span>TRANSFORM_CACHE_FAILED: cache transformation precludes this operation. When unzip operation fails, this evaluation would fail. When runtime error happens, this evaluation would fail. Note that, unzip is running in non-overwrite mode, so if unzipping target directory already contains files which are also in the zip package, unzip would fail.</span></p><p><span><span>TRANSFORM_CACHE_SUCCESSFUL: cache transformation allows this operation.</span></span></p></td></tr><tr><td class="confluenceTd" colspan="1">SANITIZE_CACHE_INPROGRESS = 0x500,<br />SANITIZE_CACHE_FAILED,<br />SANITIZE_CACHE_SUCCESSFUL,</td><td class="confluenceTd" colspan="1"><p>Sanitize cache would validate content cache against content manifest and delete any files that are not specified in content manifest.</p><p><span>SANITIZE_CACHE_FAILED: cache sanitize precludes this operation. When content files in cache folder are not matching ones specified in content manifest, this evaluation would fail. When runtime error happens, this evaluation would fail.</span></p><p><span><span>SANITIZE_CACHE_SUCCESSFUL: cache sanitize allows this operation</span></span></p></td></tr><tr><td class="confluenceTd">REQUIREMENTS_EVALUATION_INPROGRESS = 0x300,<br />REQUIREMENTS_EVALUATION_FAILED,<br />REQUIREMENTS_EVALUATION_SUCCESSFUL,</td><td class="confluenceTd"><p>Requirements evaluation evaluate the conditions requirements to perform the install/uninstall operation. For example, evaluate memory, power, etc.</p><p><span>REQUIREMENTS_EVALUATION_FAILED: Requirements evaluation precludes this operation. When requirements are not met, this evaluation would fail. When runtime error happens, this evaluation would fail.</span></p><p><span><span>REQUIREMENTS_EVALUATION_SUCCESSFUL: Requirements evaluation allows this operation</span></span></p></td></tr><tr><td class="confluenceTd" colspan="1">PENDING_USER_SESSION = 0x800,<br />EXEC_DEPLOYMENT_INPROGRESS,<br />PENDING_EXEC_DEPLOYMENT_RETRY,<br />EXEC_DEPLOYMENT_FAILED,<br />EXEC_DEPLOYMENT_SUCCESSFUL,<br />PENDING_REBOOT,</td><td class="confluenceTd" colspan="1"><p><span>PENDING_USER_SESSION: It's not used.</span></p><p><span><span>PENDING_EXEC_DEPLOYMENT_RETRY: "Install Command" / "Uninstall Command" execution failed and the client would retry again. Retry timeout and interval are specified through deployment manifest.</span></span></p><p><span><span><span>EXEC_DEPLOYMENT_FAILED: The "Install/Uninstall Command" execution precludes this operation after retrying. When command execution returns some exit code which is considered error (not matching success exit code, e.g), this evaluation would fail. When command execution is timed out, this evaluation would fail. When runtime error happens, this evaluation would fail.</span></span></span></p><p><span><span><span><span>EXEC_DEPLOYMENT_SUCCESSFUL: The execution allows this operation.</span></span></span></span></p><p><span><span><span>PENDING_REBOOT: The execution is finished and requires reboot.</span></span></span></p></td></tr><tr><td class="confluenceTd" colspan="1"><p>PENDING_NETWORK_CONNECTIVITY = 0x600,<br />DOWNLOAD_CONTENT_INPROGRESS,<br />PENDING_DOWNLOAD_RETRY,</p><p>DOWNLOAD_CONTENT_FAILED,<br />DOWNLOAD_CONTENT_SUCCESSFUL,</p></td><td class="confluenceTd" colspan="1"><p><span>/* Retry attempts elapsed and/or we need a new CM. We</span><br /><span>* will suspend for a DOWNLOAD_CONTENT_FAILED for a given</span><br /><span>* period of time, before rolling it back. */</span></p><p><span>"PENDING_NETWORK_CONNECTIVITY" indicates the underlying network condition has been changed and download would be reattempted in 5 mins interval with 3 retry count. (default).</span></p><p><span>"PENDING_DOWNLOAD_RETRY" indicates download would be reattempted in 5 mins interval with 3 retry count. (default)</span></p></td></tr><tr><td class="confluenceTd">FIRST_DETECTION_INPROGRESS = 0x100,<br />FIRST_DETECTION_FAILED,<br />FIRST_DETECTION_SUCCESSFUL,</td><td class="confluenceTd"><div class="content-wrapper"><p>Executes the detection criteria before installing or downloading the application.</p><p>Detection Failed indicates that the criteria defined was unable to detect the application or failed to be executed due to some runtime error which would abort the deployment. "LastStatusCode" would reflect the result.</p><p>Detection Successful indicates that the criteria were executed successfully and it successfully detected the application.</p></div></td></tr><tr><td class="confluenceTd" colspan="1">FINAL_DETECTION_INPROGRESS = 0x900,<br />FINAL_DETECTION_FAILED,<br />FINAL_DETECTION_SUCCESSFUL,</td><td class="confluenceTd" colspan="1"><p>Final detection verifies the execution result in previous step. It has the same implications as first detection.</p><p>Detection Failed indicates that the criteria defined was unable to detect the application or failed to be executed due to some runtime error which would abort the deployment. "LastStatusCode" would reflect the result.</p><p>Detection Successful indicates that the criteria were executed successfully and it successfully detected the application.</p></td></tr><tr><td class="confluenceTd">DEPLOYMENT_OPERATION_QUEUED = 0x000</td><td class="confluenceTd">a registry entry would be created under HKLM-&gt;SOFTWARE-&gt;AirWatchMDM-&gt;Queue</td></tr><tr><td class="confluenceTd" colspan="1"><p>&nbsp;DEPLOYMENT_OPERATION_FAILED = 0x40000000,</p><p>DEPLOYMENT_OPERATION_SUCCEEDED = 0x80000000,<br />DEPLOYMENT_OPERATION_SUSPENDED = 0xC0000000</p></td><td class="confluenceTd" colspan="1"><p><span>DEPLOYMENT_OPERATION_FAILED: There are some runtime/fatal errors thrown and the operation is aborted.</span></p><p><span><span>DEPLOYMENT_OPERATION_SUCCEEDED: The operation is successfully performed.</span></span></p><p><span><span><span>DEPLOYMENT_OPERATION_SUSPENDED: On certain conditions, the operation has to be suspended. The suspended operation would be reattempted on predefined interval.</span></span></span></p></td></tr><tr><td class="confluenceTd">DEPENDENCIES_INPROGRESS = 0x400,<br />DEPENDENCIES_FAILED,<br />DEPENDENCIES_SUCCESSFUL,</td><td class="confluenceTd"><p>Dependencies evaluation installs app dependencies. The installation of app dependency would go through the same deployment flow shown in this table.</p><p><span>DEPENDENCIES_FAILED: Dependencies evaluation precludes this operation. When dependency app deployment encounters failure on all evaluations here and the operation is considered being failed, for example, download failure or runtime error, this evaluation would fail.</span></p><p><span><span>DEPENDENCIES_SUCCESSFUL: <span>Dependencies evaluation allows this operation</span></span></span></p></td></tr><tr><td class="confluenceTd">CHECK_REFERENCE_COUNT_INPROGRESS = 0x200,<br />CHECK_REFERENCE_COUNT_FAILED,<br />CHECK_REFERENCE_COUNT_SUCCESSFUL,</td><td class="confluenceTd"><p>Reference Count is the count for app installation and number of apps who depends on it. The corresponding record/output for this stage is "InstallCount" in registry. The most significant bit in "InstallCount" is called "Permanent Bit" indicating whether the application is user installed. The remaining 31 bits are referring to actual reference count. If it is larger than 1 or it is equal to 0 for uninstallation, then the client state machine would preclude the following steps.</p><p><span>CHECK_REFERENCE_COUNT_FAILED: Reference count evaluation precludes this operation (install/uninstall). When the application is already installed/uninstalled, this evaluation would fail. When application is installed externally (user installed), this evaluation would fail. When any other runtime error happens, this evaluation would fail.</span></p><p><span>CHECK_REFERENCE_COUNT_SUCCESSFUL: Reference count evaluation allows this operation (install/uninstall).</span></p></td></tr></tbody></table>

{{% /expand %}}

### Steps

If the installation fails after status #2, **Install command dispatched**, take these steps to find the reason for the failure.

1.  In the AirWatch Console, validate the configurations for the Win32 application on the **Deployment Options** tab.
    1.  Go to **Apps & Books > List View > Internal** and edit the Win32 application.
    2.  Select **Edit > Deployment Options** tab.
    3.  In the **How To Install** section, review the **Install** **Context** configurations for **Device** or **User**.
    4.  Review the **Admin Privileges** setting.
    5.  Review the **Install Command** setting.
    6.  Side-load the application to the device to see if this actions triggers the install command.
2.  In the AirWatch Console, look at the Console Event Logs to find the reason for the failure in **HUB > Reports & Analytics > Events > Console Events**.
3.  Look for a failure reason on the device.
    1.  On the device, open a command-line session and run **regedit**. This opens the Registry Editor.
    2.  In the Registry Editor, go to **HKEY_LOCAL_MACHINE > SOFTWARE > AirWatchMDM** **> AppDeploymentAgent.**
    3.  Look in the **Queue** node at the log field.
    4.  If there is no **Queue** node, look for a node with the device or user SID. This value has the Win32 application product code. Select the product code to view the reason for installation failure.

Review the App Installer  Flow chart  for a depiction of how the device validates the pre and post installation checks.