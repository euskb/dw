+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "windows", "security" ]
description = "Windows Conditional Access"
title = "Windows SSO"
+++

External Links:

-   Livefire training - Adv Integrations: [https://euc-labs.livefire.solutions/m/84508/l/1090424-authentication-method-windows-10-sso-aw-ca](https://euc-labs.livefire.solutions/m/84508/l/1090424-authentication-method-windows-10-sso-aw-ca)
-   Troubleshooting Windows 10: [https://techzone.vmware.com/troubleshooting-windows-10-vmware-workspace-one-operational-tutorial#968027](https://techzone.vmware.com/troubleshooting-windows-10-vmware-workspace-one-operational-tutorial#968027)
-   TrueSSO in Win10 Chrome browser: [https://blog.eucse.com/windows-10-true-sso-using-chrome/](https://blog.eucse.com/windows-10-true-sso-using-chrome/)
-   Enabling Certificate Based Access – Windows 10/mac: [https://blog.eucse.com/workspace-one-enabling-certificate-based-access-windows-10-mac/](https://blog.eucse.com/workspace-one-enabling-certificate-based-access-windows-10-mac/)

## Configuration
-   You must have a CA with template for SSO. Refer to [MobileSSO for iOS](../../../access/mobile-sso-ios/index.html) for configuring internal AirWatch CA with SCEP protocol.
-   Check WS1 Access network ports. 

## Certificate Chain
-   From version of vIDM 1903+ Cert Auth Service port TCP7443 **must be opened** for vIDM Appliance (regardless of connector).
-   Also certificate chain and private key must be pasted into vIDM Appliance Admin Portal (accessed via port TCP8443).

Reference document - [https://docs.vmware.com/en/VMware-Identity-Manager/19.03/vidm-install/GUID-B625A0BA-2991-4F46-9D41-A1BD8C4D8BE2.html](https://docs.vmware.com/en/VMware-Identity-Manager/19.03/vidm-install/GUID-B625A0BA-2991-4F46-9D41-A1BD8C4D8BE2.html)
![](WinSSO02.jpeg)
![](WinSSO03.png)

### On AirWatch / Workspace ONE UEM side
-   If inner AirWatch CA/SCEP is used, go to **Groups & Settings → All Settings → Enterprise Integration → Workspace ONE Access → Configuration**, click Certificate → **EXPORT** button
-   Go to **Devices → Profiles & Resources → Profiles → ADD → Add Profile → Windows → Windows Desktop → User Profile** and make a User profile

{{% notice style="warning" %}}
There is also a **Windows Desktop →** **Device Profile**. Do **NOT** use it for SSO/Conditional Access, it will not work!
{{% /notice %}}

-   Give it a **Name**. Example: "Win10 SSO". Select a group in **Smart Groups.** For example, choose all devices (World icon)
-   Go to SCEP tab/payload, and set:  
    
    Credential Source: **AirWatch Certificate Authority  
    **Certificate Template: **Certificate (Cloud Deployment)  
    **Issuer: **CN=<Issuer name in certificate, example = name of current Organization Group>**

**On vIDM / Workspace One Access side**
-   Go to **Identity & Access Management → Manage** and select **Authentication Methods**  
-   Choose **Certificate (Cloud Deployment)** authentication method (pencil icon)
-   Check the **Enable Certificate Adapter**, then **Select File** and upload the Certificate (*.cer) which you downloaded from the AirWatch CA/SCEP from the step above, or from ADCS Domain CA
-   Click **Save**
-   Go to **Identity & Access Management →  Identity Providers**, click on **Built-in** IF you are NOT using the ESC connector. If you are using the Connector, choose it in the list
-   Find **Authentication Methods** area and select **Certificate (Cloud Deployment)** check box, then click **Save**
-   Go to **Identity & Access Management → Policies** and select **default_access_policy_set**, select **Edit**
-   In **Configuration** tab, **ALL RANGES**, select **Device Type** = **Windows 10**, and in "**then the user may authenticate using "**choose **Certificate (Cloud Deployment)**
-   (Optional) In "**if the preceding method fails or is not applicable, then"** choose **Password (Cloud Deployment)**
-   (Optional) Select the **(****+) ADD FALLBACK METHOD** and in "**If the preceding method fails or is not applicable, then"** select **Password ( Local Directory)**

## Windows 10 Device Tests and Checks  
-   On enrolled Win10 device, open MMC, select Menu **File → Add/Remove Snap-In...**, select **My User Account**  
-   Check Personal folder to see that the profile certificate was delivered
![](Win10SSO01.jpg)

-   Use Hub to access vIDM/Workspace ONE Access portal.

### Troubleshooting certificate issues
CertificateAuthAdapterBase function header for requesting the certificate from Windows:
```java
//function names & var names obfuscated
 
protected X509Cert[] getCert(@Nonnull String tenantId,
                             @Nonnull HttpServletRequest request,
                             @Nonnull HttpServletResponse response,
                             @Nonnull Map<String, String> attribVal,
                             @Nullable Map<String, String> inputParam) throws AuthAdapterConfigException { ... }
 
 
X509Cert[] certs = getCert(tenantId, request, response, attribVal, inputParam);
        if (certs == null || certs.length == 0) {
            logger.info("No certificates were provided by the browser"); // --> horizon.log
            if (certs == null) {
                adapterResponse.setStatus(AuthnAdapterResponse.AuthnStatus.FAILURE);
                logger.info(logId + " authentication failure, no certificate provided"); // --> horizon.log
```

getCert method returns the certificate, received from client browser HTTP-request. If not, it logs errors in **horizon.log** file on WS1 Access/vIDM.