+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "Configuring AirWatch right after installation"
title = "WS1 First Time Config"
+++

## GroupID
-   Switch to tenant - Company
-   Switch to **Groups & Settings → Groups → Organization Groups → Organization Groups Detail**
-   Set the company name, set GroupID, country and time zone

## iOS Agent
-   Switch to **Groups & Settings → All Settings → Devices & Users → Apple → Apple iOS → Intelligent Hub Settings,** click **Override**
-   Set **Background App Refresh** - for the AirWatch Agent works in the background and does not interfere with other apps
-   Turn on **Collect Location Data** - collection of data from GPS (Location Services)
-   Do not touch SDK profiles, leave settings as is currently

## AWCM
-   Launch the Admin Console **on the exact server, where AWCM is installed (FE)** and go to **Groups & Settings > All Settings > Device & Users > Android > Intelligent Hub Settings**
-   Switch **Use AWCM Instead of C2DM as Push Notification Service** - if you have Android devices with no Google Apps/Services, no Google Account, or want to restrict PUSH notifications to MDM-direct
-   Switch **AWCM Client Deployment Type** to Always Running, click **Save**
-   Go to **Groups & Settings > All Settings > System > Advanced > Secure Channel Certificate**
-   Check/configure the Windows environment variable JAVA_HOME - it must point to the last version installed c:\Program Files\jre-<номер версии>
-   Click **Download AWCM Secure Channel Certificate Installer** and launch the cert installer
-   Check the cert install: open **cmd** as Administrator and enter command such as:

    c:\Program Files\Java\jre1.8.0_131\bin>keytool.exe -list -v -keystore "C:\AirWatch\AirWatch 1811\AWCM\config\awcm.truststore"

    Enter password as the password for the keystore, and check there are 2 certificates entered, including the secure channel certificate
    Switch to tenant = **Global**, go to **Groups & Settings > All Settings > System > Advanced > Site URLs,** click **Enable AWCM Server** button at the end of the page

-   Check AWCM settings: internal and external DNS names (**they MUST be exactly those used in the corp certificate!**) and port number (**TCP2001**).


Port TCP 2001 MUST be open FROM the outside to server with AWCM (Device Services - FE) in order for direct PUSH to work with Android, and Windows Phone/Desktop devices.

If the external DNS name is published on an external proxy or load balancer, and the inner servers do not know this, use **hosts** file on Admin Console (BE) server and AirWatch Cloud Connector (ACC) / Enterprise Systems Connector (ESC) to make an alias of external DNS name and internal IP of Device Services (FE) server.

AWCM Troubleshooting - [see article](docs/uem/AWCM-ECC-Troubleshooting).

## APNs certificate for Apple and SSL for Apple profiles

-   Launch Admin Console using Firefox, Safari or Chrome (**IE not supported!**). Go to **Groups & Settings → All Settings → Devices & Users → Apple** → **APNs for MDM**
-   Download plist file.
-   Prepare an [AppleID account](https://appleid.apple.com), click **Go to Apple**
-   On Apple website click **Create certificate**, accept the terms, upload the plist file, download the corresponding PEM file on local disk
-   Return to the Admin Console - click **Next**, upload the PEM file and enter the corresponding AppleID click **Save**. Enter the PIN code of the console administrator
-   Go to **Groups & Settings → All Settings → Devices & Users → Apple** → **Profiles**
-   Click **Upload** and choose the PFX file of the corp certificate enter the password of the PFX container

## Google Play Registration and Android for Enterprise/Legacy Enrollment

-   Launch Admin Console using Firefox, Safari or Chrome (**IE not supported!**). Switch to **Groups & Settings → All Settings → Devices & Users → Android → Android EMM Registration**
-   Click **Register with Google**
-   Proceed with steps on Google website, entering the GMail Account (**each GMail Account may only be used ONCE for 1 ЕММ system (any)**)

For old AirWatch Console 9.0.1 and earlier, if upgraded to latest version of UEM, the **Enable Play Store** button should be clicked.

-   Open **Enrollment Restrictions** tab: choose **Define the enrollment method for this organization group**. Default is **Always use Android**, which means to always use Android for Enterprise of type = Device Work Profile (duplication of software into BYOD/Corp containers). If devices with potential AfE support in current group are to be enrolled and managed using Android Legacy ELM/POEM drivers, then choose **Always use Android (Legacy)** in list, or choose hybrid mode by defining user groups for AfE: **Define Assignment Groups that use Android**.



After choosing the EMM registration method, **DO NOT CHANGE IT** with many devices enrolled. Consequences:

-   Profiles can still be installed on the device as they’re being installed directly from Workspace ONE;
-   Communication is maintained between the device and Workspace ONE UEM;
-   You will be unable to leverage any Play store services;
-   No new apps added to Workspace ONE will be visible on the device managed play store;
-   Previously added applications in Workspace ONE will no longer be deployable from the console.

Source - [https://blog.eucse.com/things-not-to-do-workspace-one-changing-android-emm-registration/](https://blog.eucse.com/things-not-to-do-workspace-one-changing-android-emm-registration/)

## AirWatch Cloud Connector (ACC) / Enterprise Systems Connector

-   Launch the Admin Console **on the exact server, where ACC is to be installed** and switch to a **non-Global** Tenant

You cannot download the ACC distrib on one server, then copy and launch on another!

-   Go to **Groups & Settings > All Settings > System > Enterprise Integration > Cloud Connector,** turn on **Override**, switch **Enable AirWatch Cloud Connector** and **Enable Auto Update**

It is strongly recommended to configure ACC on non-Global level

-   Switch to **Advanced** tab, click **Generate Certificate** button to create the connection certificate to AWCM
-   Choose ****Use Internal AWCM URL**** - if the connector is in LAN, and AWCM - in DMZ
-   Use buttons to switch ON services/components, which will talk to the connector (the usual minimum is LDAP, CA, SCEP, Syslog, )
-   Switch back to **General** tab, choose **Download Enterprise Systems Connector Installer** link, enter password
-   Install .NET Framework 4.6.2 on server
-   Launch the downloaded installer
-   Enter the password for certificate

### Check the installation

-   Go to **Groups & Settings > All Settings > System > Enterprise Integration > Cloud Connector**
-   Click **Test Connection** and check that the connector is available

"Error : Reached AWCM but VMware Enterprise Systems Connector is not active" is resolved by server reboot and opening TCP2001 port from Cloud Connector to AWCM.

## Active Directory

-   Use Company tenant, go to **Groups & Settings > All Settings > System > Enterprise Integration > Directory Services**
-   Choose **Skip wizard and configure manually** (alternative path **Accounts > Administrators > Settings > Directory Services**)
-   Enter domain data:
    -   **Directory Type** = Active Directory
    -   **Server** - domain controller name
    -   **Encryption Type** = None
    -   **Bind Authentication Type** - connection type = **GSS-Negotiate** is recommended, which means choose automatically Kerberos or NTLM depending on what is available
    -   **Bind Username** - enter the service account user for reading the domain as <domain>\<user>
    -   **Bind Password** - enter the domain service account password
    -   In **Domain - Server** fields enter the suffix of the domain and the name of the domain controller
-   Click **Test Connection**, check there is network access to the domain controller
-   Choose the **User** tab, **DN** field - choose the topmost level from the list
-   Choose the **Group** tab, **DN** field - choose the topmost level from the list
-   Click **Save**

Troubleshooting connection to Active Directory - [see article](http://192.168.1.3:8090/display/AIRWATCH/AirWatch+and+Microsoft+Active+Directory).

### Self-enrollment of Active Directory Users

-   In Company tenant go to **Groups & Settings > All Settings > **Device & Users >** General > Enrollment
    **
-   In **Authentication Mode(s)** choose **Directory** checkbox
-   Go to **Restrictions**, make sure that **Restrict Enrollment To Known Users** and **Restrict Enrollment To Configured Groups** are disabled.

### Batch Import and Message Templates

To Batch Import users in an AirWatch group, this group needs a Group ID, which allows Enrollment into it.

During user import, a connection token can be distributed via EMail. The template language depends on the localization configuration of the specific Organization Group.

When defining localization on the topmost level, the sub-groups of the lower level may have a strange setting like "Select*". It is recommended to specify the localization settings on each group and sub-group, so there is no obscurity in the settings.

## Configure SDK default profiles

-   Switch to **Groups & Settings → All Settings → Apps → Settings & policies → Security Policies,** click **Override**
-   Leave **Passcode** turned on
-   Activate **Single Sign-On**
-   Activate **Integrated Authentication** to auto-enter apps and websites, put **Enrollment Credentials** and write star symbol ( * ) in mask = all websites
