+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "AirWatch verify installation was correct"
title = "WS1 Verify Installation"
+++

# Verify Installation
-   Open AirWatch Console
-   Choose About Airwatch - check the version
-   Check the Site Links: open **Groups & Settings > All Settings > System > Advanced > Site URLs** and look through the links
    -   Only Peripheral Service URL should be "localhost"
    -   Google Play has a defined port
    -   Change SOAP and REST API URL links, instead of AirWatch Console URL put AirWatch Devices Services сервер - for example, instead of [https://acme-console.com/AirWatchServices](https://acme-console.com/AirWatchServices) put [https://acme-ds.com/AirWatchServices](https://acme-ds.com/AirWatchServices) and instead of [https://acme-console.com/API](https://acme-console.com/API) put [https://acme-ds.com/API](https://acme-ds.com/API).
-   Check the connection with Device Services server with a defined in the install phase external URL, signed with external certificate (type of link: https://<DS_URL>/DeviceManagement/Enrollment  )
-   Check the AWCM component, using link https://<DS_URL>:2001/awcm/status*
-   Check AirWatch services - launch **services.msc** in Windows Server and check that AirWatch services are Started
-   Check the GEM Inventory Service: go to the AirWatch Console server, in the folder ```C:\AirWatch\Logs\Services\``` and delete the file **AirWatchGemAgent.log**; open **services.msc** and restart GEM Inventory Service. New log will either NOT show up, or show up without errors.  

<!-- *Also see: [AWCM and ESC Troubleshooting page](http://192.168.1.3:8090/display/AIRWATCH/AWCM+and+ESC-ACC+troubleshooting).  -->
