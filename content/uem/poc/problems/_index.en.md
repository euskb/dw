+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "troubleshooting" ]
description = "AirWatch problems during or after installation"
title = "WS1 Installation Problems"
+++

## Failed to extract custom package message

Console installer needs to be extracted to the **root disk C:\**

## AirWatch Services are not started before timeout

1.  Click Start, click Run, type regedit, and then click OK.
2.  Locate and then click the following registry subkey: **HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control**
3.  In the right pane, locate the **ServicesPipeTimeout** entry.

**Note**: If the **ServicesPipeTimeout** entry does not exist, you must create it. To do this, follow these steps:

 - On the Edit menu, point to New, and then click **DWORD** Value.

 - Type ServicesPipeTimeout, and then press ENTER.

4. Right-click ServicesPipeTimeout, and then click Modify.

5. Click Decimal, type 60000 (default is 30000, use numbers from 60000 to 125000), and then click OK.

 - This value represents the time in milliseconds before a service times out.

6. Restart the computer.

## Database is not getting updated on upgrade of WS1 UEM
Error detected on upgrade to WS1 UEM 2108+

SQL update fails with error:

```Only members of sysadmin role are allowed to update or delete jobs owned by a different login.```

⭐️ Give account used for WS1 UEM db the MSSQL server role of **sysadmin**.

## AirWatch Self-Service Portal gives error

"HTTP error 503: The service is unavailable"

The Self Service Portal has an associated App Pool in IIS. If the App Pool is not started we see this error.

Ensure that the SSP App Pool is started in IIS.

## Log collection

See [AirWatch Services and Devices log collection page](uem/logs/index.html)

## App Catalog not seen after successful enroll

**Problem:** App Catalog does not automatically appear after device enrollment.
**Solution:**
1.  In AirWatch Admin Console go to **Groups & Settings > All Settings > Apps > Catalog > General > Publishing >** repeat **SAVE** procedure
2.  Alternative: create separate webclip profile for all devices with URL: https://{DS_URL}/Catalog/ViewCatalog/{SecureDeviceUdid}/{DevicePlatform}
External link: https://kb.vmware.com/s/article/50100220?lang=en_US