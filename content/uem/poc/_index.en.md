+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "AirWatch PoC installation"
title = "WS1 UEM Installation"
+++

## Articles in section

{{% children sort="weight" description="true" %}}

# Installation
❗️All Windows systems, which will be used to deploy AirWatch / Workspace One UEM, in localization settings, there should be **language = US-EN**.

❗️Special attention to Regional Settings on floating point identifier: it must be a dot, not a comma!

❗️All Windows systems, which will be used to deploy AirWatch / Workspace One UEM, should have **all current patches applied**. For example, early versions of Win2012R2 have broken ASP.NET and break AirWatch installation.

## Legend

-   **BE** - (BackEnd server) WS1 UEM Admin Console
-   **FE** - (FrontEnd server) WS1 UEM Device Services
-   **SQL** - Microsoft SQL Database Server
-   **UEM** - AirWatch / Workspace One UEM

## Database Deployment
❗️See first [SQL Recommendations](docs/uem/SQL-Requirements) page before production or semi-production deployment. 

-   Install SQL
-   Login to the SQL server, launch SQL Management Studio;
-   Create a new database. In database settings apply **General → Autogrowth / Maximize** → **File Growth → In Megabytes = 128**;
-   Choose collation: **Options → Collation → SQL_Latin1_General_CP1_CI_AS**;
-   <span style="color:red"> SQL 2008 and MS SQL 2008R2 are not supported anymore</span>. For MS SQL 2016 choose **Options → Compatibility Level = 2014**

   ❗️Issues currently detected with installing Workspace ONE UEM up to version 1909 in Microsoft SQL 2017. Services do not start after install, console does not launch.
    As recommended by Microsoft for SQL 2017, services should use the **<generatePublisherEvidence>** element to improve startup performance. Using this element can also help avoid delays that can cause a time-out and the cancellation of the service startup. See [Microsoft KB article.](https://docs.microsoft.com/en-us/dotnet/framework/configure-apps/file-schema/runtime/generatepublisherevidence-element)

-   Create a user in Mixed-mode SQL (non-domain), with **sysadmin** permissions for server and **db_owner** for database. Gice the user permissions for **msdb**: **SQLAgentUserRole**, **SQLAgentReaderRole**, **db_datareader** roles. Do not forget to cancel password expiration for this user;
-   <span style="color:red">If there is not Internet on DB server</span> - download Microsoft .NET Framework 4.6.2 from Microsoft website for English Windows on separate computer and copy to this server (during setup WorkspaceONE_UEM_DB_XX.YY.Z.K_Setup tries to download the framework itself, with no Internet it may hang the installation process for some time);
-   Copy **WorkspaceONE_UEM_DB_XX.YY.Z.K_Setup** files to the server, launch it. Copy to **C:\Distr\**;

❗️Do NOT launch installer from C:\Users\Documents and Settings\Downloads etc folders - long path may cause unpacking error.

-   Enter "localhost" in install wizard, login and password of the SQL user, and choose the UEM database;
- ❓️ If the database is created on AlwaysOn Cluster - turn on **Using SQL AlwaysON Availability Groups** option;
-   Wait for installer to end (10-15min). Install progress can be seen as log file growth in **c:\AirWatch\AirWatch 1811\Database\AWDatabaseLog*.txt** (the log will grow up to 2.3Mb when the installation will finish);
-   Check the installation, use SQL Management Studio to launch a script:

```select * from dbo.DatabaseVersion;```

The answer should be the UEM version number.

❓️ For AlwaysOn cluster - do not forget to clone the database Jobs on the other cluster nodes!

## Device Services Front-End (FE) Server

-   Enter Windows Server Manager and check the following roles/features (double-check [official doc](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2111/UEM_Installation/GUID-AWT-CONFIGURE-APP-SERVERS.html) for feature list):
    -   Web Server (IIS)
    -   Web Server (IIS) → Web Server → Common → Static Content, Default Document, Directory Browsing, HTTP Errors, HTTP Redirection
    -   Web Server (IIS) → Web Server → Performance → Dynamic Content Compression
    -   Web Server (IIS) → Web Server → ASP
    -   Web Server (IIS) → Web Server → ASP.NET 4.5
    -   Web Server (IIS) → Web Server → Security → IP & Domain Restrictions
    -   Web Server (IIS) → Web Server → Health & Diagnostics → Request Monitor
    -   Web Server (IIS) → Web Server → Application Development → Server Side Includes
    -   .NET Framework 4.5 → WCF → HTTP Activation
    -   Message Queuing
    -   Telnet Client

❗️ <span style="color:red">**DON NOT** turn on **Web Server (IIS) → Web Server → Common → WebDav Publishing** - this will lead to multiple bugs in managing iOS devices</span>

- ❓️ If there is not Internet on FE server - download and install NET Framework 4.6.2 ([Microsoft .NET Framework 4.6.2 (Offline Installer) for Windows 7 SP1, Windows 8.1, Windows Server 2008 R2 SP1, Windows Server 2012 and Windows Server 2012 R2](https://www.microsoft.com/en-us/download/details.aspx?id=55167)). Reboot server;
- ❓️ If there is not Internet on FE server - download and install URL Rewrite Module 2.0 ([https://go.microsoft.com/?linkid=9722532](https://urldefense.proofpoint.com/v2/url?u=https-3A__go.microsoft.com_-3Flinkid-3D9722532&d=DwMF3g&c=uilaK90D4TOVoH58JNXRgQ&r=0GsqnR5P4bztHRKXPR3Sx7GSoehDSiG0CQLWqidhhrk&m=I36E5ZVwgWipx94UmskYWtX08X25nV7WjAO7oYdjDC0&s=dlriUZqDEdFlHNFStGUUZPptcK4KBt8TjgP2Uhd43L8&e=)) for IIS. Old version of Rewrite Module 2.0 provided on this page as attachment in case of need;

-   Upload an external certificate in PFX format with private key. Password used to protect the certificate <span style="color:red">**MUST be 6+ characters long. Short password will lead to problems with AWCM Java keystore**!</span> Install the certificate into **Local Machine** account, leaving **Automatic Detect** option for certificate type. Also install any root and intermediate certificates of the certificate trust chain. **Subject Alternative Name** of the certificate <span style="color:red">**MUST contain the external DNS name**</span> of the server!
-   Check correct start config of IIS - use browser to go to [http://127.0.0.1/](http://127.0.0.1/) (start page of IIS must be present)
-   Go to IIS admin console, bind the certificate: in sites tree choose **Default Web Site → Bindings** menu → **Add..**, choose **https**, in **SSL Certificates** list choose the certificate from previous step. Enter the external DNS name of the server, which is written in the certificate. 

❗️Port binding is needed ONLY for Device Service and Console Service.

-   Launch installer **WorkspaceONE_UEM_Application_X.X.X.X_Full_Install**. Choose **Continue setup without importing/exporting config file**;
-   In modules selection choose only **Device Services**, select **This feature will not be available** for **Admin Console**, continue installation;

❗️**For AirWatch 9.2.2+**: during installation, AirWatch installer deploys SQL Native Client, which may not have enough time to initialize during the work of the wizard. During SQL check, an error may be generated, that SQL is not found. Press **Cancel** and reboot the server, then re-launch the setup process.

-   Enter SQL data: in full database name, only enter the server name, do not enter SQL Instance name;
-   Specify the DNS name for reaching the server by HTTPS from outside and inside. Do not choose SSL Offload - it is much easier to make all connections as HTTPS and then edit configuration;

⭐️ Instead of choosing different DNS names and then have issues with AWCM, I recommend to enter the same external name for Device Services and Web Console (check **Same as above?** option). After this, make an alias on the local DNS server, or use the **hosts** file on Admin Console/BE server to alias the external name of Directory Services/FE to an internal IP address.

-   Choose **Default Web Site** as install target;
-   Leave **AWCM listening IP** as **0.0.0.0** since it is installed locally, and **port 2001** for connection. Install the PFX certificate and enter its' password; 

❗️<span style="color:red">The PFX certificate MUST be created with **Export All Properties** option! Or the Java keytool will not be able to import it into awcm.keystore, and it will not give errors in the log! But AWCM will not work!</span>

-   Choose **Implicit Clustering** (do not cluster AWCM);
-   Wait for install completion. AirWatch Certificate Installation Wizard will open, click **Next** and choose **SQL Authentication**. If Internet is accessible, a code must be entered. For offline installation, click **Get File** and save the *.plist fiel on disk;
-   Go to [my.workspaceone.com](https://my.workspaceone.com): **My Workspace One menu → My Company → Certificate Signing Portal → Authorize Install → Generate a token** (for Internet access);
    OR
-   **My Workspace One menu → My Company → Certificate Signing Portal → Authorize Install → Upload Your File** (for offline), and upload *.plist file.
-   Save the **certs.plist** answer file and upload it in the installation wizard, thus ending the installation.

❗️AirWatch (WOne UEM 1909) services may not start due to timeout error on Windows 2008-2012.
Increase Timeout time in Windows registry: **HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control → ServicesPipeTimeout=180000**
External link: [https://kb.vmware.com/s/article/50105044?lang=en_US](https://kb.vmware.com/s/article/50105044?lang=en_US)

## Admin Console Back-End (BE) Server

-   Enter Windows Server Manager and check the following roles/features (double-check [official doc](https://docs.vmware.com/en/VMware-Workspace-ONE-UEM/2001/UEM_Installation/GUID-AWT-CONFIGURE-APP-SERVERS.html#GUID-AWT-CONFIGURE-APP-SERVERS) for feature list):
    -   Web Server (IIS)
    -   Web Server (IIS) → Web Server → Common → Static Content, Default Document, Directory Browsing, HTTP Errors, HTTP Redirection
    -   Web Server (IIS) → Web Server → Performance → Dynamic Content Compression
    -   Web Server (IIS) → Web Server → ASP
    -   Web Server (IIS) → Web Server → ASP.NET 4.5
    -   Web Server (IIS) → Web Server → Security → IP & Domain Restrictions
    -   Web Server (IIS) → Web Server → Health & Diagnostics → Request Monitor
    -   Web Server (IIS) → Web Server → Application Development → Server Side Includes
    -   .NET Framework 4.5 → WCF → HTTP Activation
    -   Message Queuing
    -   Telnet Client

❗️ <span style="color:red">**DON NOT** turn on **Web Server (IIS) → Web Server → Common → WebDav Publishing** - this will lead to multiple bugs in managing iOS devices</span>

- ❓️  If there is not Internet on FE server - download and install NET Framework 4.6.2 ([Microsoft .NET Framework 4.6.2 (Offline Installer) for Windows 7 SP1, Windows 8.1, Windows Server 2008 R2 SP1, Windows Server 2012 and Windows Server 2012 R2](https://www.microsoft.com/en-us/download/details.aspx?id=55167)). Reboot server;
- ❓️ If there is not Internet on FE server - download and install URL Rewrite Module 2.0 ([https://go.microsoft.com/?linkid=9722532](https://urldefense.proofpoint.com/v2/url?u=https-3A__go.microsoft.com_-3Flinkid-3D9722532&d=DwMF3g&c=uilaK90D4TOVoH58JNXRgQ&r=0GsqnR5P4bztHRKXPR3Sx7GSoehDSiG0CQLWqidhhrk&m=I36E5ZVwgWipx94UmskYWtX08X25nV7WjAO7oYdjDC0&s=dlriUZqDEdFlHNFStGUUZPptcK4KBt8TjgP2Uhd43L8&e=)) for IIS. Old version of Rewrite Module 2.0 provided on this page as attachment in case of need;
-   Check correct start config of IIS - use browser to go to [http://127.0.0.1/](http://127.0.0.1/) (start page of IIS must be present)
-   Configure the certificate on IIS - for Admin Console on BE a self-signed certificate may be used:
    -   Enter IIS Admin Console, choose **Server Certificates**, and in the right column menu choose **Create Self-Signed Certificate**;
    -   Enter a name for the certificate, **type = Web Hosting**, click **ОК**;
    -   Go to IIS admin console, bind the certificate: in sites tree choose **Default Web Site → Bindings** menu → **Add..**, choose **https**, in **SSL Certificates** list choose the certificate from previous step.

❗️Port binding is needed ONLY for Device Service and Console Service.

-   Launch installer WorkspaceONE_UEM_Application_18.11.0.3_Full_Install. Choose **Continue setup without importing/exporting config file**;
-   In modules selection choose only the **Admin Console**, choose **This feature will not be available** for **Device Services**, continue the installation;

❗️**For AirWatch 9.2.2+**: during installation, AirWatch installer deploys SQL Native Client, which may not have enough time to initialize during the work of the wizard. During SQL check, an error may be generated, that SQL is not found. Press **Cancel** and reboot the server, then re-launch the setup process.

-   Enter SQL data: in full database name, only enter the server name, do not enter SQL Instance name;
-   Specify the FQDN name for HTTPS access on Admin Console from the inside. Do NOT use a short name of DNS alias. Choose an External DNS name for access via HTTPS on Device Services server. Check the absence of space characters before or after the names. An error in this form may be corrected only by re-installing UEM!
-   Choose **Default Web Site** as the install target;
-   In **Company Profile** choose the company name and installation type = **Production**;


❗️AirWatch (WS1 UEM 1909+) services may not start due to timeout error on Windows 2012R2+.
Increase Timeout time in Windows registry: **HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control →** New 32-Bit DWORD: **ServicesPipeTimeout, Decimal=200000** (Decimal=60000 too small, put more!)

External link: [https://support.microsoft.com/en-us/kb/922918](https://support.microsoft.com/en-us/kb/922918)

Manually running services: under **Supplemental Software** → **QueueSetup** locate and run the **InstallQueues.Bat** file.

-   Check the installation:
    -   [https://localhost/airwatch](https://localhost/airwatch) - access to the Admin Console;
    -   [https://localhost/devicemanagement/enrollment](https://localhost/devicemanagement/enrollment) - Device Services webpage must be working;
    -   Reboot IIS if needed with **iisreset** command;
-   Enter the UEM console. Use Login: **administrator**, Password: **airwatch**. Choose a new password, choose a PIN-code and secret questions/answers pairs.

⭐️ Hardening of the IIS web-server for AirWatch/UEM Device Services is described [in this article](http://192.168.1.3:8090/display/AIRWATCH/AirWatch+Hardening+Guide).
