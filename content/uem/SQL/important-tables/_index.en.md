+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "Important tables in WS1 UEM DB description"
title = "WS1 UEM DB Important Tables"
+++

Each application uploaded to AirWatch would have its own row in the deviceApplication.Application table.  Each column contains specific attributes for that object.  For example, an application in the deviceApplication.Application table would have a column for Name, PackageID, LocationGroup, etc.

## Device Management Tables
**dbo.DeviceOperatingSystem**: OS versions supported by AirWatch Console in current version

**dbo.DeviceModelInfo**: Device models (for Apple) or Android vendors supported by AirWatch Console in current version

## User Management Tables
**dbo.CoreUser**: Admins – UserName, TimeZone, LastLogin, etc.

**mobileManagement.EnrollmentUser**: End users – UserName, LocationGroupID, DisplayName, UserPrincipleName, etc.

**mobileManagement.CurrentDeviceEnrollmentUser**: Ties enrolled devices to enrollment users

**dbo.UserGroup**: FriendlyName,DN,RootLocationGroupID, domain, etc

## MAM Tables

**deviceApplication.Application**: Internal applications

**deviceApplication.RecommendedExternalApplication**: All things public application

**deviceApplication.VPPLicensePool**: SmartGroup, Allocated licenses, Redeemed licenses, etc.

**deviceApplication.ApplicationGroup**: For blacklists/whitelists/required lists of apps

**smartGroup.SmartGroup**: info about the Smart Group

**smartGroup.AWEntitySmartGroupAssignmentMap**: Maps a Smart Group to the App/Book that uses it for assignment

## MEM Tables
**mobileEmailGateway.MEMConfig**
Contains attributes related to the Email Configuration, including pre-7.1 sets of System Codes

**mobileEmailGateway.MEMDevice**
Contains all the attributes for each device, whether managed or unmanaged

**mobileEmailGateway.MEMDeviceActivity**
Contains the status and activity info for each device

**mobileEmailGateway.MEMConfigProfile**
Contains associations between MEMConfig and EAS device profiles

**mobileEmailGateway.MEMDeviceConfig**
Contains associations between MEMConfig and MEMDevice. When an enrolled device receives an EAS profile, MEMDevice will receive corresponding MEMConfig

**mobileEmailGateway.MEMDeviceDiagnostic**
Contains diagnostics data for devices

**mobileEmailGateway.{all the rest}** 
Email Policy configurations, such as attachment encryption or OS policies

## MCM Tables

**enterpriseContent.Content**
All settings pertaining to uploaded content – offlineViewing, allowEmail, Name, etc.

**enterpriseContent.ContentRepository**
Details for repositories – Name, Link, AuthenticationUsername, User/Admin Repository

**enterpriseContent.ContentMap**
Links DeviceID, EnrollmentUserID, and ContentVersionID

**enterpriseContent.ContentVersion**
BlobID, Version, Size, Author, etc

## MDM Tables
**dbo.Device**
Device centric information – OS, Friendly Name, Serial Number, Phone Number, Enrollment Date, etc.

### Profiles
**DeviceProfile.DeviceProfile**
Settings for profiles – Name, Description, DevicePlatformID, ProfileAssignmentTypeID, etc.

**deviceProfile.DeviceProfileVersion**
Shows historic versions of a profile and their modified dates

**deviceProfile.DeviceProfileDevicePool**
Maps devices to profiles and profileVersions

**deviceProfile.deviceProfileSettingValue**
Maps actual profile settings to profileVersions

### Telecom
**dbo.Plan**
Telecom plan details

**dbo.PlanAssignmentRule**
Assignment rule details

**dbo.CellCard**
Cellular details

**MobileDataUsage, CellCardUsage**
Interrogator tables

### Compliance
**DeviceComplianceStatus**
Compliance status of devices

**Policy**
List of policies

### Device Samples
**ManagedAppList,ProfileListSample, SecurityInformationSample**
Interrogator tables

### Reports
**dbo.Report**
Info on all available reports

**dbo.ReportSubscription**
Created subscriptions

## Miscellaneous tables
**dbo.Certificate**
Thumbprint, SubjectName, Validity Dates, etc.

**dbo.LocationGroup**
OG info – LocationGroupID, Name, CreatedOn, CreatedBy, MDMSettingsInheritable

**deviceCommandQueue.DeviceQueue / deviceQueueAudit**
Current commands queued / already executed commands for devices

**dbo.SystemCode**
All settings and default values configurable in the AirWatch Console - Groups & Settings

**EventLog.EventLog**
Where console Event Logs pull from


### Example of deleting "wipe device" commands from Message Queue

This is needed in case of accidental mass wipe created as result of incorrect compliance policy etc.
```sql
SELECT * FROM deviceCommandQueue.DeviceQueue WHERE commandid = 10;
DELETE * FROM deviceCommandQueue.DeviceQueue WHERE commandid = 10;
```