+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "security" ]
description = "Admin password recovery via SQL"
title = "Admin Password Reset"
+++

# Changing an administrator's password

Changing an administrator's password requires first getting the CoreUserID for that user, and then updating the password and password salt for that user in the database.

### Getting the CoreUserID

Run the following SQL query to get the CoreUserID for the user.  Make sure to replace <USERNAME> with the administrator's username.  If more than one result are returned, make sure you identify the right entry.
```sql
Select * from CoreUser where UserName = '<USERNAME>';
```
### Updating the password and password salt

The following query will update the administrator's password.  Make sure to replace <COREUSERID> with the proper CoreUserID identified in the first section.  Have updating the password, log in with the account and reset the password to something else.
```sql
-- This Updates the password to 'a123b456c789'
UPDATE CoreUser SET Password = 'c2otxl1SURGxVibCX1K9IJvyizYl4ylnIIfXhwNtfe1iCuuVM8LNPK1oWWSwE3C3BB3AYxspGqrfXaVnryxjzw==' ,
PasswordSalt = '6eklCHP5ixaMT9RREfQbmi4Z2jc=' WHERE CoreUserID = <COREUSERID>
 
-- This Updates the password to 'Password123!'
UPDATE dbo.CoreUser SET Password = 'awhash4:IGdeYQ7eaHqyh3g6RelU0zkbHjoRW111/dg2xyqjiK0=:100000:K91XOTYtcw/20ZxDOUm7pe3DVLFA3XrzuUAYtWdl21M=' WHERE UserName = 'Administrator'
```

### Account lock-out
Check **IsLockedOut** flag - it must be set to 0 for active/unlocked account.
```sql
UPDATE dbo.CoreUser SET IsLockedOut = 0 WHERE UserName = 'Administrator'
UPDATE dbo.CoreUser SET LastLoginAttempts = 0 WHERE UserName = 'Administrator'
```