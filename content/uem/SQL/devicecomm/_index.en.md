+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "troubleshooting" ]
description = "Device communication queues"
title = "Device Command Queues"
+++

## Device operation sequence
A. AirWatch server sends out a PUSH notifications to device:
-   AirWatch prepares command, stores in command queue (SQL)
-   AirWatch sends check-in request to Messaging Service, one of the following:
    1.  Google Cloud Messaging: formerly C2DM (Cloud 2 Device Messaging) deprecated 2012 →  **Google Cloud Messaging (GCM)** deprecated April 2019 → Firebase Cloud Messaging (FCM)
    2.  Internal message bus for Android & Windows devices: **AirWatch Cloud Messenger (AWCM)**
    3.  Message bus for iOS: **Apple Push Notification Service (APNs)**
-   Messaging Service relays check-in request to managed device
-   Managed device checks into MDM server
-   MDM Server delivers commands to device, one-by-one

B. Device performs the commands;
C. Device contacts AirWatch server to report the result of the last commands and request new commands, if any.

## Command Queue
Every **pending** command is in SQL database:
```sql
SELECT * FROM deviceCommandQueue.DeviceQueue;
```
Every processed command is in SQL database:
```sql
SELECT * FROM deviceCommandQueue.DeviceQueueAudit;
```
Commands description is in SQL database:
```sql
SELECT * FROM deviceCommandQueue.DeviceQueueCommand;
```
Command statuses in SQL:
```sql
SELECT * FROM deviceCommandQueue.DeviceQueueStatus;
```

### Basic command types & statuses...
Device Management commands
-   Lock device
-   Query device
-   Send Message to device

Application commands
-   Install
-   Removal
-   Settings

Profile commands
-   Install
-   Removal

Normal Command Status Flow
**1** = Queued
**2** = Pending
**3** = Processed
**7** = Held (if batching is enabled)

## Troubleshooting Command Logs

Device Services (DS) server (front-end communication with managed device) logs:
-   DeviceServicesLogs (`C:\AirWatch → Logs → DeviceServices`)
-   InterrogatorQueueService (`C:\AirWatch → Logs → Services`)

Console (CN) server (communication with external services: APNs, GCM/FCM, AWCM, ACC etc.) logs:
-   MessengingServiceLog (`C:\AirWatch → Logs → Services`)
-   BulkProcessingServiceLog (`C:\AirWatch → Logs → Services`)
-   SchedulerServiceLog (`C:\AirWatch → Logs → Services`)

Targeted logging: AirWatch console → Device Details → More → Targeted Logging