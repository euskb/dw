+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "GPS" ]
description = "Query and insert GPS history"
title = "GPS History Table"
+++

-   GPS Poll Time Interval - X mins (configured in AirWatch Console, for example, in Android Agent settings)
-   Data Transmit Interval - Y mins

Privacy settings for "GPS Data" needs to be set to Collect and Display for required device Ownership type.

The samples which are transmitted by the Agent as per the **Y** interval are all stored in the database in the dbo.GPSLog table. To see if any samples have been reported for your specific device, use the following query:
```sql
SELECT * from dbo.LogSample (nolock) ls
inner join dbo.GPSLog (nolock) gl
on ls.logsampleID = gl.logsampleid
WHERE deviceid = ####
```
Under **device details > Location** tab on console, not all location data points will be reported:
-   Consecutive duplicate samples will not be reflected
{{% notice style="note" %}} 
Consecutive samples which are not at least 100 ft. apart will not be listed corresponding to the particular time-stamp as can be seen in the DB.
{{% /notice %}}

## Insert GPS History directly
Copy GPS coordinates from one device to another:
```sql
select * from device where Name like '%User01%' --deviceID = 7
select * from device where Name like '%User02%' --deviceID = 52
  
Insert dbo.LogSample ([DeviceUid] ,     [IpAddress] ,[SampleTime],[TransmitTime] ,[DeviceID] )
select      
--[LogSampleID] [int] IDENTITY(1,1) NOT NULL,
             [DeviceUid] ,[IpAddress] ,[SampleTime],[TransmitTime] ,
             52--[DeviceID] ,
       from dbo.LogSample  ls where deviceId = 7 and LogSampleID =8
  
  
select *  from dbo.LogSample  ls where deviceId = 52 --LogSampleID =9
  
Insert dbo.GPSLog ( [LogSampleID] ,[SampleTime],[Latitude] ,[Longitude] ,[Elevation] ,[Speed] ,
[Note] ,[LatitudeInternal] ,[LongitudeInternal] ,[ElevationInternal] ,       [SpeedInternal]
--,    [RowVersion]
                    )
       select 9 ,--[LogSampleID] ,      
             [SampleTime],[Latitude] ,[Longitude] ,  [Elevation] ,[Speed] ,[Note] ,[LatitudeInternal] ,[LongitudeInternal] ,     [ElevationInternal] ,     
[SpeedInternal] --, [RowVersion]
       from dbo.GPSLog  gl where LogSampleID in (select  LogSampleID from dbo.LogSample  ls where deviceId = 7and LogSampleID =8)
```