+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "performance" ]
description = "Memcached activation via SQL DB"
title = "Memcached Activation"
+++

## Activate usage of Memcached
```sql
DECLARE @myid uniqueidentifier
DECLARE @GroupOverrideID int
DECLARE @userID int
DECLARE @CacheEndpoint varchar(255)
DECLARE @EnabledOverrideID int
DECLARE @SolutionOverrideID int
DECLARE @EndpointOverrideID int
SET @myid = NEWID()
SET @userID = 52
SET @CacheEndpoint = '[
  {
    "name" : "mc_node1",
    "address" : "192.168.1.1",
    "port" : "11211"
  },
  {
    "name" : "mc_node2",
    "address" : "192.168.1.2",
    "port" : "11211"
  }
]
'
EXEC SystemCodeGroupOverride_Save @GroupOverrideID, 132, 7, NULL, 1, 1, 1, @UserID
IF (@GroupOverrideID IS NULL)
BEGIN
    SELECT TOP (1) @GroupOverrideID =  scgo.SystemCodeGroupOverrideID
    FROM dbo.SystemCodeGroupOverride scgo
    WHERE [SystemCodeGroupID] = 132
    AND LocationGroupID=7
    AND LocationID IS NULL
END
EXEC SystemCodeOverride_Save @EnabledOverrideID, 877, @GroupOverrideID, 'True', @userID
EXEC SystemCodeOverride_Save @SolutionOverrideID, 4147, @GroupOverrideID, '0', @userID
EXEC SystemCodeOverride_Save @EndpointOverrideID, 829, @GroupOverrideID, @CacheEndpoint, @userID
UPDATE dbo.DatabaseVersion set buildkey = CONVERT(varchar(255), @myid)
```

-   Login into [https://localhost/AirWatch/#/AirWatch/Installation/CacheAdmin](https://cnares.ssdevrd.com/AirWatch/#/AirWatch/Installation/CacheAdmin) and verify if cache is enabled.
-   Deactivate usage of Memcached:

```sql
DELETE from dbo.SystemCodeOverride where SystemCodeid in (829,877,4147)
DELETE from dbo.SystemCodeGroupOverride where SystemCodeGroupID = 132
```

### Check Memcached in SQL
```sql
SELECT * from dbo.SystemCodeOverride where SystemCodeID IN (829 , 827, 877, 4147)
```