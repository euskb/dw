+++
tags = ["MDM", "SQL", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "troubleshooting" ]
description = "Device status analysis in DB"
title = "Device Status"
+++

Workspace ONE Registered – Unmanaged WS1 device (MAM Only)
**ManagedBy** column in dbo.Device = 2
**EnrollmentCategoryID** column in dbo.DeviceExtendedProperties = 1 
Query:
```sql
select d.ManagedBy, dep.EnrollmentCategoryID, d.DeviceID from dbo.Device d INNER JOIN
dbo.DeviceExtendedProperties dep
ON
d.DeviceID = dep.DeviceID
where d.ManagedBy = 2
AND dep.EnrollmentCategoryID = 1
```

Workspace ONE Managed – MDM on the device, but did not enroll through agent (Adaptive Mgmt)
**ManagedBy** column in dbo.Device = 1
**EnrollmentCategoryID** column in dbo.DeviceExtendedProperties = 1 
Query:
```sql
select d.ManagedBy, dep.EnrollmentCategoryID, d.DeviceID from dbo.Device d INNER JOIN
dbo.DeviceExtendedProperties dep
ON
d.DeviceID = dep.DeviceID
where d.ManagedBy = 1
AND dep.EnrollmentCategoryID = 1
```

Workspace ONE Managed – MDM on the device, but did not enroll through agent (Direct Enrollment)
**ManagedBy** column in dbo.Device = 1
**EnrollmentCategoryID** column in dbo.DeviceExtendedProperties = 3
Query:
```sql
select d.ManagedBy, dep.EnrollmentCategoryID, d.DeviceID from dbo.Device d INNER JOIN
dbo.DeviceExtendedProperties dep
ON
d.DeviceID = dep.DeviceID
where d.ManagedBy = 1
AND dep.EnrollmentCategoryID = 3
```

Agent Enrollment with Workspace ONE – Workspace ONE as an app catalog, pushed as a managed app.
**ManagedBy** column in dbo.Device = 1
**EnrollmentCategoryID** column in dbo.DeviceExtendedProperties = NULL or 0
Query:
```sql
select d.ManagedBy, dep.EnrollmentCategoryID, d.DeviceID from dbo.Device d INNER JOIN
dbo.DeviceExtendedProperties dep
ON
d.DeviceID = dep.DeviceID
where d.ManagedBy = 1
AND dep.EnrollmentCategoryID IN (NULL, 0)
```

**EnrollmentCategoryID** is mapped to following enum:
```javascript
public enum EnrollmentCategory
{
    Unknown = 0,
    VmWorkspace = 1,
    Offline = 2,
    WorkSpaceOneDirectEnrollment = 3
}
```

**ManagedBy** is mapped to following enum:
```javascript
public enum DeviceManagedBy
{
    /// <summary>
    /// Unknown value
    /// </summary>
 
    Unknown = 0,
 
    /// <summary>
    /// Managed By MDM
    /// </summary>
 
    MDM = 1,
 
    /// <summary>
    /// Managed by Workspace (newer terminology is 'Container')
    /// </summary>
 
    Workspace = 2,
 
    /// <summary>
    /// Managed by Application Catalog (Web MAM enrollment)
    /// </summary>
 
    AppCatalog = 3,
 
    /// <summary>
    /// Managed by Application
    /// </summary>
 
    AppLevel = 4,
 
    /// <summary>
    /// managed by VmWorkspace
    /// </summary>
 
    VmWorkspace = 5,
 
    /// <summary>
    /// managed by Offline (Enrolled Offline)
    /// </summary>
 
    Offline = 6
}
```