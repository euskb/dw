+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "Internal apps upload and config"
title = "Internal Apps"
+++

## Articles in section

{{% children sort="weight" description="true" %}}

## Apps Upload
Automatic application upload can be made using the following CURL command via blob file:
```shell
curl -k -v -X POST -H 'Authorization: Basic ***' -H 'aw-tenant-code: ***'
-H 'Content-Type: application/octet-stream' -H 'Accept: application/json;version=1'
-H 'Expect: 100-continue' --data-binary '@./TestApp2.ipa'
'https://airwatch.testserver.ru/API/v1/mam/blobs/uploadblob?fileName=TestApp2.ipa&organizationGroupId=571'
```
Important moments:
**-H 'Expect: 100-continue'** MUST be sent, because else the server answers with code 100, and curl does not know what to do;

**-H 'Content-Type: application/octet-stream'** MUST be specified, because in the original text we have some JSON or similar.

As result we receive the needed:
```json
Value {"uuid":"7182fd4c-7661-4753-b4d3-fd9353ad4c6a","Value":856}
```

We take this value and do **begininstall**
```shell
curl -k -v -X
POST -H "Authorization: Basic ***" -H "aw-tenant-code: ***"
-H "Accept: application/json" -H "Content-Type: application/json" -d
'{"ApplicationName":"TestApp20", "AutoUpdateVersion":true, "BlobId":856,
"PushMode":"Auto", "DeviceType":"Apple", "LocationGroupId":571,
"SupportedModels":{"Model":[{"ApplicationId":139, "ModelId":2,
"ModelName":"iPad"}]}}' "https://airwatch.testserver.ru/API/v1/mam/apps/internal/begininstall"
```

{{% notice style="note" %}}
What is Blobid? Not fully understood. Tried different numbers.. 
{{% /notice %}}

As an answer we receive:
```json
{"ApplicationName":"TestApp20","BundleId":"ru.testserver.TestApp3",
"AppVersion":"2.54.0","Platform":2,"IsReimbursable":false,"ApplicationSource":0,
"LocationGroupId":571,"OrganizationGroupUuid":"24e7ceee-d233-4697-9fb9-78e32ee25462",
"PushMode":0,"AppRank":0,"AssignedDeviceCount":0,"InstalledDeviceCount":0,
"NotInstalledDeviceCount":0,"AutoUpdateVersion":false,"EnableProvisioning":false,
"IsDependencyFile":false,"ContentGatewayId":0,"Id":{"Value":141},"Uuid":"dde99cc2-228d-4886-8fc8-c28b82683c36"}
```