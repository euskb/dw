+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware" ]
description = "Important tools for MDM admin"
title = "Important Tools"
+++

# General Tools

-   **Portecle** Java keystore GUI - [http://sourceforge.net/projects/portecle/](http://sourceforge.net/projects/portecle/)
-   **LDAP Directory Tools** - [http://www.computerperformance.co.uk/ScriptsGuy/ldp.zip](http://www.computerperformance.co.uk/ScriptsGuy/ldp.zip)
    [LDAP Directory Tools instructions](http://www.computerperformance.co.uk/w2k3/utilities/ldp.htm)
-   **DNS Propagation checker**. Use to confirm that registered FQDN has fully propagated - [https://www.whatsmydns.net/](https://www.whatsmydns.net/)
-   **Port Forwarding Tester**. Tests if a firewall port is open to the Internet - [https://www.yougetsignal.com/tools/open-ports/](https://www.yougetsignal.com/tools/open-ports/)
-   **NMap** Security Scanner for Network Exploration & Hacking - [https://nmap.org/](https://nmap.org/)
-   **Postman** REST API Client - [https://www.getpostman.com/](https://www.getpostman.com/)
-   **Workspace One Topology diagram** tool - [https://tools.techzone.vmware.com/static/topology/index.html](https://tools.techzone.vmware.com/static/topology/index.html#profile)
-   **AirWatch Pre-Installation Verification** Toolset (internal): [https://support.air-watch.com/resources/115008871747](https://support.air-watch.com/resources/115008871747)

<details>
<summary><b>Verification Tool Description...</b></summary>

-  The pre-installation verification tool is an exe file that can be loaded onto the AirWatch app servers and will perform standard checks on the local machine, to the DB, and to the internet to ensure that the environment meets the VMware AirWatch Pre-Reqs.
-  Once installed, certain information is required, such as DB server and username/PW, and once entered takes you through the checks and presents the results in the application. Should it need to be shared there is an export option that creates an Excel file.
-  There is also additional functionality tests related LDAP, SMTP, Exchange, SSRS, and PKI

</details>

# OS Specific Tools

## iOS

-   **Network Analyzer Pro** all-in-one iPhone and iPad app for network analysis, scanning and problem detection - [https://itunes.apple.com/au/app/network-analyzer-ping-traceroute/id557405467?mt=8](https://itunes.apple.com/au/app/network-analyzer-ping-traceroute/id557405467?mt=8)
-   **iOS Device Logs** console log viewer - [http://lemonjar.com/iosconsole/](http://lemonjar.com/iosconsole/)
-   **iTools for Windows**. Allows you to install applications on, move files to and from, and otherwise maintain your iOS device, such as an iPod or an iPhone - [http://itools-for-windows.en.softonic.com/download](http://itools-for-windows.en.softonic.com/download)

## MacOS

-   **VMware AirWatch Admin Assistant** - [https://awagent.com/AdminAssistant/VMwareAirWatchAdminAssistant.dmg](https://awagent.com/AdminAssistant/VMwareAirWatchAdminAssistant.dmg)
    AppCast.XML file for Admin Assistant auto-update: [https://awagent.com/AdminAssistant/VMwareAirWatchAdminAssistant.xml](https://awagent.com/AdminAssistant/VMwareAirWatchAdminAssistant.xml)
-   Automation framework for MacOS software:

-   -   **Autopkg** - [https://github.com/autopkg/autopkg](https://github.com/autopkg/autopkg)
-   **Autopkgr** - [https://github.com/lindegroup/autopkgr](https://github.com/lindegroup/autopkgr)

## Android

-   **Android for Enterprise change EMM** **account** - [https://play.google.com/work/adminsettings](https://play.google.com/work/adminsettings)
-   **Workspace One Hub (AirWatch Android Agent)** APK:
  1.  [https://awagent.com/mobileenrollment/airwatchagent.apk](https://awagent.com/mobileenrollment/airwatchagent.apk)
  2.  [https://discovery.awmdm.com/mobileenrollment/airwatchagent.apk](https://discovery.awmdm.com/mobileenrollment/airwatchagent.apk)
-   **Android Root Checker** on Play Store: [https://play.google.com/store/apps/details?id=com.joeykrim.rootcheck](https://play.google.com/store/apps/details?id=com.joeykrim.rootcheck%5D)
-   **AirWatch Android Diagnostics** Tools (internal): [https://resources.air-watch.com/view/krd5xl2b9fdt24nsj8z8/en](https://resources.air-watch.com/view/krd5xl2b9fdt24nsj8z8/en)
    Check why AirWatch shows Android device as compromised.

Description of tool...

## Windows 10/11

-   Intelligent Hub/AirWatch Agent download - [https://getwsone.com/](https://getwsone.com/) OR [https://getws1.com/](https://getws1.com/)
-   [https://www.vmwarepolicybuilder.com/](https://www.vmwarepolicybuilder.com/)
    XML/SyncML custom profile builder engine for different builds of Windows 10

## Windows Phone (support deprecated)

-   [Windows Phone Certificates](https://www.windowsphone.com/en-au/store/app/certificates/c356e3f2-451f-4d82-87df-e53b644bec01)
    Manage personal and enterprise certificates on Windows Phone

# Other Tools

## SSL Checkers

-   [SSLShopper SSL Checker](https://www.sslshopper.com/ssl-checker.html)
-   [SSL Tools](https://ssltools.websecurity.symantec.com/checker/views/certCheck.jsp)
-   [SSL Labs](https://www.ssllabs.com/ssltest/)
-   [SSL Checker](https://www.sslchecker.com/insecuresources)
-   [BlueSSL SSL Tester](https://www.bluessl.com/en/ssltest)

## EAS Troubleshooting Tools

-   [Microsoft Remote Connectivity Analyzer](https://testconnectivity.microsoft.com/)
    ActiveSync, LDAP and O365 Connectivity Tool
    The tool can identify: Host connectivity and name resolution (DNS) problems, Exchange Server/ActiveSync configuration issues, etc.

-   [AccessMyLan ActiveSync Tester](https://store.accessmylan.com/main/diagnostic-tools)
    PC and iOS testers available
