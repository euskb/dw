+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "ldap" ]
description = "Integration with Novell eDirectory"
title = "Novell eDirectory"
+++

## Server Settings
![](1-EnServer.png)

## User Settings
![](2-EnUser2.png)

## Advanced User Settings
![](3-Enuser2a.png)

## Group Settings
![](4-EnGroup3.png)

## Advanced Group Settings
![](5-Engroup3a.png)