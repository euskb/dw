+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "ldap", "active", "directory" ]
description = "Integration with different LDAP Catalogs"
title = "Integration - LDAP"
+++

## Linked Articles

{{% children sort="weight" description="true" %}}

### List of LDAP Queries used in Directory Searches (Active Directory and NON-Active Directory) 
  
**Search Group**  
```
(&(objectClass=group)(|(CN=*{inputName}*)(distinguishedName={inputName})))  
```

**Sync Group**  
```
(&(objectClass=group)(|(objectGUID={ExternalID1})(objectGUID={ExternalID2}))) {ExternalID}
```
 - Hex Value if the object identifier has GUID value - String Value if the object identifier has string value  
  
**Search User**  
```
(&(objectCategory=person)(sAMAccountName={InputUserName}))  
```

**Sync User**  
```
(&(objectCategory=person)(sAMAccountName=*)(|(objectGUID={ExternalID1})(objectGUID={ExternalID2}))  
```
  
**Search Group member**  
```
(&(objectCategory=person)(sAMAccountName=*)(memberOf={GroupDN})) (&(objectClass=Group)(memberOf={GroupDN})) -- For recursive member search {GroupDN} - Group's distinguishedName value  
```
 
**Add Missing Users**  
```
(&(objectCategory=person)(sAMAccountName=*)(|(distinguishedName={UserDN1})(distinguishedName={UserDN2})))
```

A sample how LDAP query works on the customer envirionment.  

--sample---
```shell
ldapsearch -h gvx0lsami01q.company.com -p 389 -b O=COMPANY cn=EU_MDM_AirwatchEnabled,ou=groups,O=COMPANY member
```

Output:
```shell
C:\tools>ldapsearch -h gvx0lsami01q.company.com -p 389 -b O=COMPANY cn=EU_MDM_AirwatchEnabled member  
cn=EU_MDM_AirwatchEnabled,ou=groups,O=COMPANY  
member=secAuthority=Default  
member=cn=emmatest01,ou=eu,O=COMPANY  
member=cn=emmatest02,ou=eu,O=COMPANY  
member=cn=emmatest03,ou=eu,O=COMPANY  
member=cn=emmatest04,ou=eu,O=COMPANY
```

```shell
ldapsearch -h gvx0lsami01q.company.com -p 389 -b cn=emmatest01,ou=eu,O=COMPANY objectclass=* cn sn mail uid uniqueidentifier  
Alternatief evt: ldapsearch -h gvx0lsami01q.company.com -p 389 -b cn=emmatest01,ou=eu,O=COMPANY uid=* cn sn mail uid uniqueidentifier
```

Output:
```shell
C:\tools>ldapsearch -h gvx0lsami01q.company.com -p 389 -b cn=emmatest01,ou=eu,O=COMPANY objectclass=* cn sn mail uid uniqueidentifier  
cn=emmatest01,ou=eu,O=COMPANY  
cn=emmatest01  
cn=John Doe  
sn=emmatest01  
uid=emmatest01  
uniqueidentifier=5510012309  
mail=rr@company.com
```

```shell
C:\tools>ldapsearch -h gvx0lsami01q.company.com -p 389 -b cn=emmatest02,ou=eu,O=COMPANY objectclass=* cn sn mail uid uniqueidentifier  
cn=emmatest02,ou=eu,O=COMPANY  
cn=emmatest02  
cn=Adam Smith  
sn=emmatest02  
uid=emmatest02  
uniqueidentifier=5510012319  
mail=ph@company.com
```

