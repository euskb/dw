+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "ldap", "active", "directory" ]
description = "Integration with Microsoft Active Directory"
title = "Microsoft Active Directory"
+++

{{% notice style="note" %}}
#### VMware vSphere & Microsoft LDAP Channel Binding & Signing patch

Due to CVE-2017-8563 potential exploit, Microsoft is changing behavior of AD to accept connections only using TLS.

See external links:
-   [https://blogs.vmware.com/vsphere/2020/01/microsoft-ldap-vsphere-channel-binding-signing-adv190023.html](https://blogs.vmware.com/vsphere/2020/01/microsoft-ldap-vsphere-channel-binding-signing-adv190023.html)
-   [https://kb.vmware.com/s/article/2149697](https://kb.vmware.com/s/article/2149697) gives tips on creating needed CER files and URLs to be used.

Test if LDAP server listens to ports TCP636 and TCP3269:
```shell
nc -v LDAP-SERVER-IP 636
nc -v LDAP-SERVER-IP 3269
```
{{% /notice %}}

## Integration

Go to **Configuration > System Configuration > System > Enterprise Integration  > Directory Services**

-   **Directory Type**: Directory platform being utilized
-   **Server**: Address of the directory services server
-   **Port**: TCP Port for communication with directory services
-   **Protocol Version**: Version of LDAP being used
-   **Bind Authentication Type**: Protocol used to authenticate the LDAP session (GSS-NEGOTIATE recommended, gives auto-choice of Kerberos/NTLM)

### Advanced config

-   **Search Subdomains**: Search subdomains by LDAP chase referrals
-   **Connection/Request Timeout**: Timeout setting per connection/request in seconds
-   **Search without Base DN**: Enable to search without sending a base DN
-   **Use Recursive OID at Enrollment/Group Sync** : Only supported by AD; used to obtain group membership during enrollment and not rely on the scheduler to run
-   **Object Identifier Data Type**: Set the object ID type to string or binary. AD is binary by default while most other LDAPs are strings.
{{% notice style="note" %}}
In AirWatch 7.3+, the AD group structure itself is stored in the sync table, and if the user is shown to be a member of a nested group whose External ID is already stored in the table, the group membership will be reflected. The only flow unaccounted for is the group structure itself changes between scheduler iterations, in which at most there will be up to a 12 hour delay from when the user enrolls and is associated with the group after the group structure has changed.
{{% /notice %}}

## Directory users mapping

-   From **Directory Services**, navigate to **User**
-   Specify the **Base DN AirWatch** uses to find users in the directory
-   For **User Search Filter**, enter the parameters used to associate AirWatch user accounts with AD/LDAP accounts **(&(objectCategory=person)(sAMAccountName={EnrollmentUser}))**
-   Select **Show Advanced** to display additional options
-   Enable **Auto Merge** to consolidate changes made in the directory with AirWatch automatically
-   Use **Attributes** to assign directory services information to the correct AirWatch fields
-   Click **Save**
{{% notice style="note" %}}
All members of the Group or Organizational Unit from the directory are first synced into the **dbo.UserGroupEnrollmentUserMapSync** table by running the below LDAP query (for AD).
{{% /notice %}}
-   Another query is run for all of the DN’s in the sync table to pull the actual user information from the directory:
    **(&(objectCategory=person)(sAMAccountName=*)(|(distinguishedName={UserDN1})(distinguishedName={UserDN2})))**
    
-   When we sync User attributes, we query the directory for the users based on their ExternalID:  
    **(&(objectCategory=person)(sAMAccountName=*)(|(objectGUID={ExternalID1})(objectGUID={ExternalID2})))**
    
-   The User Group Membership Sync is the process by which we sync up the group membership for users from the directory. This will happen during a scheduler iteration if the Auto Sync option is enabled on the User Group and can be performed manually by clicking the Sync button on the User Group in the List View:
    **(&(objectClass=group)(|(objectGUID={ExternalID1}) (objectGUID={ExternalID2}))**

## Sync interval
There are three primary scheduler jobs that sync up the group membership and user attributes:

1.  **LDAP Sync** – the LDAP Sync job will sync the User Group membership.
2.  **Sync Directory Users** – this job will sync up user attributes.
3.  **Sync Admin Users** – this job will sync up admin attributes.

{{% notice style="note" %}}
#### member vs. memberOf

Every object in AD has certain attributes like **phone number, name, etc**. In AD, membership to a group is determined by both the **member** and **memberOf** attribute, which is not the case in other directory types. Some directories only have member or only memberOf. The difference is key, **memberOf** is the attribute on the actual “member” or “user” object that says “I am a member of XYZ group”, this value is usually the Distinguished Name (DN) of the group the object is a member of. The **Member** attribute is on the “container” or “group”, and says “I have XYZ as a member”, which is also usually the DN of their members. Directories that use the Member relation will have groups with a list of Member attributes of all the users that are members of that group. Directories that use the MemberOf attribute will have users that list the groups they are members of on the actual user object.
{{% /notice %}}

### Auto sync
In production all LDAP based scheduler jobs are set to fire every 12 hours. It is not recommended to lower these values for On-Premise or Dedicated SaaS customers as setting the scheduler interval too low may cause performance issues in versions older than 7.1. If a customer requires a lower interval, it is recommended to run at least one sync, and pull the **LastSyncTimeInMinutes** column from the **dbo.LDAPDefinition** table to determine how long it takes to sync the Organization Group.
{{% notice style="note" %}}
All issues that arise during a scheduler iteration will appear in the **scheduler** logs
{{% /notice %}}

### Manual sync

The Add Missing Users, User Group Membership Sync, and Sync User Attributes processes can all be triggered manually by clicking a button in the console.
{{% notice style="note" %}}
Manual user attribute sync will only sync **enrollment users**, not administrator attributes
{{% /notice %}}

{{% notice style="warning" %}}
If any issues arise during one of these processes when they are triggered manually, the **BulkProcessingServiceLogFile.txt** in the Services folder will contain the backend information. If it appears there is a UI issue with the buttons, the **WebLogFile.txt** file in the WebConsole folder will contain the information needed to troubleshoot.
{{% /notice %}}

## User enrollment

The **mobileManagement.EnrollmentUser** table contains information on all of the enrollment users in the environment:

```sql
select ExternalID, SecurityTypeID, * from mobileManagement.EnrollmentUser EU
join dbo.LocationGroup LG
on LG.LocationGroupID = EU.LocationGroupID
where LG.Name = 'ams'
```
-   **ExternalID** – the ExternalID column contains a hashed value of the attribute configured for Object Identifier. This value is used to match the AirWatch user with the customer’s directory user. If for whatever reason this value is null or incorrect, the AirWatch user will not sync.
-   **SecurityTypeID** – this column determines the type of user. 1 denotes a directory user, 2 denotes a basic user, and 3 denotes an authentication proxy user.
-   **LocationGroupID** – the Organization Group ID where the user is imported. Note that all directory users will always reside at the same level Directory Services is configured, even if imported or added at a child.
-   **LDAPDefinitionID** – the ID of the LDAP Definition the user is associated with.

**dbo.LDAPDefinition** table contains all of the configuration information from the Directory Services for an Organization Group
```sql
select LD.UserSearchFilter, LD. * from dbo.LDAPDefinition LD
join dbo.LocationGroup LG
on LG.LocationGroupID = EU.LocationGroupID
where LG.Name = 'ams'
```
-   **LastSyncDurationInMinutes** – this column contains the time it took to sync the entire Organization Group in minutes.
-   **LastSyncedOn** – last date the Organization Group synced with the directory.
-   **MemberPageSize** – the MemberPageSize value can be configured, but should not exceed 5000 if the customer is using EIS. This value determines the chunk size of information being sent back and forth between ACC\EIS
-   **IsSortControlSupported** – determines if the directory type supports sorting results at the directory server before the response is sent.

The **dbo.UserGroupEnrollmentUserMapSync** table will contain only the ExternalIDs of both the users and the groups that are members of the AirWatch group that was added

```sql
select MAP. * from dbo.UserGroupEnrollment(Core)UserMapSync MAP
join dbo.UserGroup UG
on UG.UserGroupSyncID = MAP.UserGroupSyncID
where UG.FriendlyName = 'ams'
```

The **dbo.UserGroup** table contains information about the configured User Groups in AirWatch.
```sql
select UG. * from dbo.UserGroup UG
join dbo.LocationGroup LG
on UG.RootLocationGroupID = LG.LocationGroupID
where LG.Name = 'ams'
```

The **dbo.UserGroupSync** table contains syncing information for directory User and Admin groups. It provides a few more columns of information that contain the settings you have configured per group in the console.
```sql
select UGS. * from dbo.UserGroupSync UGS
join dbo.LocationGroup LG
on UGS.RootLocationGroupID = LG.LocationGroupID
where LG.Name = 'ams'
```

{{% notice style="note" %}}
A user’s primary group in Active Directory cannot be added to AirWatch, as the primary group has no memberOf attribute on the user object. This is an AD limitation.
{{% /notice %}}

## Query Troubleshooting
{{% notice style="tip" %}}
**LDAP Admin** is the LDAP browser most commonly used internally. An LDAP browser is an excellent way to troubleshoot certain queries and determine which attributes should be configured in Directory Services. The tool can be download at [http://www.ldapadmin.org/](http://www.ldapadmin.org/)
{{% /notice %}}

With some issues, AirWatch is not able to find certain users and/or groups. This is often due to an incorrect filter or lack of permissions in the directory. If we are unable to find the user and group objects with the LDAP Admin tool using the same settings from the console, we will be unable to find them using AirWatch. It will be necessary to test queries during troubleshooting. To run a custom query, click the magnifying glass icon in the toolbar, select the Custom tab in the window that appears, type the Base DN in the Path field, and type the query in the Filter field.
![](Picture1.png)
## Connection Troubleshooting
Test connection failures are usually due to one of two error codes, either **49** or **81**. An 81 error code indicates the console cannot find the directory server, which can happen if the hostname was entered incorrectly, ACC\EIS is not functioning properly, the directory server is firewalled, or there is no route to the directory server from the console server.

When an administrator encounters a 49 error it is important to note that this error is generated by the directory server, not AirWatch. In 99% of cases this is because the bind authentication type is not supported, or the account and passwords are incorrect. To verify that the console is not sending a bad username or password, SSL must be turned off and the authentication type must be set to basic so the bind request can be sniffed off the network in plaintext. Use Wireshark!

{{% notice style="warning" %}}
“System.DirectoryServices.Protocols.LdapException: Error code:81”
{{% /notice %}}

{{% notice style="tip" %}}
-   LDAP error 81 = The LDAP library can't contact the LDAP server
-   AirWatch is not able to communicate with the LDAP server. Verify that Airwatch services are enabled in AirWatch Cloud Connector. If not, the AirWatch Cloud Connector may require re-installation after enabling.
{{% /notice %}}

## Optimization
{{% notice style="warning" %}}
Optimization of slow enrollment into AD

**Groups & Settings > All Settings > Enterprise Integration > Directory Services**, **Advanced** section

Use Recursive OID At Enrollment = Disable
{{% /notice %}}

## Common LDAP Queries

Search Group:
**(&(objectClass=group)(|(CN=*{inputName}*)(distinguishedName={inputName})))**

Sync Group:
**(&(objectClass=group)(|(objectGUID={ExternalID1})(objectGUID={ExternalID2})))**

Search User:
**(&(objectCategory=person)(sAMAccountName={InputUserName}))**

Sync User:
**(&(objectCategory=person)(sAMAccountName=*)(|(objectGUID={ExternalID1})(objectGUID={ExternalID2})))**

Add Missing Users:
**(&(objectCategory=person)(sAMAccountName=*)(|(distinguishedName={UserDN1})(distinguishedName={UserDN2})))**