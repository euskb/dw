+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "ldap" ]
description = "Integration with IBM ISAM"
title = "IBM ISAM"
+++

Notes on integration:
- Change the attribute where Distinguished Name in the AW Console is 'distinguishedName' to 'entryDN' (see attached screenshot)
- This is Accessible from the AW Console from **Groups & Settings > All Settings > System > Enterprise Integration > Directory Services**

![](LDAPDN.png)

![](LdapGroup01.png)

![](LdapGroup02.png)

![](LdapUser01.png)

![](LdapUser02.png)