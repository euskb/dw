+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "CA", "certificate", "authority", "integration" ]
description = "Integration with Microsoft CA using 2 methods: direct and EOBO"
title = "Integration - Microsoft CA"
+++

## DCOM preferred architecture for PoC

-   Windows Server for Certificate Authority role should be Datacenter/Enterprise Edition
-   The Certificate Authority should be in Enterprise Mode (AD integration)
-   The ACC/ESC connector should be in the same Active Directory Domain with appropriate CA
-   Check network ports! They are different for DCOM and SCEP

![WS1 UEM Integration with MS CA Schema](AirWatch_DCOM_Arch.jpg)

#### Network Requirements

-   TCP Port 135: Microsoft DCOM Service Control Manager.
-   TCP Ports 1025 - 5000: Default ports DCOM processes.
-   TCP Ports 49152 - 65535: Dynamic Ports.

## Configuration
### Step 1 - Install Microsoft CA
{{% expand title="Installation of MS CA Role..." %}}
#### Add the ADCS Role
1. Click the Server Manager icon next to the Start button to open the Server Manager window.
2. Click Roles in the left pane.
3. Click Add Role in the right pane. An Add Roles Wizard window displays.
4. Under Server Roles, select the Active Directory Certificate Services checkbox.
5. Click Next.
6. Select the Certification Authority checkbox and then select Next.
7. Select Enterprise and then select Next.
8. Select Root CA and then select Next.

#### Define CA Private Key Settings
1. Select Create a new private key and then select Next.
2. Select your preferred Key character length (for example 4096).
3. Select your preferred algorithm (for example SHA256) from the Select the hash algorithm for signing certificates issued by the CA and then select Next.
4. Click Common name for this CA and enter the name of the CA or use the default CA displayed and then select Next.

Make note of the name of the CA server. You will need to enter this information in AirWatch when setting up access to the CA.

5. Select the desired length of time under Set the validity period for the certificate generated for this CA and then select Next.

The length of time you select is the validity period for the CA ‒not the certificate, however, when the validity for the CA expires, so does the certificate.

#### Configure the ADCS Certificate Database
1. Click Next to accept the default information in the Configure Certificate Database screen.
2. Click Next to accept the Confirm Installation Selections screen.
3. Click Install. The installation begins. After the installation completes, the Installation Results window displays. Click Close.
{{% /expand %}}

### Step 2a - Configure Microsoft CA, Basic
{{% notice style="warning" %}}
The following steps are applicable only if the Security Department allows a service account to have access to their CA
{{% /notice %}}

#### Add a Service Account on the CA

1. Launch the Certification Authority Console from the Administrative Tools in Windows.
2. In the left pane, select (+) to expand the CA directory.
3. Right-click the name of the CA and select Properties. The CA Properties dialog box displays.
4. Click the Security tab.
5. Click Add. The Select Users, Computers, Service Accounts, or Groups dialog box displays.
6. Click within the Enter the object names to select field and type the name of the service account (e.g., Ima Service).
7. Click OK. The CA Properties dialog box displays.
8. Select the service account you added in the previous step (e.g., Ima Service) from the Group or user names list.
9. Select the **Read**, the **Issue** **and** **Manage Certificates**, and the **Request Certificates** checkboxes to assign permissions to the service account. Click OK.

{{% notice style="info" %}}
If Security Department does not allow the above, propose to use a separate child CA, and do the above configuration on it. Separate CA can be turned off in case of problems.
{{% /notice %}}

{{% notice style="warning" %}}
CA server **must be in Enterprise Mode** of operation. To install CA in Enterprise mode, **Enterprise Admin account is needed**. CA in Enterprise Mode is installed **on forest level**! This means Domain Controllers will have links to this CA, even if it is installed in a subdomain.
{{% /notice %}}

### Step 2b - Configure Microsoft CA, Enroll On Behalf Of (EOBO)
{{% notice style="tip" %}}
Security Advantage!
-   The following steps are applicable if the Security Department DOES NOT allow a service account to have access to their CA;
-   As result of EOBO configuration, users will request certificates by themselves. If service account gets disabled, users will still be able to request certificates.
{{% /notice %}}

#### CA Step 0: Enable LDAP Referrals
{{% notice style="info" %}}
This action is only needed **in multi-domain** environment!
{{% /notice %}}

Run the following commands on the CA. This configuration is needed on ADCS CA since we are requesting certificates on behalf of some other user using service account.  
This feature is only supported on Windows 2008 R2 Enterprise and later. See the link below for context and details: [https://technet.microsoft.com/en-us/library/ff955842(v=ws.10).aspx](https://technet.microsoft.com/en-us/library/ff955842(v=ws.10).aspx)

Use CMD to restart certificate services and enable cross-forest LDAP Referrals:
```shell
net stop certsvc
certutil -setreg policy\EditFlags +EDITF_ENABLELDAPREFERRALS
net start certsvc
```

#### CA Step 1: Create the Restricted Enrolment Agent Certificate Template

1. Open the Certificate Authority (CA).
2. Expand the CA Name, Right click Certificate Templates, and select Manage.
3. Right click the **Enrollment Agent (Computer)** template and select Duplicate Template (Do not choose Enrollment Agent user cert!). Name it per your preference.
4. Select Windows Server 2008+ Enterprise.
5. On the Request Handling tab, select **Allow Private Key to be Exported**.
6. In the **Subject Name** tab, make sure **Build from this Active Directory** Information is activated and **Subject Name** format is set to **Fully distinguished name**. Click OK.
8. Navigate back to the CA, right click Certificate Templates, select New, and select Certificate Template to Issue.
9. Select the duplicate copy of the template created in the previous step. Click OK.

#### CA Step 2 - Enroll a computer for the Signer Certificate
##### Substep A: Generate a new Restricted Enrollment Agent Signer Certificate
{{% notice style="tip" %}}
The following actions in this step can be done on any server that can connects to the Certificate Authority.
**Hint**: do it on the server with ESC/ACC connector.
{{% /notice %}}

1. Open MMC.
{{% notice style="warning" %}}
Use **the same** service user account to open MMC and import certificate, as the one used later for transmitting EOBO certificates. Using other user account, including admin accounts, will break the certificate request schema!
{{% /notice %}}
2. Click File and select **Add/Remove Snap in**.
3. Select **Certificates**.
4. Select **Computer Account**.
5. Select **Local Computer** and select Finish. Click OK.
6. Expand **Certificates (Local Computer)**, double click **Personal**, right click **Certificates**, select **All Tasks**, and select **Request New Certificate**. Click Next.
7. Select **Active Directory Enrollment Policy** and select Next.
8. Check the duplicate template created in earlier steps and select **Enroll**.
9. Once completed, select Finish.
{{% notice style="warning" %}}
Service user account, which is used by AirWatch to create a certificate request, **must be a domain account and have enough permissions** to access Windows certificate store. Standard user will not work. Local admin rights on the computer is a simple solution.
{{% /notice %}}

{{% expand title="Troubleshooting ESC service rights..." %}}
1. Run Services.msc
2. Stop VMware Enterprise Systems Connector Service
3. Right Click VMware Enterprise Systems Connector service.
4. Select **Properties**
5. Click on **Log On**
Use domain service account with admin permissions on local server. Make sure you are logged in with an account that has admin permissions both on the VMware Enterprise Systems Connector server and on the domain, or you may not be able to access the computer store and also add a domain user to manage the private keys
{{% /expand %}}

##### Substep B: Configure the issued certificate
1. Once the certificate has been issued, right click it and select All Tasks followed by Manage Private Keys.
2. Click Add.
3. Type **Network Service** and select Check Names. Once added, select OK twice.

##### Substep C: Export the Certificate
{{% notice style="note" %}}
If the certificate needs to be installed on multiple Device Services servers or AirWatch Cloud Connector servers, export **with the private key**. If not, skip to exporting **just the public key**.
{{% /notice %}}
Export the public key to .cer file
Only the public key needs to be exported for upload to the console:
1. Right click the issued certificate, select **All Tasks** followed by **Export**.
2. Select **No, do not export the private key**, select Next.
3. Select **DER encoded binary X.509 (.CER)**, select Next.
4. Select a destination for the exported certificate and select Next. Click Finish.

##### Substep D: Import the certificate for Device Services and ESC/ACC servers
1. Open MMC.
2. Click File and select **Add/Remove Snap in**.
3. Select **Certificates**.
4. Select **Computer Account** and select Next.
5. Select **Local Computer** and select Finish. Click OK.
8. Expand Certificates (Local Computer) and select **Personal**. Right click **Certificates**, select **All Tasks** and select **Import**…
9. Select the .cer file exported in previous steps and select Next.
10. Ensure **Place all certificate in the following store** is set to **Personal** and select Next. Click Finish.

#### CA Step 3: Add a User Certificate Template on the CA
0. Open the CA (certsrv) window.
1. In the left pane, select (+) to expand the CA directory.
2. Right-click the **Certificate Template** folder and select **Manage**. The Certificate Templates Console window displays.
3. Select the desired template (**User**) under Template Display Name, and right-click **Duplicate Template**. The Duplicate Template dialog box displays. AirWatch will use the duplicate certificate template. The template you choose depends on the function being configured in AirWatch.
{{% notice style="tip" %}}
For Wi-Fi, VPN, or Exchange Active Sync (EAS) client authentication select User template.
{{% /notice %}}
4. Select the Windows Server that represents the oldest enterprise version being used within the domain to ensure backward compatibility of the certificate that was issued.
5. Click OK. The Properties of New Template dialog box displays.

#### CA Step 4: Configure Certificate Template Properties

1. Click the **General** tab.
2. Type the name of the template displayed to users in the **Template display name** field. The Template name field auto-fills with the template display name without spaces.

You may use this default value or enter a new template name if desired. The template name may not contain spaces. Make note of the template name. You will need to enter this information in AirWatch. You will enter the Template name you just configured with no spaces in the AirWatch Console in the Issuing Template field within the Configuring the Certificate Template screen.

3. Select the desired length of time for the certificate to be active from the Validity period entry field/drop-down menu.
4. Click Apply.
5. Click the Request Handling tab.
6. Select the appropriate client authentication method from the Purpose: drop-down menu. This selection might be based on the application of the certificate being issued, although for general purpose client authentication, select **Signature and Encryption**.
7. Select the **Allow private key to be exported** checkbox. For a certificate to be installed on an iOS device, this checkbox MUST be selected.
8. Click Apply.
9. Select the **Subject Name** tab.
10. Select **Supply in the request** or **Build from this Active Directory**

{{% notice style="note" %}}
Selecting **Supply in the request** means the certificate fields will be generated by AirWatch console. Doing this will give AirWatch admin control over the text in certificate request.

Selecting **Build from this Active Directory** allows to write something simple in the AirWatch console Request Template fields (DN). CA admin will control over the text in certificate request. Do this if multiple fields are required for customer to configure EMail etc., and add these:
+ Include e-mail name in subject name
+ Include this information in alternate subjject name: E-Mail name, User principal name (UPN), etc.
{{% /notice %}}

11 (optional). **If Enrollment agent template is used**, select the **Issuance Requirements** tab and select **This number of authorized signatures** = 1. Under the **Application policy** drop-down field, select **Certificate Request Agent** and select Apply.

##### Enable the Template for Certificate Authentication
1. Click the **Extensions** tab.
2. Select **Application Policies** from the **Extensions** included in this template: field. This allows you to add client authentication.
3. Click Edit. The Edit Application Policies Extension dialog box displays.
4. Click Add. The Add Application Policy dialog box displays.
5. Select Client Authentication from the Application policies: field.
6. Click OK. The Properties of New Template dialog box displays.

##### Provide the AD Service Account Permissions to Request a Certificate
1. Click the **Security** tab.
2. Click Add. The Select Users, Computers, Service Accounts or Groups dialog box displays. This allows you to add the service account configured in Active Directory to request a certificate.
3. Enter the name of the AirWatch service account in the **Enter the object names to select** field.
4. Click OK. The Properties of New Template dialog box displays.
5. Select the service account you created previously - for AirWatch on the CA, from the Group or user names: field.
6. Select the **Enroll** checkbox under Permissions for CertTemplate ServiceAccount. Click OK.

##### Enable the Certificate Template on the CA
1. Navigate to the Certificate Authority Console.
2. Click (+) to expand the CA directory.
3. Click Certificate Templates folder.
4. Right-click and select **New > Certificate Template to Issue**. The Enable Certificates Templates dialog box displays.
5. Select the name of the certificate template (e.g., Mobile User) that you previously created in Creating a Name for the Certificate Template.
6. Click OK.

##### (optional - if Enrollment Agent template is used) Link User template to Enrollment Agent
1. Open the Certificate Authority (CA).
2. Expand the CA Name, Right click **Certificate Templates**, and select **Manage**.
3. Choose the new **Enrollment Agent (Computer)** template created for AirWatch
4. Open the **Superseded Templates** tab.
5. Click Add..
6. Choose the created User Template, and add it to the list. Click OK.

### Step 3 - Configure the AirWatch console
#### Configure the CA
1. Login to the AirWatch Console as a user with AirWatch Administrator privileges, at minimum.
2. Navigate to **System > Enterprise Integration > Certificate Authorities**.
3. Click Add.
4. Select **Microsoft ADCS** from the **Authority Type** drop-down menu. You need to select this option prior to populating other fields in the dialog so applicable fields and options display.
5. Enter the following details about the CA in the remaining fields:
    + Enter a name for the CA in the Certificate **Authority field**. This is how the CA will be displayed within the AirWatch Console.
    + Enter a brief Description for the new CA.
    + Select **ADCS** radio button in the **Protocol** section.
{{% notice style="note" %}}
If you select SCEP, note that there are different fields and selections available not covered in this guide
{{% /notice %}}
    + Enter the host name of the CA server in the Server **Hostname field** (**FQDN or IP**)
    + Enter the actual **CA Name** in the **Authority Name** field. This is the name of the CA to which the ADCS endpoint is connected
{{% notice style="tip" %}}
**Authority Name** can be found by launching the Certification Authority application on the CA server - the inner name of the CA.
{{% /notice %}}
    + Select the radio button that reflects the type of service account in the Authentication section. **Service Account** causes the device user to enter credentials. **Self-Service Portal** authenticates the device without the user having to enter their credentials.
    + Enter the service account **Domain\Username** and **Password**. This is the username and password of the ADCS service account which has sufficient access to allow AirWatch to request and issue certificates.

{{% notice style="note" %}}
If Enrollment Agent is used:
+ In **Additional Options** list choose **Restricted Enrollment Agent**.
+ Upload the public key file (.cer) exported in previous steps.
{{% /notice %}}
6. Click Save.

#### Configure the Certificate Template

##### For Enrollment Agent, data supplied by AD (in CA config)  
1. Select the **Request Templates** tab. Click Add.
2. Complete the certificate template information:.
+ **Name**: a friendly name for the new Request Template. This name is used by the AirWatch Console
+ **Description (optional)**: a brief Description for the new certificate template
+ **Certificate Authority**: choose the just created one from the certificate authority drop-down menu
+ **Issuing Template**: the name that you configured in CA in Configuring Certificate Template Properties in the **Template** name field. Make sure you enter the name with no spaces. AirWatch automatically places "certificatetemplate:" prefix afterwards. This is normal. **Do not enter the word "certificatetemplate:" yourself**!
+ **Requester Name:** put something simple here, since request attribs are handled at the CA level. Example: **{EmailDomain}** or **{EnrollmentUser}**
3. Click Save.

##### For direct User template, data supplied in request  
1. Select the **Request Templates** tab. Click Add.
2. Complete the certificate template information.
+ **Name**: a friendly name for the new Request Template. This name is used by the AirWatch Console
+ **Description (optional)**: a brief Description for the new certificate template
+ **Certificate Authority**: choose the just created one from the certificate authority drop-down menu
+ **Issuing Template**: the name that you configured in CA in Configuring Certificate Template Properties in the **Template** name field. Make sure you enter the name with no spaces. AirWatch automatically places "certificatetemplate:" prefix afterwards. This is normal. **Do not enter the word "certificatetemplate:" yourself**!
+ **Subject Name:** put specific fields, which will be in the certificate. Example syntax for multi-line Subject Name field:
```shell
CN={EnrollmentUser},E={EmailAddress},OU={UserDistinguishedName}
```
+ **Private Key Length:** choose a value (This is typically **2048** and should match the setting on the certificate template that is being used by DCOM)
+ **Private Key Type:** +Signing, +Encryption (This should match the setting on the certificate template that is being used by DCOM)
+ **San Type:** enter fields for Subject Alternative Name. **Email Address**, **User Principal Name**, and **DNS Name** are supported by ADCS Templates by default, and AirWatch recommends that you use them.

Example of fields to match CA config:
**User Principal Name** = {UserPrincipalName}
**Email Address** = {EmailAddress}

    - Select the **Automatic Certificate Renewal** checkbox to have certificates using this template automatically renewed prior to their expiration date. If enabled, specify the Auto Renewal Period in days.

    - Select the **Enable Certificate Revocation** checkbox to have certificates automatically revoked when applicable devices are unenrolled or deleted, or if the applicable profile is removed.

{{% notice style="note" %}}
If you are making use of the **Enable Certificate Revocation** feature, go to **Devices & Users > General > Advanced** and set the number of hours in the **Certificate Revocation Grace Period** field. This is the amount of time in hours after the discovery that a required certificate is missing from a device that the system will wait before actually revoking the certificate.This will help to NOT identify certificate missing on device because of big Wi-Fi latency or network issues.
{{% /notice %}}

    - Select the **Force Key Generation on Device** checkbox to generate public and private key pair on the device which improves CA performance and security.

3. Click Save.

## Test the certificates

-   Create a new device profile

![Profile screen 1](profile1.jpg)

-   Configure EMail settings in profile to use the Certificate (automatically named "certificate #1")

![Profile screen 2](profile2.jpg)

-   Configure Wi-Fi settings in profile to use Certificate (automatically named "certificate #1")

![Profile screen 3](profile3.jpg)

Check the profile on iPhone/Android: you should see the Certificate profile applied, then certificate gets issued. Check the customized fields: Subject Name and Subject Alternative Name.


