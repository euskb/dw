+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "CA", "certificate", "authority", "integration" ]
description = "SCEP and revocation workflows"
title = "SCEP and DCOM"
+++

## SCEP
SCEP vs DCOM: SCEP does NOT support certificate revokation, unlike DCOM, so DCOM integration is the preferred method.

#### SCEP Workflow
![](scep01.jpg)

#### Manual Revocation 
![](scep02.png)

#### Manual Renewal
![](scep03.png)

### EOBO
**Question:** We trying to integrate AirWatch with CA authority (ADCS). Instead of creating the certificate for the user that enrolled his device, every certificate is created for the service account we used in the integration. Any config we missed?

**Answer:** This is expected behavior. If you need the certificate created for the user object you need to leverage Enrollement On Behalf Of(Eobo). Just be aware that you lose flexibility with the cert template and values you can use.

This is done in the template settings in Airwatch. The template in ADCS is configured with Subject Name = "supply in request". So whatever you set up for the template in Airwatch will be requested from the CA. Including SN (can be username/email/serial number and so on) and SAN (again all kinds of values available in AW)

That is also the main difference to EOBO where you configure SN/SAN in the ADCS template and can only use attributes from the user object in AD.