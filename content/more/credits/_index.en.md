+++
disableToc = true
title = "Author"
+++

## About the Author

![](Alex_Bletchley.jpg)

### Podcasts (together with Alex Malashin)

[https://digital-work.space/podcast/](https://digital-work.space/podcast/)
 
### Talks and Presentations

[The Platform for University Workflows - pres (rus)](https://icebow.ru/portal/download/attachments/16515136/160512_VUZ_Platform_Ryb.pdf?version=1&modificationDate=1474888951591&api=v2)  
Presented at "[University of the Future](http://universityofthefuture.ru/)" forum, St. Petersburg, 12.05.2016

[Methodical aspects of IT projects made in engineering classes - pres (rus)](https://icebow.ru/portal/download/attachments/16515136/151126_Ryb_Presentation.pdf?version=1&modificationDate=1474886050460&api=v2)  
Presented at School 2015 in Kurkino, Moscow Region, 26.11.2015

[The answers to 10 compliance questions - live (rus)](https://www.youtube.com/watch?v=r9eWyLf4XlU&list=PLSHgmTQvHHVErxN4aSjUYAoE_wefeQ47q&index=2)Presented at Virtual Russia 2015 in Moscow, 12.11.2015.

[PCoIP Protocol Optimization - live (rus)](https://www.youtube.com/watch?v=N8IJPIyopoQ)  
Presented at Virtual Russia 2014 in Moscow, 05.11.2014.

[Network virtualization with NSX - live (rus)](https://www.youtube.com/watch?v=Z5GcMAtyDnM)  
Presented at Virtual Russia 2014 in Moscow, 16.07.2014.

[VMware vCenter Hyperic Monitorig - webinar (rus)](https://www.youtube.com/watch?v=5qKEXVO6JbI)  
Speech made at VMware Office in Moscow, 24.04.2014.

[Mobile Metrics - pres (site registration needed, rus)](http://2012.russianinternetweek.ru/get/127276/1037/)  
Presented at Russian Internet Week RIW 2012 in Moscow,18.10.2012

### Publications

[The High Availability Model of Virtual Container Services in Datacenters (rus)](https://mai.ru/publications/index.php?ID=87356)  
Vestnik MAI Journal, 2017, release 97

[Software Set of Intellectual Support and Security of LMS MAI CLASS.NET (rus, eng)](http://mi.mathnet.ru/eng/vyuru350)  
_Vestnik YuUrGU. Ser. Mat. Model. Progr., 2016, Volume 9, Issue 4_

[Cloud Service Security Modeling Using Virtualization Mechanisms (rus, eng annotation)](http://www.mai.ru/publications/index.php?ID=12514)  
Vestnik MAI Journal, 2009, part 16 N6

[Virtualization - the base of New Generation Computer Security (rus, eng annotation)](http://www.mai.ru/publications/index.php?ID=10224)  
Vestnik MAI Journal, 2009, part 16 N2

### My University Courses 

(Russian language, use [Google Translate](https://translate.google.com/) to figure)

-   [DevOps courses](http://mai.moscow/pages/viewpage.action?pageId=11599878)
-   [Multimedia courses](http://mai.moscow/pages/viewpage.action?pageId=9633840)