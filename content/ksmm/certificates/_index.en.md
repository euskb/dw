+++
tags = ["KSMM", "KSC", "SQL", "config", "settings", "kaspersky", "CA", "certificate", "authority", "integration", "security"  ]
description = "Certificates schema and flows in KSMM"
title = "KSMM Certificates"
+++
### Links:
- [Enabling certificate-based authentication of KES devices](https://support.kaspersky.com/help/KSC/14.2/en-US/112804.htm)
- [Replacing the Administration Server certificate by using the klsetsrvcert utility](https://support.kaspersky.com/KSC/14/en-US/227838.htm)
- [Renewing an iOS MDM Server certificate in Kaspersky Security for Mobile](https://support.kaspersky.com/ks10mob/settings/10890#block1)
- [Tracing KSC server](https://support.kaspersky.com/ksc14/diagnostics/15025)
## Articles
{{% children sort="weight" description="true" %}}
## General Schema of certificates storage and flows
The following schema is used in the KSMM architecture with KSC server in LAN, Network Agents in DMZ with Gateway and iOS Server roles, plus a corporate apps (Self-Service) portal is used. Client devices include Android, iOS/iPadOS and Windows desktops.
![KSMM/KSC Certificate general schema](Certificates_schema.jpg)

## Important Notes
- By default, certificate-based authentication of KES devices is disabled. Certificates are NOT checked by KSC, until registry key is created (see link on enabling certificate-based auth above). Use default behaviour for test and proof of concept projects only;

- PKI SSL protocol is used by KSC to transfer only  Mail and VPN. After configuring PKI integration and choosing the CA template, check Mail or VPN certificate settings. Next, manually create a certificate in the Certificates tab. Choose creation of certificates on KSC - this will in reality activate creation of the certificate on CA via the new integration. Certificate is first created manually, then auto-renewed by KSC;

- SCEP / NDES protocol schema not only requires CA to be exposed for device access. The certificate request (CSR) on stage 1 of protocol exchange is also done using plain text HTTP. This poses risk of man-in-the-middle attacks, use only in closed environments with extreme caution;

- Current version of KSC Web console (NWC) does NOT allow usage of manual certificates. Only self-signed certificates from KSC may be created. This is due to absence of an internal encryption mechanism for certificates and passwords in the web console code. Use MMC console or OpenAPI methods instead;

- Manual change of certificates in folders of KSC for Win server and iOS Server is prohibited! Certificates have references to them in the database, so change of file will lead to breaking in functionality of components. Use provided utilities (see schema and links above); 

- Corp App Portal certificate is a file in a folder, but changing it manually is not recommended: potential side effects are not explored. Use Windows Uninstaller to **Change** certificate:
![](iosmdm.png)