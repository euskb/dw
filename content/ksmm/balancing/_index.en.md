+++
tags = ["KSMM", "KSC", "config", "settings", "kaspersky", "network", "ports", "tcp", "udp" ]
description = "KSMM Gateways Balancing"
title = "KSMM Gateways LB"
+++
## Architecture
```mermaid
graph LR  
SS1[Device 00001] -->|TCP13292| A(HAProxy)
SS2[Device 00002] -->|TCP13292| A
SS3[Device 00003] -->|TCP13292| A
SS4[Device 00004] -->|TCP13292| A
SS5[Device 50000] -->|TCP13292| A
A -->|TCP13292| S1[NAgent 1]
A -->|TCP13292| S2[NAgent 2]
A -->|TCP13292| S3[NAgent 3]
A -->|TCP13292| S4[NAgent 4]
A -->|TCP13292| S5[NAgent 5]
S1 <-->|TCP13000| KSC[KSC Linux]
S2 <-->|TCP13000| KSC
S3 <-->|TCP13000| KSC
S4 <-->|TCP13000| KSC
S5 <-->|TCP13000| KSC
KSC <-->|TCP5432| P[(PostgreSQL)]
```

### Gateway Config
Configure load balancer shared name in Web-Server Options of KSC, Certificate and in Gateway properties:
![](CGWConf.jpg)
### HAProxy Config
HAProxy config with IP [Sticktables](https://www.haproxy.com/documentation/haproxy-configuration-tutorials/core-concepts/stick-tables/) strategy:
```json
defaults
log global
mode tcp
option tcplog
option dontlognull
timeout connect 5000
timeout client 50000
timeout server 50000
  
#frontend1
#---------------------------------
frontend front
mode tcp
bind *:13292
default_backend back
  
#sticky sessions
#---------------------------------
peers sticktables
bind :10000
server 10.0.0.10
table sticky-sessions type ip size 1m
  
#backend1
#---------------------------------
backend back
balance leastconn
mode tcp
stick match src table sticktables/sticky-sessions
stick store-request src table sticktables/sticky-sessions
server gw1 10.0.0.101:13292 check
server gw2 10.0.0.102:13292 check
server gw3 10.0.0.103:13292 check
server gw4 10.0.0.104:13292 check
server gw5 10.0.0.105:13292 check
```
