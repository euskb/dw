+++
tags = ["KSMM", "KSC", "powershell", "python", "batch", "config", "settings", "kaspersky", "scripts", "programming" ]
description = "Preparation and launching of system scripts to manage Windows workstations"
title = "KSMM Windows Desktops"
+++

## Windows Desktop Management
Desktop management is done using scripts. Scripts are packed into "Installation packs":
![](installpack.jpg)
Entry point for an installation pack is a ".BAT", ".EXE" or ".MSI/.MSP" file. A BAT-file may be used to launch Powershell, Python or any other type of scripts.

Scripts are launched using Scheduled Tasks of Remote App Install type:
![](tasks.jpg)

#### Python script launch
Example of entry-point BAT-file **runpy.bat** running Python script and then logging a success in a text file:
```Shell
c:\python3\python.exe testpy.py
@echo SCRIPT RUN SUCCESS>%SYSTEMDRIVE%\LOG.txt
```

Python **testpy.py** payload script to create a file needs to be placed in the same installation pack:
```python
if __name__ == "__main__":
    text = "File write"
    file1 = open("c:\\testFile.txt", "w")
    file1.write(text)
    file1.close() 
```

#### Powershell script launch
Example of entry-point BAT-file **runps.bat** running Powershell script and then logging a success in a text file:
```Shell
Powershell.exe -executionpolicy remotesigned -File certinstall.ps1
@echo CERT INSTALL SUCCESS>%SYSTEMDRIVE%\LOG.txt
```

Powershell payload script **certinstall.ps1** to decrypt and install a PFX certificate test2.pfx provided in the installation pack in the Windows certificate store (Local Machine store):
```Powershell
$Pass = ConvertTo-SecureString -String '12345' -Force -AsPlainText
$User = "lab\administrator"
$Cred = New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList $User, $Pass
Import-PfxCertificate -FilePath test2.pfx -CertStoreLocation Cert:\LocalMachine\My -Password $Cred.Password
```
### Parameters transfer
Parameters can be transferred using the UI, parameter section in the installation pack:
![](installpackparam.jpg)
Parameters are sent to the entry-point file (BAT-file) and should be handled there. Example of the **certinstall.bat** BAT-file running Powershell and transferring a "seconds" parameter in %1:
```shell
Powershell.exe -executionpolicy remotesigned -File timeout.ps1 -sec %1 > c:\PSLOG.txt
```

Powershell script payload **timeout.ps1** to insert a value of seconds in the Windows registry, for the system to timeout (default of 120sec in case parameter is not provided) and lock screen  in case of inactivity (restart needed for registry parameter to be applied):
```Powershell
param([Int32]$sec=120)

Write-Host 'Current timeout value: '

Get-ItemPropertyValue 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' 'InactivityTimeoutSecs' 

Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' -Name  InactivityTimeoutSecs -Value $sec

Write-Host 'New timeout value: '

Get-ItemPropertyValue 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' 'InactivityTimeoutSecs'

Restart-Computer -Force
```