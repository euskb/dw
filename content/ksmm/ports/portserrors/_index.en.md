+++
tags = ["KSMM", "KSC", "kaspersky", "troubleshooting", "network", "ports", "tcp", "udp" ]
description = "KSMM components network ports troubleshooting"
title = "Troubleshooting Ports"
+++

## Links
- [NAgent Check utility](https://support.kaspersky.com/KSC/13/en-US/3912.htm)
## Network Agent + Connection Gateway
- Network schema:
```mermaid
graph LR
K[KSC] --> |TCP13000| A[NAgent +Distr Point & Gateway];
A <--> |TCP13292| M[Mobile Device]
```

Listening service: klnagent.exe, opens TCP13000

- Check open port:
```shell
netstat -napo tcp | find "13000" # port must be LISTENING
```
- Check gateway function:
    Use **klnagchk.exe** tool (see link above), you should see `This device is a connection gateway` in output text.

- Check gateway port:
```shell
netstat -napo tcp | find "13292" # port must be LISTENING
```

- Check gateway certificate:
```shell
openssl s_client -connect ksmm.lab.local:13292
```
    You should see certificate data with correct Subject CN.

## iOS MDM Server
- Network schema:
```mermaid
graph LR
I[iOS for MDM] <--> |TCP443| D[iOS Device];
I --> |TCP2197| A(((APNs Cloud, 17.0.0.0/8)))
A --> D
```

Listening service: kliosmdmservicesrv2.exe, opens TCP443

- Check open port:
```shell
netstat -napo tcp | find "443" # port must be LISTENING
```

- Check iOS MDM certificate:
```shell
openssl s_client -connect ksmm.lab.local:443
```

## Corporate Apps Portal
- Network schema:
```mermaid
graph LR
C[Corp Portal] --> |klakaut.exe TCP13291| K[KSC];
A[Admin] --> |TCP8070| C
U[User] --> |TCP8071| C
```

- Check open port:
```shell
tasklist | find "klakaut" # => PID
netstat -napo tcp | find "PID" # port must be ESTABLISHED
```

