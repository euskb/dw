+++
tags = ["KSMM", "KSC", "config", "settings", "kaspersky", "network", "ports", "tcp", "udp" ]
description = "KSMM network ports"
title = "KSMM Network Ports"
+++
## Links
- [KSC Ports](https://support.kaspersky.com/KSC/14.2/ru-RU/158830.htm)
- [Corp App Catalog Ports](https://support.kaspersky.com/help/Corporate_App_Catalog/TR1/en-US/248482.htm)
- [Google AS15169 IP Block](https://bgp.he.net/AS15169)
## Network Troubleshooting Articles
{{% children sort="weight" description="true" %}}
## Devices to KSMM/KSN

| Sender                                                      | Recipient                                         | Ports                         | Description                                                                                                                     | Port may be changed                      |
| ----------------------------------------------------------- | ------------------------------------------------- | ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| (WAN/DMZ) Mobile Device with KES Agent                      | (DMZ) NAgent +Gateway mode                        | TCP13292, TCP13293            | Mobile device connects to KSC via Gateway                                                                                       | Yes - on KSC console                     |
| (WAN/DMZ) Mobile Device with KES Agent                      | (DMZ/LAN) KSC                                     | TCP13292, TCP13293            | Mobile device connects to KSC directly                                                                                          | Yes - on KSC console                     |
| (DMZ/LAN) Device with KES Agent                             | (DMZ/LAN) Activation Proxy-server on KSC klactprx | TCP17000, TCP17100            | License activation proxy for desktop devices (TCP17000) OR mobile devices (TCP17100) IF devices do not have internet connection | Yes - on KSC console                     |
| (WAN/DMZ) iOS Device with Control Profile                   | (DMZ) iOS MDM Server, kliosmdmservicesrv.exe      | TCP443                        | Sending data to iOS devices                                                                                                     | Yes - during iOS MDM Server installation |
| (WAN/DMZ) Browser on Device with KES Agent                  | (DMZ) Corp Catalog, Apache Web Server             | TCP8071                       | User device downloads apps from Corp App Catalog component                                                                      | Yes - during installation                |
| (WAN/DMZ/LAN) Desktop (Win/macOS/NIX) Device with KES Agent | (WAN) KSN Cloud Proxy                             | TCP13111, UDP15111            | Access of controlled devices to Kaspersky Security Network (Cloud, optional)                                                    | Yes - on KSC console                     |
| (WAN/DMZ) Desktop (Win/macOS/NIX) Device with KES Agent     | (DMZ) NAgent +Gateway mode                        | TCP13000                      | Connection of NAgent from client desktop to NAgent Gateway proxy for data exchange with KSC                                     | Yes - on KSC console                     |
| (DMZ/LAN) Desktop (Win/macOS/NIX) Device with KES Agent     | (LAN) KSC                                         | TCP13000, UDP13000, TCP14000  | Connection of NAgent from client desktop directly to KSC. UDP13000 needed for NAgent status update                              | Yes - on KSC console                     |
| (WAN/DMZ) Desktop (Win/macOS/NIX) Device with KES Agent     | (DMZ) NAgent +Gateway mode                        | TCP13295                      | PUSH-notifications to desktop machines                                                                                          | Yes - on KSC console                     |
| (DMZ/LAN) Device with KES Agent                             | (DMZ/LAN) KSC                                     | TCP8060(HTTP), TCP8061(HTTPS) | Installation packets request and download directly from KSC (if Corp App Component is not available)                            | Yes - on KSC console                     |
| (DMZ/LAN) OS Windows Device with KES Agent                  | (DMZ/LAN) KSC Web Console - klsctunnel            | TCP19170                      | Remote desktop assist connection of tech support to user device                                                                 | Yes - on KSC console                     |

## KSMM to PUSH Cloud Services

| Sender               | Recipient                                                                                                                                                                                              | Ports                     | Description                            | Port may be changed  |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------- | -------------------------------------- | -------------------- |
| (DMZ/LAN) KSC        | Google Firebase Cloud Messaging (FCM) ASN15169 IP block: android.googleapis.com, play.google.com, android.clients.google.com, accounts.google.com, fonts.googleapis.com                                | TCP5228, TCP5229, TCP5230 | PUSH notifications for Android devices | No                   |
| (DMZ) iOS MDM Server | Apple Cloud APNs IP Block 17.0.0.0/8: gateway.push.apple.com, feedback.push.apple.com, mdmenrollment.apple.com, *.itunes.apple.com, *.mzstatic.com, *phobos.apple.com, *phobos.apple.com.edgesuite.net | TCP2195, TCP2197          | PUSH notifications for iOS             | No                   |


## KSMM to KSMM Components

| Sender                                             | Recipient                              | Ports    | Description                                                                                                | Port may be changed?                  | 
| -------------------------------------------------- | -------------------------------------- | -------- | ---------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| (LAN) KSC                                          | (DMZ) NAgent +Gateway Mode             | TCP13000 | Request to NAgent to establish tunnel session for connecting devices from WAN/DMZ                          | Yes - in KSC Console                  |
| (DMZ) Corp App Catalog klakaut.exe                 | (DMZ)  NAgent +Gateway Mode            | TCP13000 | Corp App Catalog connects via Gateway to KSC for data exchange with KSC                                    | Yes - during Corp App Catalog install |
| (DMZ) Subordinate KSC                              | (LAN) Main KSC OR NAgent +Gateway Mode | TCP13000 | In cascade KSC Architecture, two KSC servers communicate between each other directly OR via NAgent Gateway | Yes - in KSC Console                  |
| (LAN) MMC Admin Console                            | (LAN) KSC                              | TCP13291 | Connection of remote MMC admin console to KSC                                                              | Yes - in KSC Console                  |
| (DMZ/LAN) NAgent, NAgent +Gateway, KSC             | (DMZ/LAN) NAgent, NAgent +Gateway, KSC | UDP15000 | Installation packets, status update messages, NAgent discovery of other NAgents in broadcast domain        | Yes - in KSC Console                  |
| (LAN) Web Admin Console (New Web Console = NWC)    | (LAN) KSC                              | TCP8080  | Connection from browser to Web admin console https://ksc.local:8080                                        | Yes - in KSC Web Console settings     |
| (LAN) Third-party scripts, Web Admin Console (NWC) | (LAN) KSC OpenAPI Interface            | TCP13299 | KSC klserver component REST API port for OpenAPI commands                                                  | Yes - in KSC Console                  |








