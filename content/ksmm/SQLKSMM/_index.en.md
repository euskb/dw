+++
tags = ["KSMM", "KSC", "SQL", "config", "settings", "kaspersky", "troubleshooting" ]
description = "Config and troubleshooting of SQL in KSMM"
title = "KSMM SQL"
+++

## Recommendations
* **MS SQL Express 2019** is embedded in the current version of KSC/KSMM. This is a limited version of SQL. It is not available in case of 10000+ device install and/or Program Control component enabled;

* **MySQL v8.0.20**+ may be used for installations of <50000 devices (Program Control component NOT enabled);

* **MariaDB v10.3**+ may be used for installations of <20000 devices (Program Control component NOT enabled);

### MS SQL Express 2019 w/o CU12 update error
Symptom: very slow response from MMC console of KSC. Very high resources utilisation of KSC server. 

* Download [Management Studio](https://docs.microsoft.com/ru-ru/sql/ssms/download-sql-server-management-studio-ssms?view=sql- server-ver16) and install it on the SQL server;
* Open Management Studio, connect to the SQL Express localhost server;
* Use **New Query** button and launch command:
```sql
USE KAV  
GO  
ALTER DATABASE SCOPED CONFIGURATION SET TSQL_SCALAR_UDF_INLINING = OFF 
GO
```
"KAV" - database default name. Change if using another name for the database. 

* After completing the SQL query above, restart the SQL Server (SQLEXPRESS) service using Sql Server Configuration Manager. 