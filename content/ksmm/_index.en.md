+++
archetype = "chapter"
title = "[KL] Kaspersky Labs"
ordersectionsby = "title"
weight = 4
+++

Статьи по установке, настройке и решению проблем с Kaspersky Secure Mobile Management.

* Справка последней версии KSMM: https://support.kaspersky.com/KESMob/AndroidTR48_iOSTR2/ru-RU/150909.htm
* Корпоративный каталог приложений: https://support.kaspersky.com/help/Corporate_App_Catalog/TR1/ru-RU/246363.htm

{{% children sort="weight" description="true" %}}

### KSMM Architecture (KSC + Gateways)

![](ArchWinNix.jpg)