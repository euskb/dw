+++
tags = ["KSMM", "KSC", "SQL", "config", "settings", "kaspersky", "API", "REST", "python"  ]
description = "Open API for KSC"
title = "OpenAPI"
+++

### Links
- [KSMM OpenAPI Documentation](https://support.kaspersky.com/help/KSC/14.2/KSCAPI/index.html)
- [KlAkOAPI Python Package Documentation](https://support.kaspersky.com/help/KSC/14.2/KSCAPI/a00444.html)

{{% notice style="warning" %}}
KlAkOAPI Python Package is a wrapper over the regular REST API calls. I do NOT recommend its' usage, because it does not cover all classes, parameters and attributes available. 
{{% /notice %}}

## Articles
{{% children sort="weight" description="true" %}}
## Config / Auth
Access with OpenAPI requires 2 auth elements:
- Local technical user account login/password (Basic Auth method)
- KSC Server certificate verification (optional)

For **KSC for Windows**, server certificate  is located at path:
```%ProgramData%\KasperskyLab\adminkit\1093\cert\klserver.cer```
Certificate needs to be copied to the machine, from which a script is run.
### Login example
#### REST API Classic login:
```python
import requests
import base64
import json
import urllib3
from pprint import pprint

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

ksc_server = "https://ksmm.demo.local:13299"
url = ksc_server + "/api/v1.0/login"
user = "user_api"
password = "P@ssw0rd"
data = {}

user = base64.b64encode(user.encode('utf-8')).decode("utf-8")
password = base64.b64encode(password.encode('utf-8')).decode("utf-8")
session = requests.Session()

auth_headers = {
    'Authorization': 'KSCBasic user="' + user + '", pass="' + password + '", internal="1"',
    'Content-Type': 'application/json',
}

response = session.post(url=url, headers=auth_headers, data=data, verify=False)
print(f'Server response = {response.status_code}')
```

#### KlAkOAPI Python Package Login:
```python
import socket
import uuid
import datetime
from sys import platform
from KlAkOAPI.Params import KlAkArray, paramBinary, strToBin, dateTimeToStr
from KlAkOAPI.AdmServer import KlAkAdmServer

def GetServer():
    server_address = 'ksmm.demo.local'
    server_port = 13299
    server_url = 'https://' + server_address + ':' + str(server_port)
    
    username = 'user_api'
    password = 'P@ssw0rd'
    SSLVerifyCert = 'C:\\Lab\\klserver.cer'

    server = KlAkAdmServer.Create(server_url, username, password, verify = SSLVerifyCert)
    return server

# Call server login function:
server = GetServer()
```

