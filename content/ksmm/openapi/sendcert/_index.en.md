+++
tags = ["KSMM", "KSC", "SQL", "config", "settings", "kaspersky", "API", "REST", "python"  ]
description = "Open API for KSC to create & send certificates"
title = "OpenAPI - Certificates"
+++

## Links
- [MdmCertCtrlApi::SetCertificateAsync2](https://support.kaspersky.com/help/KSC/14/KSCAPI/a00536_a7e29bfca2f95f29dcb8e15c060ad65ae.html#a7e29bfca2f95f29dcb8e15c060ad65ae)
- [KeyService::EncryptData](https://support.kaspersky.com/help/KSC/14/KSCAPI/a00526_a9b0573d12a2fcb015a851243cbdf53f2.html#a9b0573d12a2fcb015a851243cbdf53f2)
- [paramBinary](https://support.kaspersky.com/help/KSC/14/KSCAPI/a00422.html)
- [Base 64 Encode/Decode website](https://www.base64decode.org/)
- [Common format for certificate parameters](https://support.kaspersky.com/help/KSC/14/KSCAPI/a00179.html)
## Send a certificate created on KSC
This is a programmatic way to do Create Certificate -> Mail or VPN certificate (placed in User Certificate Store) -> Self-Signed Certificate from KSC.
The certificate is generated on the KSC and send to a user and his device - to the device, where the specific user is the owner. User is chosen by his unique ID, `ul_binId` parameter. See [List Users page](../listusers/index.html) for details on how to get unique user IDs. 

```python
userID = 'YbTpoXJ4XkSxzy5hcXm75w=='

url = ksc_server + "/api/v1.0/MdmCertCtrlApi.SetCertificateAsync2"
# "NSDomain" - Domain Auth 
# "CPKES" - certificate for OS Android
# "CTMail" - Mail certificate type
# "ul_binId" - paramBinary complex data with base64-encoded unique user ID

data = {'pAuthType':{'NSDomain': True},'pCertificate':{'CPKES':True, 'CTMail':True}, "pRecipient":{"ul_binId":{"type":"binary","value":userID}}}

response = session.post(url=url, headers=common_headers, data=json.dumps(data), verify=False)
wstrIteratorId = json.loads(response.text)
```

## paramBinary
This is a complex data type with base64 encoded string data. 
Example:
```python
import base64
paramBinary = {"type": "binary", "value": "c29tZXRleHQ="}
print(base64.b64decode("c29tZXRleHQ=")) # b'sometext'
```
This paramBinary type is used for transferring certificate PEM data, certificate password data etc. 
Note: although PFX container is supported in documentation, using it raises "cannot be JSON serialised" errors. 
