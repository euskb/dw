+++
tags = ["KSMM", "KSC", "SQL", "config", "settings", "kaspersky", "API", "REST", "python"  ]
description = "Open API for KSC to list users and their IDs"
title = "OpenAPI - List Data"
+++

## Links
* [SrvView Class Reference](https://support.kaspersky.com/help/KSC/14.2/KSCAPI/a00585.html)
* [SrvView Views list](https://support.kaspersky.com/help/KSC/14.2/KSCAPI/a00154.html)

## List Data

SrvView Class, ResetIterator function is used to query and list inventory elements from KSC:
- Choose a "view" type = which data to query (SrvView Views list link above). "GlobalUsersListSrvViewName" was chosen in the code below to query users and their unique IDs;
- Choose which fields should be shown for the chosen View (for "GlobalUsersListSrvViewName", available fields are listed in [Users and groups list section](https://support.kaspersky.com/help/KSC/14.2/KSCAPI/a00108.html)). Display names (ul_wstrDisplayName) and unique IDs (ul_binId) are chosen fotr the example;
- Get the record count - how many records are there to show. Use SrvView Class, GetRecordCount function for this;
- Get the records themselves: Use SrvView Class, GetRecordRange function to provide the final records counting from the first record to the last, which was provided in the previous step. 

```python
data = {}

# CHOOSE VIEW TYPE, DATA FILEDS
url = ksc_server + "/api/v1.0/SrvView.ResetIterator"
data = {"wstrViewName": "GlobalUsersListSrvViewName", "vecFieldsToReturn": ["ul_wstrDisplayName","ul_binId"], "lifetimeSec": 3600}
response = session.post(url=url, headers=common_headers, data=json.dumps(data), verify=False)
wstrIteratorId = json.loads(response.text)['wstrIteratorId']

# FIGURE OUT THE NUMBER OF RECORDS AVAILABLE IN THE DATABASE
url = ksc_server + "/api/v1.0/SrvView.GetRecordCount"
data = {"wstrIteratorId": wstrIteratorId}
response = session.post(url=url, headers=common_headers, data=json.dumps(data), verify=False)
count = json.loads(response.text)
NUMBER_OF_RECORDS = int(count['PxgRetVal'])

# GET THE DATA ITSELF WITH ALL THE RESTRICTIONS PROVIDED EARLIER
url = ksc_server + "/api/v1.0/SrvView.GetRecordRange"
data = {"wstrIteratorId": wstrIteratorId, "nStart": 0, "nEnd": NUMBER_OF_RECORDS}
response = session.post(url=url, headers=common_headers, data=json.dumps(data), verify=False)
pRecords = json.loads(response.text)['pRecords']['KLCSP_ITERATOR_ARRAY']

# LIST THE DATA, FILTERING OUT EXTRA SYNTAX
for record in pRecords:
    print(f"{record['value']['ul_wstrDisplayName']} : {record['value']['ul_binId']['value']}" ) 
```