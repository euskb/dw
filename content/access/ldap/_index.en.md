+++
tags = ["IDM", "Access", "config", "settings", "vmware", "Identity", "LDAP", "Active", "Directory" ]
description = "Remove AD from WS1 Access DB"
title = "AD Connection Removal"
+++

## Supported removal
-   Directory cannot be removed, because an active Connector is attached to it;
-   Connector cannot be removed because an Identity Provider (IdP) is associated with it (BuiltIn IdP);
-   BuiltIn IdP cannot be dissociated, because it is locked to the Connector.
-   If you need to delete the Connector, you have to delete User Directory;
-   If you need to delete User Directory, then Built-in IDP should not be associated with it.

Steps:
 *  Check the Built-in Identity Provider and take note of the access policies associated with the Built-in Identity Provider;
 *  Un-associate all the policy rules. Edit **Identity & Access Management →** **Policies** tab (**Setup** mode) - remove "Password (Cloud Directory)" from policies, put "Password (Local Directory)" instead;
 *  Delete the Built-in IdP:
    *  Remove the connector associated with the Built-in IDP and save it. Uncheck the box for **Password (Cloud deployment)** in connector properties and save it;
    *  Click on red cross to delete Connector in Built-In properties;
    *  Click on **Delete IDP**.  Click OK to delete IdP.
*  Now you can go to Directories and delete the User Directory;
*  Wait until the directory is deleted. Check by refreshing directory page;
*  Delete the Connector using the red delete button on the right of its' name in the list;
*  Go via RDP to the server with Enterprise Connector, uninstall it.

## Unsupported removal
Access saas database of IDM and delete association of IdP with Connector.

### For PostgreSQL

If database of IDM is deployed as inherent PostgreSQL, then use SSH to connect to the **primary** appliance. 
❗️Database on primary appliance is in read/write mode.

Connect to database:
```bash

su - postgres /opt/vmware/vpostgres/current/bin/psql vcac

```

Change schema to saas:

```sql

SET SCHEMA 'saas';

```

### For Microsoft SQL Server

Use SQL Management Studio to connect to DB server, open **saas** database.

Find Connector table (example shown for MS SQL Server 2014) and 'disconnect' it from AD:

```sql

SELECT * FROM saas.saas.Connector;

-- Check the ID of your AD connector

UPDATE saas.saas.Connector
SET domainJoined = NULL
WHERE id = N; -- set N to the ID of your AD connector

```

❗️You **cannot delete** the Connector in SQL - it has dependencies from foreign keys. But all you need to do is find the ID of BuiltIn IdP and disconnect it:

```sql

SELECT * FROM saas.saas.IdentityProviders;
SELECT * FROM saas.saas.IdpJoinConn;

-- Check the ID of your IdP defore doing this!!

DELETE FROM saas.saas.IdpJoinConn
WHERE idIdentityProvider = N; -- set N to the ID of your IdP

```

-   After this, you can delete the Connector in Identity Manager Console. Then delete the AD Directory;
-   Reinstall Identity Manager Connector and activate it.

❗️After adding a new AD do not forget to follow the guide backwards: do not forget to add "Password (Cloud Directory)" back to Policies!

❗️The BuiltIn IdP must be associated back with the new Connector. This can be done using the console, or it can also be done in SQL:

```sql

SELECT * FROM saas.saas.IdpJoinConn;

-- Double-check the ID of your IdP and NEW Connector defore doing this!!

INSERT INTO saas.saas.IdpJoinConn
VALUES (X, Y) -- set X to the ID of your IdP, set Y to the ID of Connector

```