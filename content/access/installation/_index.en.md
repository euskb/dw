+++
tags = ["IDM", "Access", "config", "settings", "vmware", "Identity" ]
description = "WS1 Access Installation"
title = "Access Installation"
+++

## Connected Articles
{{% children sort="weight" description="true" %}}

## Links
 * Open [Source code of vIDM](https://github.com/vmware/lightwave/tree/dev/vmidentity)
 * Official [Documentation](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/index.html)
 * Developer [API](https://developer.vmware.com/apis)
 * Step-by-step [install of WS1 Access](https://www.carlstalhood.com/vmware-identity-manager/)
 * Load [Balancing of WS1 Access on Netscaler](http://www.carlstalhood.com/VMware-Identity-Manager-Load-Balancing)
 * Load [Balancing WS1 Access on F5](content/access/installation/SQL-install/files/IDM-cluster.pdf)
 * Configuring [SQL Database for WS1 Access 21.08](https://docs.vmware.com/en/VMware-Workspace-ONE-Access/21.08/workspace_one_access_install/GUID-FCDC0FBF-5BD5-442E-B6C1-92540389C106.html)

## Components
**Identity Manager Appliance**
-   The Service; User portal, Built-in AuthN and idP (TCP443)
-   Certificate Proxy Service (TCP5262)
-   Kerberos Key Distribution Center (KDC) (TCP/UDP 88)
-   User database (vPostgres OR external MS SQL)
-   OS = [PhotonOS](https://github.com/vmware/photon)
-   Main WS1 Access service = horizon-workspace

**Enterprise Connector**
-  ❗️Old connector version 1811 needed for ThinApp publishing
- Modern Connector is a Java Microservices app, with 4 microservices: User Auth, Kerberos Auth, Virtual App (1Gb RAM set for each service) and Directory Sync (4Gb RAM)

Increasing memory of services on connector:
1.  Log in to the Windows server in which the Workspace ONE Access enterprise service is installed.
2.  Navigate to the **INSTALL_DIR\Workspace ONE Access\serviceName** folder.
3.  Open the **serviceName.xml** file in a text editor.
4.  Change the `Xmx1g` entry to `Xmxng` where n is the maximum heap memory you want to allocate. Example: `Xmx5g`
5.  Save file, restart service.

Network ports for connector:
Inbound & outbound TCP443 (many uses);
Outbound TCP389, TCP636, TCP3268, TCP3269 (LDAP);
Outbound TCP88, UDP88, TCP464, TCP135, TCP445 (Kerberos, Directory Sync);
Outbound TCP53, UDP53 (DNS);
Outbound TCP5555 (RSA SecurID);
Outbound UDP514 (Syslog).

**Intelligent Hub**
This is a client simultaneously for WS1 UEM and WS1 Access (Corp apps marketplace + Hub Services: people search, notifications)

### Old components
**Identity Manager on Windows Server (Old, uses only Cloud KDC)**
-   The Service; User portal, Built-in AuthN and idP (TCP443)
-   The Connector; AuthN and User, ThinApp and Horizon Sync (TCP8443)
-   Certificate Proxy Service (TCP5262)

! For IDM on Windows do **NOT** use non-English localized Windows versions.
Workaround: change the regional number setting for decimal to use a period "." instead of a comma ",".
! For IDM on Windows shutdown IIS to free up port TCP80. It is not used, but it is needed for IDM install.

**Workspace One Client Mobile App**
App Bundle ID: com.air-watch.appcenter
