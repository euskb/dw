+++
tags = ["IDM", "Access", "config", "settings", "vmware", "Identity", "security", "certificate" ]
description = "Managing certificates for WS1 Access"
title = "Managing Certificates"
+++

External links:
-   Apple requirements for certificates: [https://support.apple.com/en-us/HT210176](https://support.apple.com/en-us/HT210176)
-   [https://www.carlstalhood.com/vmware-access#identitymanagercertificate](https://www.carlstalhood.com/vmware-access#identitymanagercertificate)
-   [https://medium.com/@nirmaluchoudhari/converting-certificates-using-openssl-9957a743c66b](https://medium.com/@nirmaluchoudhari/converting-certificates-using-openssl-9957a743c66b)

## General Commands

Most common command is to build PFX-file from PEM files:
```bash

openssl pkcs12 -export -out certificate.pfx -inkey privkey.pem -in fullchain.pem -certfile cert.pem

```

WS1 Access always has to be signed with corporate or trusted certificates. If Access is clustered, sign the load-balanced name with external trusted certificate and the 3 nodes - with certs from corporate CA. 

-   Go to WS1 Access web console
-   On the top, click the **Appliance Settings** tab,
-   On the left, click the **VA Configuration** node.
-   On the right, click **Manage Configuration**. You will be redirected to a separate portal
-   Login as admin account
-   On the left, click **Install TLS Certificates**.
-   On the right, in the upper box, delete the certificate and key that are currently displayed.
-   Paste in the new PEM certificate and RSA private key. Paste every certificate in the chain: server + intermediate + root. Click **Save**.

❗️The order of certificates is important! First server, then intermediate, then root.

## Certificate Requests
```

######### The cert request in idm01.domain.local.inf file

[Version]
Signature= "$Windows NT$" 
 
[NewRequest]
Subject = "CN=idm01.domain.local,OU=IT,O=Horn_n_hooves,L=Moscow,S=Moscow,C=RU"
KeySpec = 1
KeyLength = 2048
Exportable = TRUE
MachineKeySet = TRUE
UseExistingKeySet = FALSE
ProviderName = "Microsoft RSA SChannel Cryptographic Provider"
ProviderType = 12
RequestType = PKCS10
KeyUsage = 0xa0
FriendlyName = "vdm" ; needed for Horizon Connection Server only!
 
[EnhancedKeyUsageExtension]
OID=1.3.6.1.5.5.7.3.1
 
[RequestAttributes]
CertificateTemplate = WebServerExportable2008
 
[Extensions]
; If your client operating system is Windows Server 2008, Windows Server 2008 R2, Windows Vista, or Windows 7
; SANs can be included in the Extensions section by using the following text format.Note 2.5.29.17 is the OID for a SAN extension.
 
2.5.29.17 = "{text}"
_continue_ = "dns=idm01.domain.local&dns=idm01&dns=idm02.domain.local&dns=idm02&dns=idm03.domain.local&dns=idm03&dns=idm.domain.local"
##################################################################################

```

Bat script to submit certificate request:

```bash

set srvname=idm01.domain.local
cd C:\temp

Certreq -New -f %srvname%.inf %srvname%.req
Certreq -submit %srvname%.req %srvname%.crt
Certreq -accept %srvname%.crt
Certutil -exportpfx -p 12345 %srvname% "%srvname%.pfx"

```

In order to copy and paste the private key from PFX certificate for vIDM, you need a **decrypted** version of the key. Use OpenSSL to obtain this key:

```bash

openssl pkcs12 -in idm01.domain.local.pfx -nocerts -out idm01.domain.local_encrypted.key
openssl rsa -in idm01.domain.local_encrypted.key -out idm01.domain.local_decrypted.key

```

Open idm01.domain.local_decrypted.key with a text editor and copy the key from there.
After inserting certificates, click OK to restart WS1 Access web service.