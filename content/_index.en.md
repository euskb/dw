+++
archetype = "home"
title = "Рабочие заметки по EMM"
+++

Добро пожаловать на базу знаний по продуктам управления рабочими местами пользователей!

## История изменений
{{% badge %}}03.12.2024{{% /badge %}} 
* Добавил схему и конфигурацию [балансировки шлюзов KSMM](ksmm/balancing/index.html)

{{% badge %}}30.05.2024{{% /badge %}} 
* Добавил статью про [запуск скриптов в Windows с помощью KSMM](ksmm/windows/index.html)

{{% badge %}}15.09.2023{{% /badge %}} 
- Список поддержки [вариантов шифрования TLS по версиям в KSMM](ksmm/ciphers/index.html)

{{% badge %}}14.09.2023{{% /badge %}} 
- Добавил решение ошибки при настройке [интеграции KSMM с PKI](ksmm/certificates/pkica/index.html)

{{% badge %}}01.09.2023{{% /badge %}} 
- Новая статья по [интеграции KSMM с PKI](ksmm/certificates/pkica/index.html)

{{% badge %}}25.08.2023{{% /badge %}} 
* Дополнил статью по KSMM API темой [автоматизации отсылки сертификата](ksmm/openapi/sendcert/index.html)

{{% badge %}}24.08.2023{{% /badge %}} 
* Новая секция - [сетевые порты KSMM](ksmm/ports/index.html)

{{% badge %}}23.08.2023{{% /badge %}} 
* Новая статья со схемой по [сертификатам в KSMM](ksmm/certificates/index.html)

{{% badge %}}21.08.2023{{% /badge %}} 
* Дополнил статью по KSMM API темой [вывода данных из базы](ksmm/openapi/listusers/index.html)

{{% badge %}}18.08.2023{{% /badge %}} 
* Добавил статью по [KSMM API](ksmm/openapi/index.html)

{{% badge %}}15.08.2023{{% /badge %}} 
* Добавил раздел по [Kaspersky Secure Mobile Management](ksmm/index.html)

{{% badge %}}08.02.2023{{% /badge %}} 
- Добавил раздел по [управлению Windows 10](uem/windows/index.html)
- Добавил статьи по [решению проблем с Per-App-VPN](uem/vpn/index.html)
- Добавил статью по [со схемами работы SCEP](uem/ca-integration/ca-workflows/index.html)
- Добавил статью по [исследованию памяти мобильных приложений](uem/logs/memdump/index.html)

{{% badge %}}01.02.2023{{% /badge %}} 
- Добавил статью по [решению проблем с SEG на UAG](uag/seg-uag/index.html)
- Добавил статью по [записи данных в SQL по почтовому шлюзу в WS1 UEM](uem/sql/memtables/index.html)
- Добавил статью по [анализу потока данных Boxer с помощью Fiddler](uem/email/boxer-fiddler/index.html)
- Новые подробности в статье по [решению проблем с ENSv2](uem/email/ens-troubleshooting/index.html)
- Добавил статью [по БД ENSv2 в AWS](uem/sql/ensdb/index.html)

{{% badge %}}31.01.2023{{% /badge %}} 
- Добавил статью по [типовым командам Powershell для работы с MEG в WS1 UEM](uem/email/powershell/index.html)

{{% badge %}}26.01.2023{{% /badge %}} 
- Добавил статью по [поиску в логе соединений Boxer for Android](uem/logs/boxerandroid/index.html)

{{% badge %}}25.01.2023{{% /badge %}} 
- Добавил статьи по [производительности WS1 UEM на крупных проектах](uem/performance/index.html)
- Добавил статьи по [REST API в WS1 UEM](uem/restapi/index.html)
- Добавил статьи по [структуре и таблицам WS1 UEM базы SQL](uem/sql/index.html)
- Добавил статью по [WS1 Launcher и настройкам XML](uem/launcher/index.html)
- Добавил статью по [загрузке и установке ПО с WS1 UEM](uem/apps/index.html)

{{% badge %}}24.01.2023{{% /badge %}} 
- Контент мной перенесён из прошлого проекта
- Добавил статьи по [логам WS1 UEM](uem/logs/index.html)
- Добавил статьи по [защите WS1 UEM](uem/hardening/index.html)
- Добавил статью по [интеграции WS1 UEM и MS CA](uem/ca-integration/index.html)
- Добавил статьи по [интеграции WS1 UEM и почтовых систем](uem/email/index.html)
- Добавил статьи по [интеграции WS1 UEM с каталогами LDAP](uem/ldap/index.html)

{{% badge %}}20.01.2023{{% /badge %}} 
- Сайт переехал на новую тему, с меньшим числом проблем.

{{% badge %}}18.02.2022{{% /badge %}}  
+ Опубликован основной набор содержимого по Airwatch / Workspace ONE UEM 

{{% badge %}}11.12.2020{{% /badge %}} Первая версия сайта.