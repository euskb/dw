+++
tags = ["MDM", "config", "settings", "airwatch", "UEM", "EMM", "vmware", "email", "seg", "troubleshooting" ]
description = "SEG on UAG Troubleshooting"
title = "SEG on UAG"
+++

## UAG Architecture

![](seguagarch.png)

## SEG Architecture
![](segdocker.png)
- SEG process running inside the container also behaves as just any other process on UAG, from networking perspective.
- Handling ports - there is no port mapping between UAG host and the container, as container runs with host network stack itself. There are fixed set of ports reserved on UAG for running SEG. These are listed below:
![](uagports.png)

## UAG Admin UI and Logs
- Admin UI: https://UAG IP:9443/admin
- Show Edge service settings.
- Log archive: UAG + All edge services
- Log level settings: UAG logs level only.

Important logs:
- Appliance-agent: Configuration related.
- SEG: SEG service-related.
- Docker: Docker related.

Logs file path:
SEG logs: /var/log/vmware/docker/seg
UAG logs: /opt/vmware/gateway/logs 
        and /var/log/vmware/appliance-agent/ (Appliance agent logs)

## SEG Configuration
- General mem configuration.
- Outbound proxy configuration.
- Trusted certificates.
- Host file entries.
- Add SSL certificate.
- Save (Install and configure)

![](segconfig.png)

## Troubleshooting SEG
### SEG configuration issues
- Check Appliance-agent logs.
- Check v2-seg-installer and app logs (Available inside SEG folder).
- If no SEG folder then check docker logs.
- If docker fails to start, then check **Journalctl -xe**

SEG application issues
- If SEG service is up and running, then all SEG logs will be inside SEG folder.
- Change SEG logging level - https://docs.vmware.com/en/Unified-Access-Gateway/2203/uag-deploy-config/GUID-BE6BDABA-83C5-49CA-AF55-49D3B1159E0E.html

Enable SSH on UAG
- Connect to UAG using vCentre web console
- Navigate **cd /opt/vmware/gateway/bin/** and execute **./enable_ssh.sh**

### Check Docker Status
```bash
systemctl status docker.service
docker ps
```

To check all required service is up and running: `netstat –ntlp`

Firewall and connection issues - https://docs.vmware.com/en/Unified-Access-Gateway/3.10/com.vmware.uag-310-deploy-config.doc/GUID-390D3A2A-0CB7-4A82-9B0F-D525B74CF55B.html